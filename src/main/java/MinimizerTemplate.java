
public class MinimizerTemplate
{
    protected FormulaTree formula;
    protected String minimizedFormula;
    protected long startTime;
    protected long finTime;
    protected boolean finished;

    public long getComputationTime() { return finTime-startTime; }

    public String getMinimizedFormula() { return minimizedFormula; }
}
