import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.Vector;

public class QMCMinimizer extends MinimizerTemplate implements Runnable
{
    private boolean qmChangeIndicator;

    @Override
    public void run()
    {
        finished = false;
        startTime = System.currentTimeMillis();
        this.quineMccluskeyMinimization();
    }

    /**
     * Minimizes the given formula using the Quine-McCluskey algorithm
     * @return Returns a String representing the minimized formula using the DIAMOND format
     */
    public String quineMccluskeyMinimization(/*FormulaTree formula*/)
    {
        //solvedtodo: does not terminate for ac(<120>, and(neg(or(and(neg(<345>), <347>), or(and(and(neg(<380>), <372>), iff(<345>, <347>)), or(and(and(neg(<379>), <371>), and(iff(<345>, <347>), iff(<380>, <372>))), or(and(and(neg(<378>), <370>), and(iff(<345>, <347>), and(iff(<380>, <372>), iff(<379>, <371>)))), or(and(and(neg(<377>), <369>), and(iff(<345>, <347>), and(iff(<380>, <372>), and(iff(<379>, <371>), iff(<378>, <370>))))), or(and(and(neg(<376>), <368>), and(iff(<345>, <347>), and(iff(<380>, <372>), and(iff(<379>, <371>), and(iff(<378>, <370>), iff(<377>, <369>)))))), or(and(and(neg(<375>), <367>), and(iff(<345>, <347>), and(iff(<380>, <372>), and(iff(<379>, <371>), and(iff(<378>, <370>), and(iff(<377>, <369>), iff(<376>, <368>))))))), or(and(and(neg(<374>), <366>), and(iff(<345>, <347>), and(iff(<380>, <372>), and(iff(<379>, <371>), and(iff(<378>, <370>), and(iff(<377>, <369>), and(iff(<376>, <368>), iff(<375>, <367>)))))))), and(and(neg(<373>), <365>), and(iff(<345>, <347>), and(iff(<380>, <372>), and(iff(<379>, <371>), and(iff(<378>, <370>), and(iff(<377>, <369>), and(iff(<376>, <368>), and(iff(<375>, <367>), iff(<374>, <366>)))))))))))))))))), neg(<120>)))
        //solvedtodo: formula above needs at most 34,360 billion truthtable rows ...computation would take approx. 4 years
        if( formula.getTruthTable().isEmpty() )
        {
            finTime = System.currentTimeMillis();
            finished = true;
            return formula.getTruthTableAsList().get(0).getResult() ? "true" : "false";
        }

        if( !formula.getTruthTable().isFound() ) formula.findTruthTable();

        if( formula.getErrorOccured() )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return formula.getRepresentedFormula();
        }

        //tautology and contradiction handling
        if( formula.getTruthTable().isTautology() == 0 )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return "c(v)";
        }
        else if( formula.getTruthTable().isTautology() == -1 )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return "c(f)";
        }

        if( formula.getTruthTable().getTruthTable().size() == 2 )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return formula.getFormulaAtoms().first().getName();
        }

        // keeps numbers of minterms which need to be covered later
        TreeSet<Integer> minterms = new TreeSet<Integer>();

        //only keep assignments evaluating to true
        TruthTable onlyTruesTable = new TruthTable();
        int j = 0;
        for(int i = 0; i < formula.getTruthTableAsList().size(); i++)
        {
            if( formula.getTruthTableAsList().get(i).getResult() )
            {
                onlyTruesTable.addRow(formula.getTruthTableAsList().get(i));
                Vector<Integer> cover = new Vector<Integer>();
                cover.add(j);
                onlyTruesTable.getTruthTable().get(j).setCovers(cover);
                minterms.add(formula.getTruthTableAsList().get(i).getRowNumber());
                j++;
            }
        }
        j = 0;
        onlyTruesTable.setAtomOrder(formula.getTruthTable().getAtomOrder());

        //handling for special cases, when formula has 0 or only 1 minterm
        if( onlyTruesTable.getTruthTable().size() == 0 )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return "c(f)";
        }
        else if ( onlyTruesTable.getTruthTable().size() == 1 )
        {
            String finishedFormula = "";
            int ones = 0;
            for(String s : onlyTruesTable.getTruthTable().getFirst().getAssignment()) if( s.equals("true") || s.equals("false") ) ones++;
            if( ones > 1 ) finishedFormula += "and(";

            int i = 0;
            for(String s : onlyTruesTable.getTruthTable().get(0).getAssignment())
            {
                if( s.equals("true") ) finishedFormula += onlyTruesTable.getAtomOrder().get(i);
                else if( s.equals("false") ) finishedFormula += "neg(" + onlyTruesTable.getAtomOrder().get(i) + ")";
                if( i < onlyTruesTable.getTruthTable().get(0).getAssignment().size()-1 ) finishedFormula += ",";
                i++;
            }

            if( ones > 1 ) finishedFormula += ")";
            finished = true;
            finTime = System.currentTimeMillis();
            return finishedFormula;
        }

        //builds First Quinean Table.
        TruthTable firstTable = onlyTruesTable;

        firstTable.setAtomOrder(formula.getTruthTable().getAtomOrder());
        firstTable.append(qmRecursion(onlyTruesTable));

        //build Second Quinean Table
        //TruthTable secondTable = qmChoosePrimeTerms(firstTable);
        TruthTable secondTable = qmFilterTableForPrimeTerms(firstTable);
        secondTable.setAtomOrder(firstTable.getAtomOrder());
        secondTable.setIsFound();

        //find core prime implicants and check dominance relations
        do
        {
            qmChangeIndicator = true;
            secondTable = qmCheckForRowDominance(secondTable);
            secondTable = qmCheckForColumnDominance(secondTable);
        } while( qmChangeIndicator );

        secondTable = qmChoosePrimeTerms(secondTable);
        secondTable.setAtomOrder(firstTable.getAtomOrder());

        String minimizedFormula = "";
        if( secondTable.getTruthTable().size() >= 2 ) minimizedFormula = "or(";

        j = 0;
        for(TruthTableRow row : secondTable.getTruthTable())
        {
            int ones = 0;
            for(String s : row.getAssignment()) if( s.equals("true") || s.equals("false")) ones++;

            //System.out.println("Ones = " + ones);

            if( ones > 1 ) minimizedFormula += "and(";
            int i = 0, k = 0;
            for(String s : row.getAssignment())
            {
                if( s.equals("true") )
                {
                    if( i != 0 ) minimizedFormula += ",";
                    minimizedFormula += secondTable.getAtomOrder().get(k);
                    i++;
                }

                else if( s.equals("false") )
                {
                    if( i != 0 ) minimizedFormula += ",";
                    minimizedFormula += "neg(" + secondTable.getAtomOrder().get(k) + ")";
                    i++;
                }
                k++;
            }

            if( ones > 1 ) minimizedFormula += ")";
            if( j != secondTable.getTruthTable().size()-1 )  minimizedFormula += ",";
            j++;
        }
        if( secondTable.getTruthTable().size() >= 2 ) minimizedFormula += ")";

        this.minimizedFormula = minimizedFormula;
        finTime = System.currentTimeMillis();
        finished = true;
        //System.out.println(finTime);

        return minimizedFormula;
    }

    private TruthTable qmFilterTableForPrimeTerms(TruthTable firstTable)
    {
        TruthTable primeTermTable = new TruthTable();

        for(TruthTableRow row : firstTable.getTruthTable())
        {
            if( row.getIsPrimeTerm() )
                primeTermTable.addRow(row);
        }

        return primeTermTable;
    }

    /**
     * Check whether some columns are dominated by others and scraps them.
     * @param secondTable
     * @return
     */
    private TruthTable qmCheckForColumnDominance(TruthTable secondTable) {
        if( secondTable.getTruthTable().size() == 1 ) {
            qmChangeIndicator = false;
            //System.out.println("Table to small to minimize further");
            return secondTable;
        }
        else
        {
            TruthTable reducedTable = new TruthTable();
            TransformedPrimeTable transformedPrimeTable = new TransformedPrimeTable(secondTable);
            TreeSet<Integer> scrappableMinTerms = new TreeSet<Integer>();

            for (TransformedPrimeTableRow row1 : transformedPrimeTable.getMinTerms())
                for (TransformedPrimeTableRow row2 : transformedPrimeTable.getMinTerms())
                    if ( //todo: possibly some wrong conditions in here
                            !row2.equals(row1)
                                    && !scrappableMinTerms.contains(row2.getPrimeTermNumber())      //possibly working without this as well
                                    && row2.getCoveredBy().containsAll(row1.getCoveredBy())
                                    && row2.getCoveredBy().size() > row1.getCoveredBy().size()
                            )
                        scrappableMinTerms.add(row2.getPrimeTermNumber());

            reducedTable.setIsFound();
            reducedTable.setAtomOrder(secondTable.getAtomOrder());

            for (TruthTableRow row : secondTable.getTruthTable())
            {
                Vector<Integer> cover = new Vector<Integer>();

                for (Integer i : row.getCovers())
                    if (!scrappableMinTerms.contains(i))
                        cover.add(i);

                if (cover.size() != 0)      //kick out rows that don't cover anything anymore
                    reducedTable.addRow(row);
            }

            if (reducedTable.getTruthTable().size() == secondTable.getTruthTable().size())
                qmChangeIndicator = false;

            return reducedTable;
        }
    }

    /**
     * Checks whether some rows are dominated by other rows and scraps the dominated ones.
     * @param secondTable the second Quinean Table
     * @return TruthTable containing core prime implicants only
     */
    private TruthTable qmCheckForRowDominance(TruthTable secondTable)
    {
        if( secondTable.getTruthTable().size() == 1 )
        {
            qmChangeIndicator = false;
            //System.out.println("Table to small to minimize further");
            return secondTable;
        }

        //TruthTable tableCopy = new TruthTable();

        /*int i = 0;
        for(TruthTableRow row : secondTable.getTruthTable())        //create a new table with new row numbers from 0 straight to n
        {
            TruthTableRow newRow = row;
            newRow.setRowNumber(i);
            tableCopy.addRow(newRow);
            i++;
        }*/

        TreeSet<Integer> scrappableRows = new TreeSet<Integer>();     //contains numbers of rows that can be scrapped
        for(TruthTableRow row1 : secondTable.getTruthTable())
        {
            TreeSet<Integer> coveredWithoutRow1 = new TreeSet<Integer>();
            for(TruthTableRow row2 : secondTable.getTruthTable())
                if( !row2.equals(row1) )
                    coveredWithoutRow1.addAll(row2.getCovers());

            if( coveredWithoutRow1.containsAll(row1.getCovers()) )   //row1 can be scrapped
                scrappableRows.add(row1.getRowNumber());
        }

        //scrap rows
        TruthTable reducedTable = new TruthTable();
        reducedTable.setAtomOrder(secondTable.getAtomOrder());
        reducedTable.setIsFound();
        for(TruthTableRow row : secondTable.getTruthTable())
        {
            if( !scrappableRows.contains(row.getRowNumber()) )
                reducedTable.addRow(row);
        }

        if(reducedTable.getTruthTable().size() == secondTable.getTruthTable().size())
            qmChangeIndicator = false;

        return reducedTable;
    }

    /**
     * Finds the minimal set of prime terms for QM-algorithm using a generate-and-test-algorithm. Problem is NP-complete.
     * //todo dont just test all combinations
     * @param firstTable First Quinean Table
     * @return Second Quinean Table
     */
    private TruthTable qmChoosePrimeTerms(TruthTable firstTable)
    {
        TruthTable secondTable = new TruthTable();

        ICombinatoricsVector<TruthTableRow> vector = Factory.createVector(firstTable.getTruthTable());
        Generator<TruthTableRow> generator = Factory.createSimpleCombinationGenerator(vector, firstTable.getTruthTable().size());

        int minterms[] = new int[firstTable.getTruthTable().size()];
        for(int i = 0; i < firstTable.getTruthTable().size(); i++)
            minterms[i] = firstTable.getTruthTable().get(i).getRowNumber();

        boolean firstFound = false;
        ICombinatoricsVector<TruthTableRow> smallestFoundCombination = null;
        int i = 0;
        for(ICombinatoricsVector<TruthTableRow> combination : generator)
        {
            //if( smallestFoundCombination == null ) smallestFoundCombination = combination;
            //check whether all minterms are covered
            boolean invalidCombination = false;
            Vector<Integer> completeCover = new Vector<Integer>();

            //collect all covered minterms first
            for(TruthTableRow row : combination)
            {
                completeCover.add(row.getRowNumber());
                completeCover.addAll(row.getCovers());
            }
            //check if all minterms are in the cover
            for(int j = 0; j < minterms.length; j++)
                if( !completeCover.contains(minterms[j]) ) invalidCombination = true;


            if( !invalidCombination )
            {
                //smaller than the already found minimal set?
                if( !firstFound )
                {
                    smallestFoundCombination = combination;
                    firstFound = true;
                }
                else
                if( combination.getSize() < smallestFoundCombination.getSize() )
                    smallestFoundCombination = combination;
            }
            i++;
        }

        //if( smallestFoundCombination != null) System.out.println(smallestFoundCombination.getSize());

        if( smallestFoundCombination != null )
        {
            for(TruthTableRow row : smallestFoundCombination)
                secondTable.addRow(row);
            return secondTable;
        }
        else return firstTable;
    }

    /**
     * possible to-do: reduce number of loops by classifying rows before the loops (classification by number of "true"s)
     * and only compare neighboured classes
     * @param lowOrderTable
     * @return
     */
    private TruthTable qmRecursion(TruthTable lowOrderTable)
    {
        TruthTable highOrderTable = new TruthTable();

        for(int i = 0; i < lowOrderTable.getTruthTable().size(); i++)
            for(int j = 0; j < lowOrderTable.getTruthTable().size(); j++)
            {
                //lowOrderTable.getTruthTable().get(i).getAssignment().get(0).equals(lowOrderTable.getTruthTable().get(j).getAssignment().get(0));

                if( lowOrderTable.compareRows(i, j) == 1 )
                {
                    LinkedList<String> assignment = new LinkedList<String>();

                    for(int k = 0; k < lowOrderTable.getTruthTable().getFirst().getAssignment().size(); k++)
                    {
                        if( k != lowOrderTable.getDifferentPosition(i,j) )
                            assignment.add(lowOrderTable.getTruthTable().get(i).getAssignment().get(k));
                        else
                            assignment.add("d.c.");     //don't care
                    }

                    Vector<Integer> cover = new Vector<Integer>();

                    if( lowOrderTable.getTruthTable().get(i).getCovers() != null )
                        cover.addAll(lowOrderTable.getTruthTable().get(i).getCovers());
                    else
                        cover.add(lowOrderTable.getTruthTable().get(i).getRowNumber());
                    if( lowOrderTable.getTruthTable().get(j).getCovers() != null )
                        cover.addAll(lowOrderTable.getTruthTable().get(j).getCovers());
                    else
                        cover.add(lowOrderTable.getTruthTable().get(j).getRowNumber());

                    cover.sort(new Comparator<Integer>() {
                        @Override
                        public int compare(Integer o1, Integer o2) {
                            return o1.intValue() - o2.intValue();
                        }
                    });

                    lowOrderTable.getTruthTable().get(i).setIsPrimeTerm(false);
                    lowOrderTable.getTruthTable().get(j).setIsPrimeTerm(false);

                    int number;
                    if( highOrderTable.getTruthTable().size() == 0 )
                        number = lowOrderTable.getTruthTable().getLast().getRowNumber() + 1;
                    else number = Math.max(lowOrderTable.getTruthTable().getLast().getRowNumber() + 1,
                            highOrderTable.getTruthTable().getLast().getRowNumber() + 1);

                    TruthTableRow newRow = new TruthTableRow(assignment, "true", number, cover);
                    //s++;
                    highOrderTable.addRow(newRow);          // ????
                    //lowOrderTable.addRow(newRow);         // ????
                }
            }

        lowOrderTable.append(highOrderTable);
        //if( highOrderTable.equals(lowOrderTable) ) return lowOrderTable;    //no changes, end
        if( highOrderTable.getTruthTable().size() == 0) return lowOrderTable;
        else return qmRecursion(highOrderTable);                            //tables changed, do recursion
    }

    public QMCMinimizer(FormulaTree formula)
    {
        this.formula = formula;
        minimizedFormula = "";
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    public static void test()
    {
        //Don't completly trust these tests. Minimizer can sometimes produce a formula with swapped disjunctions.
        String f1 = "ac(s,or(and(<2>,<3>), and(<4>, xor(<2>,<3>))))";
        String s1 = "or(and(<2>,<3>),and(<2>,<4>),and(<3>,<4>))";
        QMCMinimizer t1 = new QMCMinimizer(new FormulaTree(f1));
        String m1 = t1.quineMccluskeyMinimization();
        System.out.println("Control result: " + s1);
        System.out.println("Minimized formula: " + m1);
        if( s1.equals(m1) ) System.out.println("Formula 1 ..... successful.");
        else System.out.println("Formula 1 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f2 = "ac(s,and(A, or(neg(A),B)))";
        String s2 = "and(A,B)".toLowerCase();
        QMCMinimizer t2 = new QMCMinimizer(new FormulaTree(f2));
        String m2 = t2.quineMccluskeyMinimization();
        System.out.println("Control result: " + s2);
        System.out.println("Minimized formula: " + m2);
        if( s2.equals(m2) ) System.out.println("Formula 2 ..... successful.");
        else System.out.println("Formula 2 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f3 = "ac(s,or(c(f),c(v)))";
        String s3 = "c(v)";
        QMCMinimizer t3 = new QMCMinimizer(new FormulaTree(f3));
        String m3 = t3.quineMccluskeyMinimization();
        System.out.println("Control result: " + s3);
        System.out.println("Minimized formula: " + m3);
        if( s3.equals(m3) ) System.out.println("Formula 3 ..... successful.");
        else System.out.println("Formula 3 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f4 = "ac(s,and(A,B,C,neg(D)))";
        String s4 = "and(A,B,C,neg(D))".toLowerCase();
        QMCMinimizer t4 = new QMCMinimizer(new FormulaTree(f4));
        String m4 = t4.quineMccluskeyMinimization();
        System.out.println("Control result: " + s4);
        System.out.println("Minimized formula: " + m4);
        if( s4.equals(m4) ) System.out.println("Formula 4 ..... successful.");
        else System.out.println("Formula 4 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f5 = "ac(s,and(A,B,C,neg(D)))";
        String s5 = "and(A,B,C,neg(D))".toLowerCase();
        QMCMinimizer t5 = new QMCMinimizer(new FormulaTree(f5));
        String m5 = t5.quineMccluskeyMinimization();
        System.out.println("Control result: " + s5);
        System.out.println("Minimized formula: " + m5);
        if( s5.equals(m5) ) System.out.println("Formula 5 ..... successful.");
        else System.out.println("Formula 5 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f6 = "ac(s,or(a,and(A,B)))";
        String s6 = "A".toLowerCase();
        QMCMinimizer t6 = new QMCMinimizer(new FormulaTree(f6));
        String m6 = t6.quineMccluskeyMinimization();
        System.out.println("Control result: " + s6);
        System.out.println("Minimized formula: " + m6);
        if( s6.equals(m6) ) System.out.println("Formula 6 ..... successful.");
        else System.out.println("Formula 6 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f7 = "ac(s,iff(imp(A,B),imp(A,and(A,B))))";
        String s7 = "c(v)";
        QMCMinimizer t7 = new QMCMinimizer(new FormulaTree(f7));
        String m7 = t7.quineMccluskeyMinimization();
        System.out.println("Control result: " + s7);
        System.out.println("Minimized formula: " + m7);
        if( s7.equals(m7) ) System.out.println("Formula 7 ..... successful.");
        else System.out.println("Formula 7 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        // xor(xor(atom(bit(pSum(1), 3)), atom(bit(line(2), 3))), atom(carry(pSum(1), line(2), 3)))
        String f8 = "ac(s,xor(xor(A,B),C))";
        String s8 = "or(and(A,B,C),and(A,neg(B),neg(C)),and(neg(A),B,neg(C)),and(neg(A),neg(B),C))".toLowerCase();
        QMCMinimizer t8 = new QMCMinimizer(new FormulaTree(f8));
        String m8 = t8.quineMccluskeyMinimization();
        System.out.println("Control result: " + s8);
        System.out.println("Minimized formula: " + m8);
        if( s8.equals(m8) ) System.out.println("Formula 8 ..... successful.");
        else System.out.println("Formula 8 ..... wrong.");
        System.out.println("_________________________________________________________________________");


        String f9 = "ac(s, or(and(A, B), and(C, xor(A, B))))";
        String s9 = "or(and(A,B),and(A,C),and(B,C))".toLowerCase();
        KVMinimizer t9 = new KVMinimizer(new FormulaTree(f9));
        String m9 = t9.karnaughVeitchMinimization();
        System.out.println("Control result: " + s9);
        System.out.println("Minimized formula: " + m9);
        if( s9.equals(m9) ) System.out.println("Formula 9 ..... successful.");
        else System.out.println("Formula 9 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f10 = "ac(s, and(A, B))";
        String s10 = "and(A, B)".toLowerCase();
        KVMinimizer t10 = new KVMinimizer(new FormulaTree(f10));
        String m10 = t10.karnaughVeitchMinimization();
        System.out.println("Control result: " + s10);
        System.out.println("Minimized formula: " + m10);
        if( s10.equals(m10) ) System.out.println("Formula  10..... successful.");
        else System.out.println("Formula 10..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f11 = "ac(s, and(neg(iff(A, xor(xor(B, C), D))), neg(E)))";
        String s11 = "or(and(neg(A),neg(B),neg(C),D,neg(E)),and(neg(A),neg(B),C,neg(D),neg(E)),and(neg(A),B,neg(C),neg(D),neg(E)),and(neg(A),B,C,D,neg(E)),and(A,neg(B),neg(C),neg(D),neg(E)),and(A,neg(B),C,D,neg(E)),and(A,B,neg(C),D,neg(E)),and(A,B,C,neg(D),neg(E)))".toLowerCase();
        KVMinimizer t11 = new KVMinimizer(new FormulaTree(f11));
        String m11 = t11.karnaughVeitchMinimization();
        System.out.println("Control result: " + s11);
        System.out.println("Minimized formula: " + m11);
        if( s11.equals(m11) ) System.out.println("Formula  11..... successful.");
        else System.out.println("Formula 11..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f12 = "ac(s, and(neg(neg(A)), neg(B)))";
        String s12 = "and(A,neg(B))".toLowerCase();
        KVMinimizer t12 = new KVMinimizer(new FormulaTree(f12));
        String m12 = t12.karnaughVeitchMinimization();
        System.out.println("Control result: " + s12);
        System.out.println("Minimized formula: " + m12);
        if( s12.equals(m12) ) System.out.println("Formula  12..... successful.");
        else System.out.println("Formula 12..... wrong.");
        System.out.println("_________________________________________________________________________");
    }
}
