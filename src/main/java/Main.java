//import jdd.zdd.ZDD;

import jdd.bdd.BDD;
import jdd.zdd.ZDD;
import jdd.zdd.ZDDGraph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

public class Main
{
    public static void main(String[] args)
    {
        //Measuring m = new Measuring();
        //m.measurement();

        View2 v = new View2();
        //m.writeTagged();
        //FormulaTree ft = new FormulaTree("ac(s, imp(or(A, B), and(A, C, neg(D))))");
        //FormulaTree ft = new FormulaTree("ac(<120>, and(neg(or(and(neg(<345>), <347>), or(and(and(neg(<380>), <372>), iff(<345>, <347>)), or(and(and(neg(<379>), <371>), and(iff(<345>, <347>), iff(<380>, <372>))), or(and(and(neg(<378>), <370>), and(iff(<345>, <347>), and(iff(<380>, <372>), iff(<379>, <371>)))), or(and(and(neg(<377>), <369>), and(iff(<345>, <347>), and(iff(<380>, <372>), and(iff(<379>, <371>), iff(<378>, <370>))))), or(and(and(neg(<376>), <368>), and(iff(<345>, <347>), and(iff(<380>, <372>), and(iff(<379>, <371>), and(iff(<378>, <370>), iff(<377>, <369>)))))), or(and(and(neg(<375>), <367>), and(iff(<345>, <347>), and(iff(<380>, <372>), and(iff(<379>, <371>), and(iff(<378>, <370>), and(iff(<377>, <369>), iff(<376>, <368>))))))), or(and(and(neg(<374>), <366>), and(iff(<345>, <347>), and(iff(<380>, <372>), and(iff(<379>, <371>), and(iff(<378>, <370>), and(iff(<377>, <369>), and(iff(<376>, <368>), iff(<375>, <367>)))))))), and(and(neg(<373>), <365>), and(iff(<345>, <347>), and(iff(<380>, <372>), and(iff(<379>, <371>), and(iff(<378>, <370>), and(iff(<377>, <369>), and(iff(<376>, <368>), and(iff(<375>, <367>), iff(<374>, <366>)))))))))))))))))), neg(<120>)))");
        //FormulaTree ft = new FormulaTree("ac(s, or(and(A,neg(B),C,D,F),and(neg(C),neg(D)),and(A,neg(C)),and(B,neg(D))))");
        //ScherzoAlgorithms s = new ScherzoAlgorithms(0,0, ft);
        //s.minimize();
        //ScherzoAlgorithms.test();
        //QMCMinimizer.test();
        //FormulaTree ft = new FormulaTree("ac(s,xor(xor(a,b),c))");
        //QMCMinimizer q = new QMCMinimizer(ft);
        //System.out.println(q.quineMccluskeyMinimization());

        //KVMinimizer.test();
        //FormulaTree ft = new FormulaTree("ac(s,xor(xor(a,b),c))");
        //KVMinimizer k = new KVMinimizer(ft);
        //System.out.println(k.karnaughVeitchMinimization());
        //System.out.println("or(and(a,b,c),and(a,neg(b),neg(c)),and(neg(a),b,neg(c)),and(neg(a),neg(b),c))");

        //FormulaTree ft = new FormulaTree("ac(s,or(a,and(A,B)))");
        //ScherzoAlgorithms s = new ScherzoAlgorithms(0,0,ft);
        //System.out.println(s.minimize());

        //FormulaTree ft = new FormulaTree("ac(s, iff(imp(a,b),imp(a,and(a,b))))");
        //ft.printTruthTable();
        //ScherzoAlgorithms s = new ScherzoAlgorithms(0,0, ft);
        //System.out.println(s.minimize());

        // -------------------------------Example generation------------------------------------//

        /*ZDD zdd = new ZDD(100,100);
        for(int i = 0; i < 8; i++) zdd.createVar();
        int f = zdd.cubes_union("01010101 01010110 01011001 01011010 10011001 10101001");
        int P = zdd.cubes_union("00011001 10001001 01010000");

        //int P1d_1 = zdd.cubes_union("00011000 10001000");
        //int P1d_2 = zdd.cubes_union("000110 100010");

        int P1a = zdd.cubes_union("011001");
        int Pa = zdd.cubes_union("001001");
        int Pnota = zdd.cubes_union("010000");

        zdd.printDot("P1a", P1a);
        zdd.printDot("Pnota", Pnota);
        zdd.printDot("Pa", Pa);

        zdd.printDot("f example", f);
        zdd.printDot("P example", P);

        //zdd.printDot("P1d_1", P1d_1);
        //zdd.printDot("P1d_2", P1d_2);

        BDD bdd = new BDD(100,100);
        Vector<Integer> bddVariables = new Vector<Integer>();
        for(int i = 0; i < 8; i++) bddVariables.add(bdd.createVar());
        int tmp1 = bdd.or(bddVariables.get(0), bddVariables.get(1));
        bdd.ref(tmp1);
        int tmp2 = bdd.and(bddVariables.get(0), bddVariables.get(2));
        bdd.ref(tmp2);
        int tmp3 = bdd.not(bddVariables.get(3));
        bdd.ref(tmp3);
        int tmp4 = bdd.and(tmp2, tmp3);
        bdd.ref(tmp4);
        bdd.deref(tmp2); bdd.deref(tmp3);
        int function = bdd.imp(tmp1, tmp4);
        bdd.ref(function);
        bdd.deref(tmp1); bdd.deref(tmp4);

        bdd.printDot("BDD f", function);*/

        // -------------------------------------------------------------------------------------//

        /*BDD bdd = new BDD(100,100);

        int a = bdd.createVar();
        int b = bdd.createVar();
        int c = bdd.createVar();
        int d = bdd.createVar();

        int t1, t2, t3, t4, t5, t6, t7, t8;
        t1 = bdd.ref(bdd.and(b, bdd.not(d)));
        t2 = bdd.ref( bdd.and(a, bdd.and(c, d)));
        t3 = bdd.ref( bdd.and(bdd.not(a), bdd.and(c, d)));
        t4 = bdd.ref( bdd.and(bdd.and(bdd.not(a), b), bdd.and(bdd.not(c), d)));
        t5 = bdd.ref( bdd.or(bdd.or(t1, t2), bdd.or(t3,t4)));
        bdd.deref(t1); bdd.deref(t2); bdd.deref(t3); bdd.deref(t4);

        bdd.printDot("t5", t5);*/

        /*ZDD zdd = new ZDD(100,100);
        int aP = zdd.createVar();
        int aM = zdd.createVar();
        int bP = zdd.createVar();
        int bM = zdd.createVar();

        //int t = zdd.cubes_union("1001 1000 0101 0100 0001");
        int t1 = zdd.cubes_union("0101 0001 0100");
        int t2 = zdd.cubes_union("0101 0110 1001");
        zdd.printDot("0101 0110 1001", t2);
*/
        /*FormulaTree ft = new FormulaTree("ac(s,or(neg(b),neg(a),and(neg(a),neg(b)),a,and(a,neg(b))))");
        ft.findTruthTable();
        int u = zdd.cubes_union(ft.getTruthTable().toZDDString());
        zdd.printDot("u",u);*/

        /*ZDDGraph zdd = new ZDDGraph(1000,500);
        int a = zdd.createVar();
        int b = zdd.createVar();
        int c = zdd.createVar();
        int d = zdd.createVar();

        int p = zdd.cubes_union("1011 1111 0101 0001 0100 0110");

        zdd.printCubes(p);
        zdd.print(p);*/

        /*String[] run = {"python3", "/home/kevin/Schreibtisch/diamond/diamond.py", "-g", "/home/kevin/adfs/05b.19x31is589.adf" };
        try {
            //ProcessBuilder builder = new ProcessBuilder("python3", "~/Desktop/diamond/diamond.py", "-grd", "/home/kevin/adfs/05b.19x31is589.adf");
            //Process p = Runtime.getRuntime().exec("python3 ~/Desktop/diamond/diamond.py -grd /home/kevin/adfs/05b.19x31is589.adf");
            //Process p = builder.start();
            Process p = Runtime.getRuntime().exec(run);

            BufferedReader outReader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));

            StringBuilder b = new StringBuilder();
            String str = "";
            while ((str = outReader.readLine()) != null) {
                System.out.println(str);
                b.append(str);
                b.append(System.getProperty("line.separator"));
            }
            String output = b.toString();
            //System.out.println(output);

            System.out.println("Time -g ADF: " + Measuring.parseDiamondOutputForTime(output) + System.getProperty("line.separator"));
        } catch(IOException e ) { e.printStackTrace(); }*/

        /*
        ac(atom(carry(line(0), line(1), 3)), or(and(atom(bit(line(0), 2)), atom(bit(line(1), 2))), and(atom(carry(line(0), line(1), 2)), xor(atom(bit(line(0), 2)), atom(bit(line(1), 2)))))).
        ac(a, or(and(b,c), and(d, xor(b,c))).
         */

        //View v = new View();

        //imp(or(c(v), B), and(B, neg(C), D))

        //FormulaTree ft=new FormulaTree("ac(<1>, or(c(f),c(v))).");
        //ft.printTree();
        //ft.findTruthTable();
        //ft.printTruthTable();
        //ft.getTruthTable().print();
        //System.out.println("Atoms: " + ft.getFormulaAtoms().size());
        //System.out.println(FormulaMinimizer.quineMccluskeyMinimization(ft));
        //ft.getFormulaAtoms().toString();
        //ft.printTree();
        //ft.printFormulaAtoms();

        //boolean[] a = {false,false,false,false};
        //ft.evaluateFormula(a);
        //ft.findTruthTable();
        //ft.printTruthTable();
        //System.out.println("Formula value: " + ft.getTreeRoot().getValue());

        //FormulaMinimizer mini = new FormulaMinimizer();
        //System.out.println(mini.quineMccluskeyMinimization(ft));
        /*try { Thread.sleep(150000); }
        catch (InterruptedException e) { e.printStackTrace(); }*/

        /*Vector<String> a1 = new Vector<String>();
        a1.add("true");
        a1.add("false");
        a1.add("false");
        a1.add("true");
        kvHyperCubeNode node1 = new kvHyperCubeNode(a1, true);

        Vector<String> a2 = new Vector<String>();
        a2.add("false");
        a2.add("true");
        a2.add("true");
        a2.add("false");
        kvHyperCubeNode node2 = new kvHyperCubeNode(a2, true);*/

        //System.out.println(node1.getDifferenceCounter(node2));

        /*Vector<String> vars = new Vector<String>();
        vars.add("a");
        vars.add("b");
        vars.add("c");
        vars.add("d");

        System.out.println("Formula: " + FormulaGenerator.generateFormula(vars, 3));
        System.out.println("Formula: " + FormulaGenerator.generateFormula(vars, 3));
        System.out.println("Formula: " + FormulaGenerator.generateFormula(vars, 3));
        System.out.println("Formula: " + FormulaGenerator.generateFormula(vars, 3));
        System.out.println("Formula: " + FormulaGenerator.generateFormula(vars, 3));*/

        //View v = new View();

        /*String s = "ab(<1>)ab<2>";
        CharSequence target = "<1>";
        CharSequence replacement = "<2>";

        System.out.println(s.replace(target, replacement));*/

        //FormulaTree ft = new FormulaTree("ac(s, xnor(a,b,c))");
        //FormulaTree ft = new FormulaTree("ac(s, imp(or(A, B), and(B, neg(C), D)))");
        //FormulaTree ft2 = new FormulaTree(FormulaTree.testFormula6); //
        //FormulaTree ft2 = new FormulaTree(FormulaTree.testFormula5); //
        //FormulaTree ft2 = new FormulaTree(FormulaTree.testFormula2);
        //FormulaTree ft = new FormulaTree(FormulaTree.testFormula5);
        //String minFormula = FormulaMinimizer.quineMccluskeyMinimization(ft);
        //System.out.println(minFormula);
        //ft2.findTruthTable();
        //ft.getTruthTable().print();

        //System.out.println(FormulaMinimizer.quineMccluskeyMinimization(ft));
        //System.out.println(FormulaMinimizer.alterQMcC(ft2));

        //BinaryDecisionTree tree = new BinaryDecisionTree(ft);
        //tree.print();

        //System.out.println(FormulaMinimizer.espressoMinimization(ft));

        /*
        String parsedCubes = "";

        for(TruthTableRow row : ft.getTruthTable().getTruthTable())
        {
            for(String s : row.getAssignment())
            {
                if(s.equals("true")) parsedCubes += "1";
                else if(s.endsWith("false")) parsedCubes += "0";
            }
            parsedCubes += " ";
        }

        ZDD zdd = new ZDD(2000,100);
        int v1 = zdd.createVar();
        int v2 = zdd.createVar();

        int a = zdd.empty();
        int b = zdd.base();
        int c = zdd.change(b, v1);
        int d = zdd.change(b, v2);
        int e = zdd.union(c,d);
        int f = zdd.union(b,e);
        int g = zdd.diff(f,c);

        int var1 = zdd.cubes_union(parsedCubes);

        zdd.printDot("D:/test/test3.txt", var1);
        */

        //Measuring.measure();
        /*int[][] arr = new int[2][4];
        arr[0][0] = 0;
        arr[0][1] = 1;
        arr[0][2] = 2;
        arr[0][3] = 3;
        arr[1][0] = 4;
        arr[1][1] = 5;
        arr[1][2] = 6;
        arr[1][3] = 7;

        for(int i = 0; i<2;i++)
        {
            for(int j = 0; j<4; j++)
                System.out.print(arr[i][j] + " ");
            System.out.println();
        }*/

    }
}
