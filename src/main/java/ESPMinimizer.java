import java.io.*;

public class ESPMinimizer extends MinimizerTemplate implements Runnable
{
    @Override
    public void run()
    {
        finished = false;
        startTime = System.currentTimeMillis();
        espressoMinimization();
    }

    public String espressoMinimization()
    {
        if( !formula.getTruthTable().isFound() ) formula.findTruthTable();

        if( formula.getErrorOccured() )
        {
            finished = true;
            return formula.getRepresentedFormula();
        }

        if( formula.getTruthTable().isTautology() == 0 )
        {
            finished = true;
            return "c(v)";
        }
        else if( formula.getTruthTable().isTautology() == -1 )
        {
            finished = true;
            return "c(f)";
        }

        String result = "";
        try
        {
            //transform formula into espresso's format and write it to a file
            File f = File.createTempFile("tmp", "tmp");
            f.deleteOnExit();
            BufferedWriter writer = new BufferedWriter(new FileWriter(f));
            String s = espConverter(formula);
            writer.write(espConverter(formula));
            writer.close();

            //run espresso
            Process p = Runtime.getRuntime().exec("C:\\Users\\Kevin\\Studium\\Bachelorarbeit\\espresso\\espresso.exe -Dexact " + f.getAbsolutePath());

            //get result
            try { p.waitFor(); }
            catch (InterruptedException e)
            {
                System.err.println("Espresso was interrupted.");
                return formula.getRepresentedFormula();
            }
            String str = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while((str = reader.readLine()) != null)
                result += str + "\n";

            BufferedReader errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            while(errorReader.readLine() != null) System.out.println(errorReader.readLine());
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.err.println("IOException during espresso minimization.");
            return formula.getRepresentedFormula();
        }

        //build output formula
        String minimizedFormula = "";
        String p = "";
        for(String s : result.split("\\n"))
            if(s.startsWith(".p "))
            {
                p = s.substring(3);
                if( Integer.parseInt(p) == 1 ) ;
                break;
            }

        if(Integer.parseInt(p) == 1)
        {
            /* ... */
            for(String s : result.split("\\n"))
            {
                if( !s.startsWith(".") )
                {
                    int dcs = 0;
                    int vars = 0;

                    for(char c : s.toCharArray())
                    {
                        if(c == '-') dcs++;
                        else if( c == '0' || c == '1') vars++;
                        else if( c == ' ' ) break;
                    }

                    int i = 0;
                    minimizedFormula += "and(";
                    /*for(char c : s.toCharArray())
                    {
                        if( c == '0' )
                            minimizedFormula += "neg(" + formula.getTruthTable().getAtomOrder().get(i) + ")";
                        else if( c == '1' )
                            minimizedFormula += formula.getTruthTable().getAtomOrder().get(i);
                        else if( c == '-' )
                        {
                            //do nothing
                        }
                        if( i < s.length()-1 ) minimizedFormula += ",";
                        i++;
                    }*/

                    for(char c : s.toCharArray())
                    {
                        if( c == ' ' ) break;

                        if( c == '0' )
                        {
                            minimizedFormula += "neg(" + formula.getTruthTable().getAtomOrder().get(i) + ")";
                            i++;
                        }
                        else if( c == '1' )
                        {
                            minimizedFormula += formula.getTruthTable().getAtomOrder().get(i);
                            i++;
                        }
                        else if( c == '-' )
                        {
                            //do nothing
                        }
                        if( i < vars
                                && minimizedFormula.toCharArray()[minimizedFormula.length()-1] != '(' )
                            minimizedFormula += ",";
                        //i++;
                    }

                    minimizedFormula += ")";
                    return minimizedFormula;
                }
            }
        }
        else if(Integer.parseInt(p) == 0)
            return "c(f)";

        minimizedFormula += "or(";
        int j = 0;
        for(String s : result.split("\\n"))
        {
            if( !s.startsWith(".") )
            {
                int dcs = 0;
                int vars = 0;

                for(char c : s.toCharArray())
                {
                    if(c == '-') dcs++;
                    else if( c == '0' || c == '1') vars++;
                    else if( c == ' ' ) break;
                }

                int i = 0;
                minimizedFormula += "and(";
                for(char c : s.toCharArray())
                {
                    if( c == ' ' ) break;

                    if( c == '0' )
                    {
                        minimizedFormula += "neg(" + formula.getTruthTable().getAtomOrder().get(i) + ")";
                        i++;
                    }
                    else if( c == '1' )
                    {
                        minimizedFormula += formula.getTruthTable().getAtomOrder().get(i);
                        i++;
                    }
                    else if( c == '-' )
                    {
                        //do nothing
                    }
                    if( i < vars
                            && minimizedFormula.toCharArray()[minimizedFormula.length()-1] != '(' )
                        minimizedFormula += ",";
                    //i++;
                }
                minimizedFormula += ")";
                if( j < (Integer.parseInt(p)-1) ) minimizedFormula += ",";
                j++;
            }
        }
        minimizedFormula += ")";


        this.minimizedFormula = minimizedFormula;
        finTime = System.currentTimeMillis();
        finished = true;

        return minimizedFormula;

    }

    private static String espConverter(FormulaTree formula)
    {
        String espressoFormula = "";

        espressoFormula += ".i " + formula.getFormulaAtoms().size() + "\n";
        espressoFormula += ".o 1" + "\n";
        //specify options
        /* ... */

        espressoFormula += ".lib ";
        int i = 0; int j = formula.getFormulaAtoms().size();
        for(PropAtom a : formula.getFormulaAtoms())
        {
            if( i != j-1 ) espressoFormula += a.getName() + " ";
            else espressoFormula += a.getName();
        }
        espressoFormula += "\n";

        for(TruthTableRow row : formula.getTruthTableAsList())
        {
            for(String s : row.getAssignment())
            {
                if( s.equals("true") ) espressoFormula += "1";
                else if( s.equals("false") ) espressoFormula += "0";
                else if( s.equals("d.c.") ) espressoFormula += "-";
            }
            if( row.getResult() ) espressoFormula += " 1\n";
            else espressoFormula += " 0\n";
        }

        espressoFormula += ".e\n";

        return espressoFormula;
    }

    public ESPMinimizer(FormulaTree formula)
    {
        this.formula = formula;
        minimizedFormula = "";
    }
}
