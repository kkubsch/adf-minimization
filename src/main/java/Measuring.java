import java.io.*;
import java.util.Vector;

public class Measuring extends Thread
{
    private Vector<FormulaTree> formulae;
    private Vector<String> minimizedFormulaeQM;
    private Vector<String> minimizedFormulaeKV;
    private Vector<String> minimizedFormulaeESP;
    private Vector<String> minimizedFormulaeSCH;
    private String completeADF;
    private Vector<String> statements;
    private Vector<String> substitutes;
    private final static String adfDirectory = "//home/kevin/ADFsMessungen";
    private final static String logDirectory = "//media/kevin/1E0C-4945/Results";
    private final static String resultsDirectory = "//media/kevin/1E0C-4945/Results/ADFs";
    private final static String tempDirectory = "/home/kevin/measuring";
    private final static String minAdfDirectory = "/home/kevin/ScherzoResults";

    //sudo cp /source/path /media/kevin/1E0C-4945/Results/ADFs

    public Measuring()
    {
        formulae = new Vector<FormulaTree>();
        minimizedFormulaeQM = new Vector<String>();
        minimizedFormulaeKV = new Vector<String>();
        minimizedFormulaeESP = new Vector<String>();
        minimizedFormulaeSCH = new Vector<String>();
        completeADF = new String();
        statements = new Vector<String>();
        substitutes = new Vector<String>();
    }

    /**
     * Measures diamond times only.
     */
    public void measureDiamondTimes()
    {
        try
        {
            File adfDir = new File(adfDirectory);
            File[] adfs = adfDir.listFiles();
            File minDir = new File(minAdfDirectory);
            File[] minAdfs = minDir.listFiles();

            BufferedWriter timesWriter = new BufferedWriter(new FileWriter(new File(tempDirectory + "/times.log")));
            //BufferedWriter logger = new BufferedWriter(new FileWriter(new File(tempDirectory + "/log.log")));

            String[] cmd = new String[]{"-g", "-c", "-p", "-a", "-sm"};

            for (int i=0; i<adfs.length; i++)
            {
                for(int l = 0; l<minAdfs.length; l++)
                {
                    String s = minAdfs[l].getName().substring(0, minAdfs[l].getName().length() - 9);
                    //if(i == 0)
                    //{
                    //    System.out.println(adfs[i].getName());
                    //    System.out.println(s);
                    //    System.out.println("---------------");
                    //}
                    if( !adfs[i].getName().equals(s) ) continue;
                    //else System.out.println("Yay");
                    timesWriter.write(adfs[i].getName() + System.getProperty("line.separator"));
                    System.out.println("Measuring: " + adfs[i].getName() + "  and  " + minAdfs[l].getName());
                    for(int j = 0; j <= 4; j++)
                    {
                        //System.out.println("doing stuff");

                        //String path = "\"".concat(adfs[i].getAbsolutePath()).concat("\"");
                        String[] run = {"python3", "/home/kevin/Schreibtisch/diamond/diamond.py", cmd[j], adfs[i].getAbsolutePath()};
                        Process p = Runtime.getRuntime().exec(run);
                        BufferedReader outReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                        StringBuilder b = new StringBuilder();
                        String str = "";
                        while ( (str = outReader.readLine()) != null ){
                            b.append(str);
                            b.append(System.getProperty("line.separator"));
                        }
                        timesWriter.write("Time " + cmd[j] + " ADF: " + parseDiamondOutputForTime(b.toString()) + System.getProperty("line.separator"));

                        //path = new String("\"".concat(minAdfs[l].getAbsolutePath()).concat("\""));
                        String[] run2 = {"python3", "/home/kevin/Schreibtisch/diamond/diamond.py", cmd[j], minAdfs[l].getAbsolutePath()};
                        Process p2 = Runtime.getRuntime().exec(run2);
                        BufferedReader outReader2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
                        StringBuilder b2 = new StringBuilder();
                        String str2 = "";
                        while ( (str2 = outReader2.readLine()) != null ){
                            b2.append(str2);
                            b2.append(System.getProperty("line.separator"));
                        }
                        //System.out.println(b2.toString());
                        timesWriter.write("Time " + cmd[j] + " minimized ADF: " + parseDiamondOutputForTime(b2.toString()) + System.getProperty("line.separator"));


                        outReader.close();
                        outReader2.close();
                    }
                    timesWriter.write(System.getProperty("line.separator"));
                }
            }
            timesWriter.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * logs the following times for each of the given ADFs:
     * time needed for minimization   |  solving time unminimized ADF  |  solving time minimized ADF
     *  KV | QM | Espresso | SCHERZO  |  grounded|complete|preferred   |  grounded|complete|preferred
     */
    public void measurement()
    {
        //String completeADF;
        //File adfDir = new File("C:\\Users\\Kevin\\Studium\\Bachelorarbeit\\ADF\\ADFs");
        File adfDir = new File(adfDirectory);
        File[] adfs = adfDir.listFiles();

        try
        {
            //BufferedWriter timesWriter = new BufferedWriter(new FileWriter(new File("U:/Measuring/times.log")));
            BufferedWriter timesWriter = new BufferedWriter(new FileWriter(new File(tempDirectory + "/times.log")));
            BufferedWriter logger = new BufferedWriter(new FileWriter(new File(tempDirectory + "/log.log")));

            for(int i = 0; i < adfs.length; i++)
            {
                System.out.println("minimizing adf nr." + i);
                logger.write(System.getProperty("line.separator") + adfs[i].getName() + "    ");
                logger.flush();
                //prerequisites
                formulae = null;
                this.completeADF = new String();

                BufferedReader reader = new BufferedReader(new FileReader(adfs[i]));
                String s = reader.readLine();
                while(s != null)
                {
                    completeADF += s;
                    s = reader.readLine();
                }
                reader.close();

                //System.out.println(completeADF);

                completeADF.trim();

                //BufferedWriter resultsWriter = new BufferedWriter(
                //        new FileWriter(new File("U:/Measuring/results" + adfs[i].getName() + ".log")));

                //resultsWriter.write(adfs[i].getName() + "\n");
                timesWriter.write( System.getProperty("line.separator") + adfs[i].getName() + System.getProperty("line.separator"));

                processStatements();

                System.out.println("Statements processed.");

                //formulae = FormulaReader.readADFFromFile(adfs[i]);

                formulae = buildFormulas();

                System.out.println("ADF read.");

                //time needed for minimization
                long completeTimeKV = 0, completeTimeQM = 0, completeTimeSCH = 0, completeTimeESP = 0;

                int k = 0;
                for(FormulaTree tree : formulae)
                {
                    System.out.print("Minimizing formula ... #" + k + " " + "of " + formulae.size());
                    //System.out.println(tree.getRepresentedFormula());
                    for(int j = 0; j < 4; j++)
                    {
                        long t1 = System.currentTimeMillis();
                        if (j == 0)
                        {
                            //minimizedFormulaeQM.add(FormulaMinimizer.quineMccluskeyMinimization(tree));
                            //long t2 = System.currentTimeMillis();
                            //completeTimeQM += t2 - t1;
                            QMCMinimizer m = new QMCMinimizer(tree);
                            Thread t = new Thread(m);
                            t.start();
                            long start = System.currentTimeMillis();

                            long waited = 0;
                            while( t.isAlive() && waited < 300000)
                            {
                                try{ Thread.sleep(10); }
                                catch(InterruptedException e) { e.printStackTrace(); }
                                waited += 10;
                            }
                            if( waited < 300000 )
                            {
                                minimizedFormulaeQM.add(m.getMinimizedFormula());
                                completeTimeQM += /*m.getComputationTime();*/(System.currentTimeMillis() - start);
                            }
                            else
                            {
                                minimizedFormulaeQM.add(tree.getRepresentedFormula());
                                logger.write("Formula #" + k + " couldn't be qm-minimized in under 300s.");
                                t.interrupt();
                                completeTimeQM += 300000;
                            }
                            //System.out.println(m.finTime + " - " + m.startTime);
                            //completeTimeQM += m.getComputationTime();

                        }
                        else if (j == 1)
                        {
                            //minimizedFormulaeKV.add(FormulaMinimizer.karnaughVeitchMinimization(tree));
                            //long t2 = System.currentTimeMillis();
                            //completeTimeKV += t2 - t1;
                            KVMinimizer m = new KVMinimizer(tree);
                            Thread t = new Thread(m);
                            t.start();
                            long start = System.currentTimeMillis();

                            long waited = 0;
                            while( t.isAlive() && waited < 300000/*1000*/ )
                            {
                                try{ Thread.sleep(10); }
                                catch(InterruptedException e) { e.printStackTrace(); }
                                waited += 10;
                            }
                            if( waited < 300000/*1000*/ )
                            {
                                completeTimeKV += /*m.getComputationTime();*/(System.currentTimeMillis() - start);
                                minimizedFormulaeKV.add(m.getMinimizedFormula());
                            }
                            else
                            {
                                minimizedFormulaeKV.add(tree.getRepresentedFormula());
                                logger.write("Formula #" + k + " couldn't be kv-minimized in under 300s.");
                                t.interrupt();
                                completeTimeKV += 300000/*1000*/;
                            }
                            System.out.println(m.finTime + " - " + m.startTime);
                            //completeTimeKV += m.getComputationTime();
                        }
                        else if (j == 2)
                        {
                            //minimizedFormulaeESP.add(FormulaMinimizer.espressoMinimization(tree));
                            //completeTimeESP += t2 - t1;
                            /*ESPMinimizer m = new ESPMinimizer(tree);
                            Thread t = new Thread(m);
                            t.start();
                            long start = System.currentTimeMillis();

                            long waited = 0;
                            while( t.isAlive() && waited < 300000)
                            {
                                try{ Thread.sleep(100); }
                                catch(InterruptedException e) { e.printStackTrace(); }
                                waited += 100;
                            }
                            if( waited < 300000 )
                            {
                                minimizedFormulaeESP.add(m.getMinimizedFormula());
                                completeTimeESP + = m.getComputationTime(); //(System.currentTimeMillis() - start);
                            }
                            else
                            {
                                minimizedFormulaeESP.add(tree.getRepresentedFormula());
                                logger.write("Formula #" + k + " couldn't be espresso-minimized in under 300s.");
                                t.interrupt();
                                completeTimeESP += //300000 //1000;
                            }
                            //completeTimeESP += m.getComputationTime();*/
                        }
                        else if (j == 3)
                        {
                            //minimizedFormulaeSCH.add(FormulaMinimizer.scherzoMinimization(tree));
                            //long t2 = System.currentTimeMillis();
                            //completeTimeSCH += t2 - t1;
                            SCHMinimizer m = new SCHMinimizer(tree);
                            Thread t = new Thread(m);
                            t.start();
                            long start = System.currentTimeMillis();

                            long waited = 0;
                            while( t.isAlive() && waited < 300000)
                            {
                                try{ Thread.sleep(10); }
                                catch(InterruptedException e) { e.printStackTrace(); }
                                waited += 10;
                            }
                            if( waited < 300000 )
                            {
                                minimizedFormulaeSCH.add(m.getMinimizedFormula());
                                completeTimeSCH += /*m.getComputationTime();*/(System.currentTimeMillis() - start);
                            }
                            else
                            {
                                minimizedFormulaeSCH.add(tree.getRepresentedFormula());
                                logger.write("Formula #" + k + " couldn't be scherzo-minimized in under 300s.");
                                t.interrupt();
                                completeTimeSCH += 300000; //1000;
                            }
                            //completeTimeSCH += m.getComputationTime();
                        }
                    }
                    //System.out.println("   ... done");
                    System.out.print("Finished formula ... #" + k + " " + "of " + formulae.size());
                    k++;
                }
                timesWriter.write("QM:   " + completeTimeQM + "ms" + System.getProperty("line.separator"));
                timesWriter.write("KV:   " + completeTimeKV + "ms" + System.getProperty("line.separator"));
                //timesWriter.write("Esp:  " + completeTimeESP + "\n");
                timesWriter.write("SCH:  " + completeTimeSCH + "ms" + System.getProperty("line.separator"));

                /*resultsWriter.write("QM result\n======================================");
                resultsWriter.write(constructExportFormula(minimizedFormulaeQM));
                resultsWriter.write("KV result\n======================================");
                resultsWriter.write(constructExportFormula(minimizedFormulaeKV));
                resultsWriter.write("Esp result\n======================================");
                resultsWriter.write(constructExportFormula(minimizedFormulaeESP));
                resultsWriter.write("SCH result\n======================================");
                resultsWriter.write(constructExportFormula(minimizedFormulaeSCH));
                */

                //write results
                BufferedWriter resultsWriter;
                ///home/kevin/measuring
                /*File qmResultFile = new File("U:/Measuring/" + adfs[i].getName() + "qmResult");
                File kvResultFile = new File("U:/Measuring/" + adfs[i].getName() + "kvResult");
                File espResultFile = new File("U:/Measuring/" + adfs[i].getName() + "espResult");
                File schResultFile = new File("U:/Measuring/" + adfs[i].getName() + "schResult");*/
                File qmResultFile = new File(tempDirectory + "/" + adfs[i].getName() + "qmResult");
                File kvResultFile = new File(tempDirectory + "/" + adfs[i].getName() + "kvResult");
                File schResultFile = new File(tempDirectory + "/" + adfs[i].getName() + "schResult");
                //File espResultFile = new File("home/kevin/measuring/" + adfs[i].getName() + "espResult");
                //File schResultFile = new File("home/kevin/measuring/" + adfs[i].getName() + "schResult");

                //if( !qmResultFile.exists() ) qmResultFile.



                resultsWriter = new BufferedWriter(new FileWriter(qmResultFile));
                for(String str : constructExportFormula(minimizedFormulaeQM).split("\\s"))
                {
                    resultsWriter.write(str + "\n");
                    resultsWriter.flush();
                }

                //resultsWriter.write(constructExportFormula(minimizedFormulaeQM));

                resultsWriter = new BufferedWriter(new FileWriter(kvResultFile));

                for(String str : constructExportFormula(minimizedFormulaeKV).split("\\s"))
                {
                    resultsWriter.write(str + "\n");
                    resultsWriter.flush();
                }

                resultsWriter = new BufferedWriter(new FileWriter(schResultFile));

                for(String str : constructExportFormula(minimizedFormulaeSCH).split("\\s"))
                {
                    resultsWriter.write(str + "\n");
                    resultsWriter.flush();
                }
                //resultsWriter.write(constructExportFormula(minimizedFormulaeKV));
                //resultsWriter = new BufferedWriter(new FileWriter(espResultFile));
                //resultsWriter.write(constructExportFormula(minimizedFormulaeESP));
                //resultsWriter = new BufferedWriter(new FileWriter(schResultFile));
                //resultsWriter.write(constructExportFormula(minimizedFormulaeSCH));

                //solving time unminimized ADF
                //String[] run = {"python3", "/home/kevin/Schreibtisch/diamond/diamond.py", "-g", adfs[i].getAbsolutePath() };
                //Process p = Runtime.getRuntime().exec(/*"python3 ~/Desktop/diamond/diamond.py -g" + adfs[i].getAbsolutePath()*/run);
                //BufferedReader outReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                //StringBuilder b = new StringBuilder();
                //String str = "";
                //while ( (str = outReader.readLine()) != null)
                //{
                //    b.append(str);
                //    b.append(System.getProperty("line.separator"));
                //}
                //timesWriter.write("Time -g ADF: " + parseDiamondOutputForTime(b.toString()) + System.getProperty("line.separator"));

                //String[] run6 = {"python3", "/home/kevin/Schreibtisch/diamond/diamond.py", "-c", adfs[i].getAbsolutePath() };
                //Process q6 = Runtime.getRuntime().exec(/*"python3 ~/Desktop/diamond/diamond.py -g" + adfs[i].getAbsolutePath()*/run6);
                //outReader = new BufferedReader(new InputStreamReader(q6.getInputStream()));
                //b = new StringBuilder();
                //str = "";
                //while ( (str = outReader.readLine()) != null)
                //{
                //    b.append(str);
                //    b.append(System.getProperty("line.separator"));
                //}
                //timesWriter.write("Time -c ADF: " + parseDiamondOutputForTime(b.toString()) + System.getProperty("line.separator"));

                //String[] run5 = {"python3", "/home/kevin/Schreibtisch/diamond/diamond.py", "-p", adfs[i].getAbsolutePath() };
                //Process q5 = Runtime.getRuntime().exec(/*"python3 ~/Desktop/diamond/diamond.py -g" + adfs[i].getAbsolutePath()*/run5);
                //outReader = new BufferedReader(new InputStreamReader(q5.getInputStream()));
                //b = new StringBuilder();
                //str = "";
                //while ( (str = outReader.readLine()) != null)
                //{
                //    b.append(str);
                //    b.append(System.getProperty("line.separator"));
                //}
                //timesWriter.write("Time -p ADF: " + parseDiamondOutputForTime(b.toString()) + System.getProperty("line.separator"));


                //timesWriter.write("Time -g ADF: " + (t2-t1) + "ms" + System.getProperty("line.separator"));

                //solving time minimized ADF
                //String[] run2 = {"python3", "/home/kevin/Schreibtisch/diamond/diamond.py", "-g", schResultFile.getAbsolutePath() };
                //Process q = Runtime.getRuntime().exec(/*"python3 ~/Desktop/diamond/diamond.py -g" + adfs[i].getAbsolutePath()*/run2);
                //outReader = new BufferedReader(new InputStreamReader(q.getInputStream()));
                //b = new StringBuilder();
                //str = "";
                //while ( (str = outReader.readLine()) != null)
                //{
                //    b.append(str);
                //    b.append(System.getProperty("line.separator"));
                //}
                //timesWriter.write("Time -g ADF minimized: " + parseDiamondOutputForTime(b.toString()) + System.getProperty("line.separator"));

                //String[] run3 = {"python3", "/home/kevin/Schreibtisch/diamond/diamond.py", "-c", schResultFile.getAbsolutePath() };
                //Process q3 = Runtime.getRuntime().exec(/*"python3 ~/Desktop/diamond/diamond.py -g" + adfs[i].getAbsolutePath()*/run3);
                //outReader = new BufferedReader(new InputStreamReader(q3.getInputStream()));
                //b = new StringBuilder();
                //str = "";
                //while ( (str = outReader.readLine()) != null)
                //{
                //    b.append(str);
                //    b.append(System.getProperty("line.separator"));
                //}
                //timesWriter.write("Time -c ADF minimized: " + parseDiamondOutputForTime(b.toString()) + System.getProperty("line.separator"));

                //String[] run4 = {"python3", "/home/kevin/Schreibtisch/diamond/diamond.py", "-p", schResultFile.getAbsolutePath() };
                //Process q4 = Runtime.getRuntime().exec(/*"python3 ~/Desktop/diamond/diamond.py -g" + adfs[i].getAbsolutePath()*/run4);
                //outReader = new BufferedReader(new InputStreamReader(q4.getInputStream()));
                //b = new StringBuilder();
                //str = "";
                //while ( (str = outReader.readLine()) != null)
                //{
                //    b.append(str);
                //    b.append(System.getProperty("line.separator"));
                //}
                //timesWriter.write("Time -p ADF minimized: " + parseDiamondOutputForTime(b.toString()) + System.getProperty("line.separator"));


                if( !(/*minimizedFormulaeSCH.equals(minimizedFormulaeESP) && minimizedFormulaeESP.equals(minimizedFormulaeKV)
                    &&*/ minimizedFormulaeKV.equals(minimizedFormulaeQM)) )
                {
                    timesWriter.write("ERROR: resulting formulas do not match! Grounding times can be wrong!\n");
                    logger.write("Err: resulting formulas do not match! Grounding times can be wrong!\n");
                }

                //sudo cp /source/path /media/kevin/1E0C-4945/Results/ADFs

                //String[] copy = {/*"sudo",*/ "cp", kvResultFile.getAbsolutePath(), "/media/kevin/1E0C-4945/Results/ADFs"};
                //Process kv = Runtime.getRuntime().exec(copy);

                //copy = new String[] { "cp", qmResultFile.getAbsolutePath(), "/media/kevin/1E0C-4945/Results/ADFs"};
                //Process qm = Runtime.getRuntime().exec(copy);

                /*copy = new String[]  { "cp", espResultFile.getAbsolutePath(), "/media/kevin/1E0C-4945/Results/ADFs"};
                Process esp = Runtime.getRuntime().exec(copy);*/

                //copy = new String[]  { "cp", schResultFile.getAbsolutePath(), "/media/kevin/1E0C-4945/Results/ADFs"};
                //Process sch = Runtime.getRuntime().exec(copy);

                //String[] delete = {"sudo", "rm", "/source/path"};
                //Process pr = Runtime.getRuntime().exec(delete);
                //kvResultFile.delete();
                //qmResultFile.delete();
                //espResultFile.delete();
                //schResultFile.delete();

                timesWriter.flush();
                logger.flush();
                resultsWriter.close();
                //outReader.close();

                System.out.println("Finished ADF nr." + i + ".");


                //cleanup
                for(FormulaTree f : formulae) f.cleanup();

            }
            timesWriter.close();
            logger.close();

            //String[] copy = {/*"sudo",*/ "cp", "/home/kevin/measuring/times.log", "/media/kevin/1E0C-4945/Results/"};
            //Process kv = Runtime.getRuntime().exec(copy);

            //copy = new String[] { "cp", "/home/kevin/measuring/log.log", "/media/kevin/1E0C-4945/Results/"};
            //Process qm = Runtime.getRuntime().exec(copy);
        } catch (Exception e)
        {
            /*todo: write error message to log*/
            e.printStackTrace();
        }
    }

    private Vector<FormulaTree> buildFormulas()
    {
        Vector<FormulaTree> formulas = new Vector<FormulaTree>();

        for(String line : completeADF.split("\\."))
            if(line.startsWith("s")) continue;
            else
                formulas.add(new FormulaTree(line));

        return formulas;
    }

    private String constructExportFormula(Vector<String> minimizedFormulae)
    {
        if( completeADF != null && minimizedFormulae.size() != 0 )
        {
            String minimizedADF = "";
            String adfNoWhites = "";
            for(char c : completeADF.toCharArray())
                if(c != ' ') adfNoWhites += c;

            /*int i = 0;
            for(String s : minimizedFormulae)
            {
                adfNoWhites.indexOf("ac(");
                int index = adfNoWhites.indexOf("ac(");
                String nodeName = adfNoWhites.substring(index+3, adfNoWhites.indexOf(",", index+3));
                //todo: check regex + indices for correctness
                adfNoWhites = adfNoWhites.replaceFirst("ac\\(.+\\)" , "ac(" + nodeName + "," + s + ").\n");
                //adfNoWhites = adfNoWhites.replaceFirst("ac.+\\." , "ac(" + nodeName + "," + s + ").\n");

                //minimizedADF += adfNoWhites.substring(0, adfNoWhites.indexOf('.')+1); //or +2 to include \n?
                minimizedADF += adfNoWhites.substring(0, index + s.length()+4 + nodeName.length() + 1);
                //line above could also work with minimizedADF = adfNoWhites.substring(0, adfNoWhites.indexOf(".")+1);

                adfNoWhites = adfNoWhites.substring(index + s.length()+2);
                //adfNoWhites = adfNoWhites.substring(0, adfNoWhites.indexOf('.')+1);
                i++;
            }*/

            int i = 0;
            for(String s : adfNoWhites.split("\\n"))
                if( s.startsWith("s(") )
                {
                    minimizedADF += s + System.getProperty("line.separator");
                    //adfNoWhites = adfNoWhites.substring(0, adfNoWhites.indexOf('.'));
                }
                else if( s.startsWith("ac(") )
                {
                    s = "ac(" + s.substring(3, s.indexOf(',')+1) + minimizedFormulae.get(i) + ").";
                    minimizedADF += s + System.getProperty("line.separator");
                    i++;
                }
            //else {} //todo more possibilites?

            //resubstitutes original statements back into the formula
            Integer j = 0;
            //System.out.println("CompleteADF before subs.: \n" + minimizedADF);
            for(String s : substitutes)
            {
                //CharSequence target = "<" + j.toString() + ">";
                CharSequence target = s;
                CharSequence replacement = statements.get(j).trim();
                //System.out.println("Substitution: " + target + " -> " + replacement);

                minimizedADF = minimizedADF.replace(target, replacement);

                //minimizedADF = minimizedADF.replaceAll(s, statements.get(i));
                j++;
            }

            //label.setText(completeADF);
            return minimizedADF;
        }
        else return null;
    }

    private void processStatements()
    {
        String[] adfLines = completeADF.split("\\.");
        //for(String s : adfLines) System.out.println(s);

        statements = new Vector<String>();

        //System.out.println(adfLines.length);
        for(int i=0; i < adfLines.length; i++)
        {
            adfLines[i] = adfLines[i].replace("\n", "");
            adfLines[i] += ".";
            if( adfLines[i].startsWith("s(")/* adfLines[i].matches("s(.+)\\.")*/ )
            {
                //System.out.println("Match");
                statements.add(adfLines[i].substring(2, adfLines[i].length()-2));
                System.out.println("Match:    " + statements.lastElement());
            }
            //System.out.println("tested: " + adfLines[i]);
        }

        //for(String s : statements) System.out.println(s);

        completeADF = completeADF.trim();
        substitutes = new Vector<String>();
        Integer i = 1;
        for( String s : statements )
        {
            //System.out.println("Running: " + s + " -> " + i);
            substitutes.add("<" + i.toString() + ">");
            s = s.trim();

            CharSequence replacement = substitutes.lastElement();
            CharSequence target = s;
            completeADF = completeADF.replace(target, replacement);
            //completeADF = completeADF.replaceAll(s, i.toString());
            i++;
        }
        //for(String s : substitutes) System.out.println(s);
    }

    /**
     * Returns -1 if Diamond did not output a time.
     */
    public static String parseDiamondOutputForTime(String dOutput)
    {
        String time = "";

        //System.out.println(dOutput);

        String line = "";
        for(String s : dOutput.split("\\r?\\n"))
        {
            if(s.startsWith("Time"))
            {
                //System.out.println(s.length());
                line = s;
                break;
            }
        }
        if( line.indexOf(':') == -1 ) return "-1";
        line = line.substring(line.indexOf(':')+2);
        line = line.substring(0, line.indexOf('s')+1);
        //System.out.println("line=" + line);

        //int index = dOutput.indexOf("Time" + "\\s" + ":")+1;
        //System.out.println("index = " + index);
        //dOutput = dOutput.substring(index, dOutput.indexOf("\\s", index));

        return line;
    }

    public static void measureTestCases()
    {
        long s1 = System.currentTimeMillis();
        QMCMinimizer.test();
        long f1 = System.currentTimeMillis();

        long s3 = System.currentTimeMillis();
        KVMinimizer.test();
        long f3 = System.currentTimeMillis();

        long s2 = System.currentTimeMillis();
        ScherzoAlgorithms.test();
        long f2 = System.currentTimeMillis();

        System.out.println("____________________");
        System.out.println("QMC:      " + (f1-s1) + "ms");
        System.out.println("KV:       " + (f3-s3) + "ms");
        System.out.println("Scherzo:  " + (f2-s2) + "ms");
        System.out.println("____________________");
    }

    /*public static void measure()
    {
        Vector<String> vars = new Vector<String>();
        vars.add("a");
        vars.add("b");
        vars.add("c");
        vars.add("d");

        String formula = FormulaGenerator.generateFormula(vars, 3);

        System.out.println("Generated formula: " + formula);

        FormulaTree ft1 = new FormulaTree(formula);
        String kvMinimized = "";
        String qmMinimized = "";

        long time1 = System.nanoTime();
        kvMinimized = FormulaMinimizer.karnaughVeitchMinimization(ft1);
        long time2 = System.nanoTime();

        long time3 = System.nanoTime();
        qmMinimized = FormulaMinimizer.quineMccluskeyMinimization(ft1);
        long time4 = System.nanoTime();

        System.out.println("Result: " + kvMinimized + "   Time for KV: " + (time2-time1) + "ns");
        System.out.println("Result: " + qmMinimized + "   Time for QM: " + (time4-time3) + "ns");
    }*/

    /**
     *
     * @param file the file storing the ADF to be grounded
     * @return the time the solving took in nanoseconds, if successful. Else returns -1
     */
    /*public static long measureGroundingTimes(File file)
    {
        long time1;
        long time2;
        try
        {
            time1 = System.nanoTime();
            Process p = Runtime.getRuntime().exec("/dependencies/diamond/diamond.py " + "-g " + file.getAbsolutePath());
            time2 = System.nanoTime();
            OutputStream outputStream = p.getOutputStream();
        }
        catch (Exception e)
        {
            return -1;
        }

        return time2-time1;
    }*/

    /**
     *
     * @param ADF the ADF supposed to be grounded
     * @return the time the solving took in nanoseconds, if successful. Else returns -1
     */
    /*public static long measuringGroundingTimes(String ADF)
    {
        try
        {
            File f = new File("C:/");
            FileWriter fileWriter = new FileWriter(f);
            fileWriter.write(ADF);

            long t = measureGroundingTimes(f);

            f.delete();
            return t;
        }
        catch (IOException e)
        {
            return -1;
        }
    }*/

    public void writeTagged()
    {
        completeADF = "s(atom(carry(pSum(1), line(2), 0))).\n" +
                "s(atom(carry(pSum(1), line(2), 1))).\n" +
                "s(atom(carry(pSum(1), line(2), 2))).\n" +
                "s(atom(carry(pSum(1), line(2), 3))).\n" +
                "s(atom(carry(pSum(1), line(2), 4))).\n" +
                "s(atom(carry(pSum(1), line(2), 5))).\n" +
                "s(atom(carry(pSum(1), line(2), 6))).\n" +
                "s(atom(carry(pSum(1), line(2), 7))).\n" +
                "s(atom(carry(pSum(4), line(5), 0))).\n" +
                "s(atom(carry(pSum(4), line(5), 1))).\n" +
                "s(atom(carry(pSum(4), line(5), 2))).\n" +
                "s(atom(carry(pSum(4), line(5), 3))).\n" +
                "s(atom(carry(pSum(4), line(5), 4))).\n" +
                "s(atom(carry(pSum(4), line(5), 5))).\n" +
                "s(atom(carry(pSum(4), line(5), 6))).\n" +
                "s(atom(carry(pSum(4), line(5), 7))).\n" +
                "s(atom(carry(pSum(4), line(5), 8))).\n" +
                "s(atom(carry(pSum(4), line(5), 9))).\n" +
                "s(atom(carry(pSum(4), line(5), 10))).\n" +
                "s(atom(bit(line(0), 0))).\n" +
                "s(atom(bit(line(0), 1))).\n" +
                "s(atom(bit(line(0), 2))).\n" +
                "s(atom(bit(line(0), 3))).\n" +
                "s(atom(bit(line(0), 4))).\n" +
                "s(atom(bit(line(0), 5))).\n" +
                "s(atom(bit(line(0), 6))).\n" +
                "s(atom(bit(line(1), 0))).\n" +
                "s(atom(bit(line(1), 1))).\n" +
                "s(atom(bit(line(1), 2))).\n" +
                "s(atom(bit(line(1), 3))).\n" +
                "s(atom(bit(line(1), 4))).\n" +
                "s(atom(bit(line(1), 5))).\n" +
                "s(atom(bit(line(1), 6))).\n" +
                "s(atom(bit(line(1), 7))).\n" +
                "s(atom(bit(line(2), 0))).\n" +
                "s(atom(bit(line(2), 1))).\n" +
                "s(atom(bit(line(2), 2))).\n" +
                "s(atom(bit(line(2), 3))).\n" +
                "s(atom(bit(line(2), 4))).\n" +
                "s(atom(bit(line(2), 5))).\n" +
                "s(atom(bit(line(2), 6))).\n" +
                "s(atom(bit(line(2), 7))).\n" +
                "s(atom(bit(line(2), 8))).\n" +
                "s(atom(bit(line(3), 0))).\n" +
                "s(atom(carry(line(0), line(1), 0))).\n" +
                "s(atom(bit(line(3), 1))).\n" +
                "s(atom(carry(line(0), line(1), 1))).\n" +
                "s(atom(bit(line(3), 2))).\n" +
                "s(atom(carry(line(0), line(1), 2))).\n" +
                "s(atom(bit(line(3), 3))).\n" +
                "s(atom(carry(line(0), line(1), 3))).\n" +
                "s(atom(bit(line(3), 4))).\n" +
                "s(atom(carry(line(0), line(1), 4))).\n" +
                "s(atom(bit(line(3), 5))).\n" +
                "s(atom(carry(line(0), line(1), 5))).\n" +
                "s(atom(bit(line(3), 6))).\n" +
                "s(atom(carry(line(0), line(1), 6))).\n" +
                "s(atom(bit(line(3), 7))).\n" +
                "s(atom(1)).\n" +
                "s(atom(bit(line(3), 8))).\n" +
                "s(atom(2)).\n" +
                "s(atom(bit(line(3), 9))).\n" +
                "s(atom(bit(line(4), 0))).\n" +
                "s(atom(3)).\n" +
                "s(atom(bit(line(4), 1))).\n" +
                "s(atom(5)).\n" +
                "s(atom(bit(line(4), 2))).\n" +
                "s(atom(bit(line(4), 3))).\n" +
                "s(atom(6)).\n" +
                "s(atom(bit(line(4), 4))).\n" +
                "s(atom(8)).\n" +
                "s(atom(bit(line(4), 5))).\n" +
                "s(atom(bit(line(4), 6))).\n" +
                "s(atom(bit(line(4), 7))).\n" +
                "s(atom(10)).\n" +
                "s(atom(11)).\n" +
                "s(atom(bit(line(4), 8))).\n" +
                "s(atom(12)).\n" +
                "s(atom(bit(line(4), 9))).\n" +
                "s(atom(bit(line(5), 0))).\n" +
                "s(atom(bit(line(4), 10))).\n" +
                "s(atom(bit(line(5), 1))).\n" +
                "s(atom(bit(line(5), 2))).\n" +
                "s(atom(bit(line(5), 3))).\n" +
                "s(atom(16)).\n" +
                "s(atom(bit(line(5), 4))).\n" +
                "s(atom(17)).\n" +
                "s(atom(bit(line(5), 5))).\n" +
                "s(atom(bit(line(5), 6))).\n" +
                "s(atom(18)).\n" +
                "s(atom(19)).\n" +
                "s(atom(bit(line(5), 7))).\n" +
                "s(atom(bit(line(5), 8))).\n" +
                "s(atom(bit(line(5), 9))).\n" +
                "s(atom(bit(line(6), 0))).\n" +
                "s(atom(22)).\n" +
                "s(atom(bit(line(5), 10))).\n" +
                "s(atom(bit(line(6), 1))).\n" +
                "s(atom(bit(line(5), 11))).\n" +
                "s(atom(bit(line(6), 2))).\n" +
                "s(atom(23)).\n" +
                "s(atom(bit(line(6), 3))).\n" +
                "s(atom(25)).\n" +
                "s(atom(bit(line(6), 4))).\n" +
                "s(atom(bit(line(6), 5))).\n" +
                "s(atom(27)).\n" +
                "s(atom(bit(line(6), 6))).\n" +
                "s(atom(carry(pSum(2), line(3), 0))).\n" +
                "s(atom(bit(line(6), 7))).\n" +
                "s(atom(carry(pSum(2), line(3), 1))).\n" +
                "s(atom(bit(line(6), 8))).\n" +
                "s(atom(29)).\n" +
                "s(atom(carry(pSum(2), line(3), 2))).\n" +
                "s(atom(30)).\n" +
                "s(atom(bit(line(6), 9))).\n" +
                "s(atom(carry(pSum(2), line(3), 3))).\n" +
                "s(atom(bit(line(6), 10))).\n" +
                "s(atom(carry(pSum(2), line(3), 4))).\n" +
                "s(atom(bit(line(6), 11))).\n" +
                "s(atom(carry(pSum(2), line(3), 5))).\n" +
                "s(atom(bit(line(6), 12))).\n" +
                "s(atom(carry(pSum(2), line(3), 6))).\n" +
                "s(atom(34)).\n" +
                "s(atom(carry(pSum(2), line(3), 7))).\n" +
                "s(atom(35)).\n" +
                "s(atom(carry(pSum(2), line(3), 8))).\n" +
                "s(atom(36)).\n" +
                "s(atom(37)).\n" +
                "s(atom(carry(pSum(5), line(6), 0))).\n" +
                "s(atom(carry(pSum(5), line(6), 1))).\n" +
                "s(atom(43)).\n" +
                "s(atom(carry(pSum(5), line(6), 2))).\n" +
                "s(atom(carry(pSum(5), line(6), 3))).\n" +
                "s(atom(carry(pSum(5), line(6), 4))).\n" +
                "s(atom(bit(pSum(1), 0))).\n" +
                "s(atom(carry(pSum(5), line(6), 5))).\n" +
                "s(atom(bit(pSum(1), 1))).\n" +
                "s(atom(carry(pSum(5), line(6), 6))).\n" +
                "s(atom(bit(pSum(1), 2))).\n" +
                "s(atom(47)).\n" +
                "s(atom(carry(pSum(5), line(6), 7))).\n" +
                "s(atom(bit(pSum(1), 3))).\n" +
                "s(atom(carry(pSum(5), line(6), 8))).\n" +
                "s(atom(bit(pSum(1), 4))).\n" +
                "s(atom(carry(pSum(5), line(6), 9))).\n" +
                "s(atom(bit(pSum(1), 5))).\n" +
                "s(atom(carry(pSum(5), line(6), 10))).\n" +
                "s(atom(bit(pSum(1), 6))).\n" +
                "s(atom(carry(pSum(5), line(6), 11))).\n" +
                "s(atom(bit(pSum(1), 7))).\n" +
                "s(atom(bit(pSum(2), 0))).\n" +
                "s(atom(bit(pSum(2), 1))).\n" +
                "s(atom(bit(pSum(2), 2))).\n" +
                "s(atom(bit(pSum(2), 3))).\n" +
                "s(atom(bit(pSum(2), 4))).\n" +
                "s(atom(bit(pSum(2), 5))).\n" +
                "s(atom(bit(pSum(2), 6))).\n" +
                "s(atom(61)).\n" +
                "s(atom(bit(pSum(2), 7))).\n" +
                "s(atom(bit(pSum(2), 8))).\n" +
                "s(atom(bit(pSum(3), 0))).\n" +
                "s(atom(64)).\n" +
                "s(atom(bit(pSum(3), 1))).\n" +
                "s(atom(bit(pSum(3), 2))).\n" +
                "s(atom(bit(pSum(3), 3))).\n" +
                "s(atom(bit(pSum(3), 4))).\n" +
                "s(atom(bit(pSum(3), 5))).\n" +
                "s(atom(bit(pSum(3), 6))).\n" +
                "s(atom(bit(pSum(3), 7))).\n" +
                "s(atom(bit(pSum(3), 8))).\n" +
                "s(atom(bit(pSum(3), 9))).\n" +
                "s(atom(bit(pSum(4), 0))).\n" +
                "s(atom(bit(pSum(4), 1))).\n" +
                "s(atom(bit(pSum(4), 2))).\n" +
                "s(atom(75)).\n" +
                "s(atom(bit(pSum(4), 3))).\n" +
                "s(atom(bit(pSum(4), 4))).\n" +
                "s(atom(bit(pSum(4), 5))).\n" +
                "s(atom(bit(pSum(4), 6))).\n" +
                "s(atom(bit(pSum(4), 7))).\n" +
                "s(atom(bit(pSum(4), 8))).\n" +
                "s(atom(bit(pSum(4), 9))).\n" +
                "s(atom(bit(pSum(5), 0))).\n" +
                "s(atom(bit(pSum(4), 10))).\n" +
                "s(atom(bit(pSum(5), 1))).\n" +
                "s(atom(bit(pSum(5), 2))).\n" +
                "s(atom(bit(pSum(5), 3))).\n" +
                "s(atom(bit(pSum(5), 4))).\n" +
                "s(atom(bit(pSum(5), 5))).\n" +
                "s(atom(bit(pSum(5), 6))).\n" +
                "s(atom(bit(pSum(5), 7))).\n" +
                "s(atom(bit(pSum(5), 8))).\n" +
                "s(atom(bit(pSum(5), 9))).\n" +
                "s(atom(bit(pSum(5), 10))).\n" +
                "s(atom(bit(pSum(5), 11))).\n" +
                "s(atom(95)).\n" +
                "s(atom(carry(pSum(3), line(4), 0))).\n" +
                "s(atom(carry(pSum(3), line(4), 1))).\n" +
                "s(atom(carry(pSum(3), line(4), 2))).\n" +
                "s(atom(carry(pSum(3), line(4), 3))).\n" +
                "s(atom(carry(pSum(3), line(4), 4))).\n" +
                "s(atom(carry(pSum(3), line(4), 5))).\n" +
                "s(atom(carry(pSum(3), line(4), 6))).\n" +
                "s(atom(carry(pSum(3), line(4), 7))).\n" +
                "s(atom(carry(pSum(3), line(4), 8))).\n" +
                "s(atom(carry(pSum(3), line(4), 9))).\n" +
                "s(atom(bit(x, 6))).\n" +
                "s(atom(bit(y, 6))).\n" +
                "s(atom(bit(z, 0))).\n" +
                "s(atom(bit(z, 1))).\n" +
                "s(atom(bit(z, 2))).\n" +
                "s(atom(bit(z, 3))).\n" +
                "s(atom(bit(z, 4))).\n" +
                "s(atom(bit(z, 5))).\n" +
                "s(atom(bit(z, 6))).\n" +
                "s(atom(bit(z, 7))).\n" +
                "s(atom(bit(z, 8))).\n" +
                "s(atom(bit(z, 9))).\n" +
                "s(atom(bit(z, 10))).\n" +
                "s(atom(bit(z, 11))).\n" +
                "s(atom(bit(z, 12))).\n" +
                "s(atom(bit(y, 0))).\n" +
                "s(atom(bit(y, 1))).\n" +
                "s(atom(bit(y, 2))).\n" +
                "s(atom(bit(y, 3))).\n" +
                "s(atom(bit(y, 4))).\n" +
                "s(atom(bit(y, 5))).\n" +
                "s(atom(bit(x, 0))).\n" +
                "s(atom(bit(x, 1))).\n" +
                "s(atom(bit(x, 2))).\n" +
                "s(atom(bit(x, 3))).\n" +
                "s(atom(bit(x, 4))).\n" +
                "s(atom(bit(x, 5))).\n" +
                "ac(atom(carry(pSum(1), line(2), 0)), c(f)).\n" +
                "ac(atom(carry(pSum(1), line(2), 1)), or(and(atom(bit(pSum(1), 0)), atom(bit(line(2), 0))), and(atom(carry(pSum(1), line(2), 0)), xor(atom(bit(pSum(1), 0)), atom(bit(line(2), 0)))))).\n" +
                "ac(atom(carry(pSum(1), line(2), 2)), or(and(atom(bit(pSum(1), 1)), atom(bit(line(2), 1))), and(atom(carry(pSum(1), line(2), 1)), xor(atom(bit(pSum(1), 1)), atom(bit(line(2), 1)))))).\n" +
                "ac(atom(carry(pSum(1), line(2), 3)), or(and(atom(bit(pSum(1), 2)), atom(bit(line(2), 2))), and(atom(carry(pSum(1), line(2), 2)), xor(atom(bit(pSum(1), 2)), atom(bit(line(2), 2)))))).\n" +
                "ac(atom(carry(pSum(1), line(2), 4)), or(and(atom(bit(pSum(1), 3)), atom(bit(line(2), 3))), and(atom(carry(pSum(1), line(2), 3)), xor(atom(bit(pSum(1), 3)), atom(bit(line(2), 3)))))).\n" +
                "ac(atom(carry(pSum(1), line(2), 5)), or(and(atom(bit(pSum(1), 4)), atom(bit(line(2), 4))), and(atom(carry(pSum(1), line(2), 4)), xor(atom(bit(pSum(1), 4)), atom(bit(line(2), 4)))))).\n" +
                "ac(atom(carry(pSum(1), line(2), 6)), or(and(atom(bit(pSum(1), 5)), atom(bit(line(2), 5))), and(atom(carry(pSum(1), line(2), 5)), xor(atom(bit(pSum(1), 5)), atom(bit(line(2), 5)))))).\n" +
                "ac(atom(carry(pSum(1), line(2), 7)), or(and(atom(bit(pSum(1), 6)), atom(bit(line(2), 6))), and(atom(carry(pSum(1), line(2), 6)), xor(atom(bit(pSum(1), 6)), atom(bit(line(2), 6)))))).\n" +
                "ac(atom(carry(pSum(4), line(5), 0)), c(f)).\n" +
                "ac(atom(carry(pSum(4), line(5), 1)), or(and(atom(bit(pSum(4), 0)), atom(bit(line(5), 0))), and(atom(carry(pSum(4), line(5), 0)), xor(atom(bit(pSum(4), 0)), atom(bit(line(5), 0)))))).\n" +
                "ac(atom(carry(pSum(4), line(5), 2)), or(and(atom(bit(pSum(4), 1)), atom(bit(line(5), 1))), and(atom(carry(pSum(4), line(5), 1)), xor(atom(bit(pSum(4), 1)), atom(bit(line(5), 1)))))).\n" +
                "ac(atom(carry(pSum(4), line(5), 3)), or(and(atom(bit(pSum(4), 2)), atom(bit(line(5), 2))), and(atom(carry(pSum(4), line(5), 2)), xor(atom(bit(pSum(4), 2)), atom(bit(line(5), 2)))))).\n" +
                "ac(atom(carry(pSum(4), line(5), 4)), or(and(atom(bit(pSum(4), 3)), atom(bit(line(5), 3))), and(atom(carry(pSum(4), line(5), 3)), xor(atom(bit(pSum(4), 3)), atom(bit(line(5), 3)))))).\n" +
                "ac(atom(carry(pSum(4), line(5), 5)), or(and(atom(bit(pSum(4), 4)), atom(bit(line(5), 4))), and(atom(carry(pSum(4), line(5), 4)), xor(atom(bit(pSum(4), 4)), atom(bit(line(5), 4)))))).\n" +
                "ac(atom(carry(pSum(4), line(5), 6)), or(and(atom(bit(pSum(4), 5)), atom(bit(line(5), 5))), and(atom(carry(pSum(4), line(5), 5)), xor(atom(bit(pSum(4), 5)), atom(bit(line(5), 5)))))).\n" +
                "ac(atom(carry(pSum(4), line(5), 7)), or(and(atom(bit(pSum(4), 6)), atom(bit(line(5), 6))), and(atom(carry(pSum(4), line(5), 6)), xor(atom(bit(pSum(4), 6)), atom(bit(line(5), 6)))))).\n" +
                "ac(atom(carry(pSum(4), line(5), 8)), or(and(atom(bit(pSum(4), 7)), atom(bit(line(5), 7))), and(atom(carry(pSum(4), line(5), 7)), xor(atom(bit(pSum(4), 7)), atom(bit(line(5), 7)))))).\n" +
                "ac(atom(carry(pSum(4), line(5), 9)), or(and(atom(bit(pSum(4), 8)), atom(bit(line(5), 8))), and(atom(carry(pSum(4), line(5), 8)), xor(atom(bit(pSum(4), 8)), atom(bit(line(5), 8)))))).\n" +
                "ac(atom(carry(pSum(4), line(5), 10)), or(and(atom(bit(pSum(4), 9)), atom(bit(line(5), 9))), and(atom(carry(pSum(4), line(5), 9)), xor(atom(bit(pSum(4), 9)), atom(bit(line(5), 9)))))).\n" +
                "ac(atom(bit(line(0), 0)), and(atom(bit(x, 0)), atom(bit(y, 0)))).\n" +
                "ac(atom(bit(line(0), 1)), and(atom(bit(x, 1)), atom(bit(y, 0)))).\n" +
                "ac(atom(bit(line(0), 2)), and(atom(bit(x, 2)), atom(bit(y, 0)))).\n" +
                "ac(atom(bit(line(0), 3)), and(atom(bit(x, 3)), atom(bit(y, 0)))).\n" +
                "ac(atom(bit(line(0), 4)), and(atom(bit(x, 4)), atom(bit(y, 0)))).\n" +
                "ac(atom(bit(line(0), 5)), and(atom(bit(x, 5)), atom(bit(y, 0)))).\n" +
                "ac(atom(bit(line(0), 6)), and(atom(bit(x, 6)), atom(bit(y, 0)))).\n" +
                "ac(atom(bit(line(1), 0)), c(f)).\n" +
                "ac(atom(bit(line(1), 1)), and(atom(bit(x, 0)), atom(bit(y, 1)))).\n" +
                "ac(atom(bit(line(1), 2)), and(atom(bit(x, 1)), atom(bit(y, 1)))).\n" +
                "ac(atom(bit(line(1), 3)), and(atom(bit(x, 2)), atom(bit(y, 1)))).\n" +
                "ac(atom(bit(line(1), 4)), and(atom(bit(x, 3)), atom(bit(y, 1)))).\n" +
                "ac(atom(bit(line(1), 5)), and(atom(bit(x, 4)), atom(bit(y, 1)))).\n" +
                "ac(atom(bit(line(1), 6)), and(atom(bit(x, 5)), atom(bit(y, 1)))).\n" +
                "ac(atom(bit(line(1), 7)), and(atom(bit(x, 6)), atom(bit(y, 1)))).\n" +
                "ac(atom(bit(line(2), 0)), c(f)).\n" +
                "ac(atom(bit(line(2), 1)), c(f)).\n" +
                "ac(atom(bit(line(2), 2)), and(atom(bit(x, 0)), atom(bit(y, 2)))).\n" +
                "ac(atom(bit(line(2), 3)), and(atom(bit(x, 1)), atom(bit(y, 2)))).\n" +
                "ac(atom(bit(line(2), 4)), and(atom(bit(x, 2)), atom(bit(y, 2)))).\n" +
                "ac(atom(bit(line(2), 5)), and(atom(bit(x, 3)), atom(bit(y, 2)))).\n" +
                "ac(atom(bit(line(2), 6)), and(atom(bit(x, 4)), atom(bit(y, 2)))).\n" +
                "ac(atom(bit(line(2), 7)), and(atom(bit(x, 5)), atom(bit(y, 2)))).\n" +
                "ac(atom(bit(line(2), 8)), and(atom(bit(x, 6)), atom(bit(y, 2)))).\n" +
                "ac(atom(bit(line(3), 0)), c(f)).\n" +
                "ac(atom(carry(line(0), line(1), 0)), c(f)).\n" +
                "ac(atom(bit(line(3), 1)), c(f)).\n" +
                "ac(atom(carry(line(0), line(1), 1)), or(and(atom(bit(line(0), 0)), atom(bit(line(1), 0))), and(atom(carry(line(0), line(1), 0)), xor(atom(bit(line(0), 0)), atom(bit(line(1), 0)))))).\n" +
                "ac(atom(bit(line(3), 2)), c(f)).\n" +
                "ac(atom(carry(line(0), line(1), 2)), or(and(atom(bit(line(0), 1)), atom(bit(line(1), 1))), and(atom(carry(line(0), line(1), 1)), xor(atom(bit(line(0), 1)), atom(bit(line(1), 1)))))).\n" +
                "ac(atom(bit(line(3), 3)), and(atom(bit(x, 0)), atom(bit(y, 3)))).\n" +
                "ac(atom(carry(line(0), line(1), 3)), or(and(atom(bit(line(0), 2)), atom(bit(line(1), 2))), and(atom(carry(line(0), line(1), 2)), xor(atom(bit(line(0), 2)), atom(bit(line(1), 2)))))).\n" +
                "ac(atom(bit(line(3), 4)), and(atom(bit(x, 1)), atom(bit(y, 3)))).\n" +
                "ac(atom(carry(line(0), line(1), 4)), or(and(atom(bit(line(0), 3)), atom(bit(line(1), 3))), and(atom(carry(line(0), line(1), 3)), xor(atom(bit(line(0), 3)), atom(bit(line(1), 3)))))).\n" +
                "ac(atom(bit(line(3), 5)), and(atom(bit(x, 2)), atom(bit(y, 3)))).\n" +
                "ac(atom(carry(line(0), line(1), 5)), or(and(atom(bit(line(0), 4)), atom(bit(line(1), 4))), and(atom(carry(line(0), line(1), 4)), xor(atom(bit(line(0), 4)), atom(bit(line(1), 4)))))).\n" +
                "ac(atom(bit(line(3), 6)), and(atom(bit(x, 3)), atom(bit(y, 3)))).\n" +
                "ac(atom(carry(line(0), line(1), 6)), or(and(atom(bit(line(0), 5)), atom(bit(line(1), 5))), and(atom(carry(line(0), line(1), 5)), xor(atom(bit(line(0), 5)), atom(bit(line(1), 5)))))).\n" +
                "ac(atom(bit(line(3), 7)), and(atom(bit(x, 4)), atom(bit(y, 3)))).\n" +
                "ac(atom(1), and(neg(iff(atom(bit(pSum(1), 1)), xor(xor(atom(bit(line(0), 1)), atom(bit(line(1), 1))), atom(carry(line(0), line(1), 1))))), neg(atom(1)))).\n" +
                "ac(atom(bit(line(3), 8)), and(atom(bit(x, 5)), atom(bit(y, 3)))).\n" +
                "ac(atom(2), and(neg(iff(atom(bit(pSum(1), 2)), xor(xor(atom(bit(line(0), 2)), atom(bit(line(1), 2))), atom(carry(line(0), line(1), 2))))), neg(atom(2)))).\n" +
                "ac(atom(bit(line(3), 9)), and(atom(bit(x, 6)), atom(bit(y, 3)))).\n" +
                "ac(atom(bit(line(4), 0)), c(f)).\n" +
                "ac(atom(3), and(neg(iff(atom(carry(line(0), line(1), 1)), or(and(atom(bit(line(0), 0)), atom(bit(line(1), 0))), and(atom(carry(line(0), line(1), 0)), xor(atom(bit(line(0), 0)), atom(bit(line(1), 0))))))), neg(atom(3)))).\n" +
                "ac(atom(bit(line(4), 1)), c(f)).\n" +
                "ac(atom(5), and(neg(iff(atom(bit(z, 8)), xor(xor(atom(bit(pSum(5), 8)), atom(bit(line(6), 8))), atom(carry(pSum(5), line(6), 8))))), neg(atom(5)))).\n" +
                "ac(atom(bit(line(4), 2)), c(f)).\n" +
                "ac(atom(bit(line(4), 3)), c(f)).\n" +
                "ac(atom(6), and(neg(iff(atom(bit(pSum(1), 6)), xor(xor(atom(bit(line(0), 6)), atom(bit(line(1), 6))), atom(carry(line(0), line(1), 6))))), neg(atom(6)))).\n" +
                "ac(atom(bit(line(4), 4)), and(atom(bit(x, 0)), atom(bit(y, 4)))).\n" +
                "ac(atom(8), and(neg(iff(atom(bit(z, 1)), xor(xor(atom(bit(pSum(5), 1)), atom(bit(line(6), 1))), atom(carry(pSum(5), line(6), 1))))), neg(atom(8)))).\n" +
                "ac(atom(bit(line(4), 5)), and(atom(bit(x, 1)), atom(bit(y, 4)))).\n" +
                "ac(atom(bit(line(4), 6)), and(atom(bit(x, 2)), atom(bit(y, 4)))).\n" +
                "ac(atom(bit(line(4), 7)), and(atom(bit(x, 3)), atom(bit(y, 4)))).\n" +
                "ac(atom(10), and(neg(iff(atom(bit(pSum(1), 3)), xor(xor(atom(bit(line(0), 3)), atom(bit(line(1), 3))), atom(carry(line(0), line(1), 3))))), neg(atom(10)))).\n" +
                "ac(atom(11), and(neg(iff(atom(bit(z, 2)), xor(xor(atom(bit(pSum(5), 2)), atom(bit(line(6), 2))), atom(carry(pSum(5), line(6), 2))))), neg(atom(11)))).\n" +
                "ac(atom(bit(line(4), 8)), and(atom(bit(x, 4)), atom(bit(y, 4)))).\n" +
                "ac(atom(12), and(neg(iff(atom(bit(z, 9)), xor(xor(atom(bit(pSum(5), 9)), atom(bit(line(6), 9))), atom(carry(pSum(5), line(6), 9))))), neg(atom(12)))).\n" +
                "ac(atom(bit(line(4), 9)), and(atom(bit(x, 5)), atom(bit(y, 4)))).\n" +
                "ac(atom(bit(line(5), 0)), c(f)).\n" +
                "ac(atom(bit(line(4), 10)), and(atom(bit(x, 6)), atom(bit(y, 4)))).\n" +
                "ac(atom(bit(line(5), 1)), c(f)).\n" +
                "ac(atom(bit(line(5), 2)), c(f)).\n" +
                "ac(atom(bit(line(5), 3)), c(f)).\n" +
                "ac(atom(16), and(neg(iff(atom(bit(z, 10)), xor(xor(atom(bit(pSum(5), 10)), atom(bit(line(6), 10))), atom(carry(pSum(5), line(6), 10))))), neg(atom(16)))).\n" +
                "ac(atom(bit(line(5), 4)), c(f)).\n" +
                "ac(atom(17), and(neg(iff(atom(bit(z, 3)), xor(xor(atom(bit(pSum(5), 3)), atom(bit(line(6), 3))), atom(carry(pSum(5), line(6), 3))))), neg(atom(17)))).\n" +
                "ac(atom(bit(line(5), 5)), and(atom(bit(x, 0)), atom(bit(y, 5)))).\n" +
                "ac(atom(bit(line(5), 6)), and(atom(bit(x, 1)), atom(bit(y, 5)))).\n" +
                "ac(atom(18), and(neg(iff(atom(carry(line(0), line(1), 4)), or(and(atom(bit(line(0), 3)), atom(bit(line(1), 3))), and(atom(carry(line(0), line(1), 3)), xor(atom(bit(line(0), 3)), atom(bit(line(1), 3))))))), neg(atom(18)))).\n" +
                "ac(atom(19), and(neg(or(and(neg(atom(bit(x, 6))), atom(bit(y, 6))), or(and(and(neg(atom(bit(x, 5))), atom(bit(y, 5))), iff(atom(bit(x, 6)), atom(bit(y, 6)))), or(and(and(neg(atom(bit(x, 4))), atom(bit(y, 4))), and(iff(atom(bit(x, 6)), atom(bit(y, 6))), iff(atom(bit(x, 5)), atom(bit(y, 5))))), or(and(and(neg(atom(bit(x, 3))), atom(bit(y, 3))), and(iff(atom(bit(x, 6)), atom(bit(y, 6))), and(iff(atom(bit(x, 5)), atom(bit(y, 5))), iff(atom(bit(x, 4)), atom(bit(y, 4)))))), or(and(and(neg(atom(bit(x, 2))), atom(bit(y, 2))), and(iff(atom(bit(x, 6)), atom(bit(y, 6))), and(iff(atom(bit(x, 5)), atom(bit(y, 5))), and(iff(atom(bit(x, 4)), atom(bit(y, 4))), iff(atom(bit(x, 3)), atom(bit(y, 3))))))), or(and(and(neg(atom(bit(x, 1))), atom(bit(y, 1))), and(iff(atom(bit(x, 6)), atom(bit(y, 6))), and(iff(atom(bit(x, 5)), atom(bit(y, 5))), and(iff(atom(bit(x, 4)), atom(bit(y, 4))), and(iff(atom(bit(x, 3)), atom(bit(y, 3))), iff(atom(bit(x, 2)), atom(bit(y, 2)))))))), and(and(neg(atom(bit(x, 0))), atom(bit(y, 0))), and(iff(atom(bit(x, 6)), atom(bit(y, 6))), and(iff(atom(bit(x, 5)), atom(bit(y, 5))), and(iff(atom(bit(x, 4)), atom(bit(y, 4))), and(iff(atom(bit(x, 3)), atom(bit(y, 3))), and(iff(atom(bit(x, 2)), atom(bit(y, 2))), iff(atom(bit(x, 1)), atom(bit(y, 1)))))))))))))))), neg(atom(19)))).\n" +
                "ac(atom(bit(line(5), 7)), and(atom(bit(x, 2)), atom(bit(y, 5)))).\n" +
                "ac(atom(bit(line(5), 8)), and(atom(bit(x, 3)), atom(bit(y, 5)))).\n" +
                "ac(atom(bit(line(5), 9)), and(atom(bit(x, 4)), atom(bit(y, 5)))).\n" +
                "ac(atom(bit(line(6), 0)), c(f)).\n" +
                "ac(atom(22), and(neg(iff(atom(bit(z, 7)), xor(xor(atom(bit(pSum(5), 7)), atom(bit(line(6), 7))), atom(carry(pSum(5), line(6), 7))))), neg(atom(22)))).\n" +
                "ac(atom(bit(line(5), 10)), and(atom(bit(x, 5)), atom(bit(y, 5)))).\n" +
                "ac(atom(bit(line(6), 1)), c(f)).\n" +
                "ac(atom(bit(line(5), 11)), and(atom(bit(x, 6)), atom(bit(y, 5)))).\n" +
                "ac(atom(bit(line(6), 2)), c(f)).\n" +
                "ac(atom(23), and(neg(iff(atom(carry(line(0), line(1), 5)), or(and(atom(bit(line(0), 4)), atom(bit(line(1), 4))), and(atom(carry(line(0), line(1), 4)), xor(atom(bit(line(0), 4)), atom(bit(line(1), 4))))))), neg(atom(23)))).\n" +
                "ac(atom(bit(line(6), 3)), c(f)).\n" +
                "ac(atom(25), and(neg(iff(atom(bit(z, 5)), xor(xor(atom(bit(pSum(5), 5)), atom(bit(line(6), 5))), atom(carry(pSum(5), line(6), 5))))), neg(atom(25)))).\n" +
                "ac(atom(bit(line(6), 4)), c(f)).\n" +
                "ac(atom(bit(line(6), 5)), c(f)).\n" +
                "ac(atom(27), and(neg(iff(atom(bit(z, 4)), xor(xor(atom(bit(pSum(5), 4)), atom(bit(line(6), 4))), atom(carry(pSum(5), line(6), 4))))), neg(atom(27)))).\n" +
                "ac(atom(bit(line(6), 6)), and(atom(bit(x, 0)), atom(bit(y, 6)))).\n" +
                "ac(atom(carry(pSum(2), line(3), 0)), c(f)).\n" +
                "ac(atom(bit(line(6), 7)), and(atom(bit(x, 1)), atom(bit(y, 6)))).\n" +
                "ac(atom(carry(pSum(2), line(3), 1)), or(and(atom(bit(pSum(2), 0)), atom(bit(line(3), 0))), and(atom(carry(pSum(2), line(3), 0)), xor(atom(bit(pSum(2), 0)), atom(bit(line(3), 0)))))).\n" +
                "ac(atom(bit(line(6), 8)), and(atom(bit(x, 2)), atom(bit(y, 6)))).\n" +
                "ac(atom(29), and(neg(iff(atom(carry(line(0), line(1), 2)), or(and(atom(bit(line(0), 1)), atom(bit(line(1), 1))), and(atom(carry(line(0), line(1), 1)), xor(atom(bit(line(0), 1)), atom(bit(line(1), 1))))))), neg(atom(29)))).\n" +
                "ac(atom(carry(pSum(2), line(3), 2)), or(and(atom(bit(pSum(2), 1)), atom(bit(line(3), 1))), and(atom(carry(pSum(2), line(3), 1)), xor(atom(bit(pSum(2), 1)), atom(bit(line(3), 1)))))).\n" +
                "ac(atom(30), and(neg(iff(atom(bit(z, 11)), xor(xor(atom(bit(pSum(5), 11)), atom(bit(line(6), 11))), atom(carry(pSum(5), line(6), 11))))), neg(atom(30)))).\n" +
                "ac(atom(bit(line(6), 9)), and(atom(bit(x, 3)), atom(bit(y, 6)))).\n" +
                "ac(atom(carry(pSum(2), line(3), 3)), or(and(atom(bit(pSum(2), 2)), atom(bit(line(3), 2))), and(atom(carry(pSum(2), line(3), 2)), xor(atom(bit(pSum(2), 2)), atom(bit(line(3), 2)))))).\n" +
                "ac(atom(bit(line(6), 10)), and(atom(bit(x, 4)), atom(bit(y, 6)))).\n" +
                "ac(atom(carry(pSum(2), line(3), 4)), or(and(atom(bit(pSum(2), 3)), atom(bit(line(3), 3))), and(atom(carry(pSum(2), line(3), 3)), xor(atom(bit(pSum(2), 3)), atom(bit(line(3), 3)))))).\n" +
                "ac(atom(bit(line(6), 11)), and(atom(bit(x, 5)), atom(bit(y, 6)))).\n" +
                "ac(atom(carry(pSum(2), line(3), 5)), or(and(atom(bit(pSum(2), 4)), atom(bit(line(3), 4))), and(atom(carry(pSum(2), line(3), 4)), xor(atom(bit(pSum(2), 4)), atom(bit(line(3), 4)))))).\n" +
                "ac(atom(bit(line(6), 12)), and(atom(bit(x, 6)), atom(bit(y, 6)))).\n" +
                "ac(atom(carry(pSum(2), line(3), 6)), or(and(atom(bit(pSum(2), 5)), atom(bit(line(3), 5))), and(atom(carry(pSum(2), line(3), 5)), xor(atom(bit(pSum(2), 5)), atom(bit(line(3), 5)))))).\n" +
                "ac(atom(34), and(neg(iff(atom(bit(pSum(1), 0)), xor(xor(atom(bit(line(0), 0)), atom(bit(line(1), 0))), atom(carry(line(0), line(1), 0))))), neg(atom(34)))).\n" +
                "ac(atom(carry(pSum(2), line(3), 7)), or(and(atom(bit(pSum(2), 6)), atom(bit(line(3), 6))), and(atom(carry(pSum(2), line(3), 6)), xor(atom(bit(pSum(2), 6)), atom(bit(line(3), 6)))))).\n" +
                "ac(atom(35), and(neg(iff(atom(bit(pSum(1), 4)), xor(xor(atom(bit(line(0), 4)), atom(bit(line(1), 4))), atom(carry(line(0), line(1), 4))))), neg(atom(35)))).\n" +
                "ac(atom(carry(pSum(2), line(3), 8)), or(and(atom(bit(pSum(2), 7)), atom(bit(line(3), 7))), and(atom(carry(pSum(2), line(3), 7)), xor(atom(bit(pSum(2), 7)), atom(bit(line(3), 7)))))).\n" +
                "ac(atom(36), and(neg(iff(atom(bit(pSum(1), 5)), xor(xor(atom(bit(line(0), 5)), atom(bit(line(1), 5))), atom(carry(line(0), line(1), 5))))), neg(atom(36)))).\n" +
                "ac(atom(37), and(neg(iff(atom(carry(line(0), line(1), 3)), or(and(atom(bit(line(0), 2)), atom(bit(line(1), 2))), and(atom(carry(line(0), line(1), 2)), xor(atom(bit(line(0), 2)), atom(bit(line(1), 2))))))), neg(atom(37)))).\n" +
                "ac(atom(carry(pSum(5), line(6), 0)), c(f)).\n" +
                "ac(atom(carry(pSum(5), line(6), 1)), or(and(atom(bit(pSum(5), 0)), atom(bit(line(6), 0))), and(atom(carry(pSum(5), line(6), 0)), xor(atom(bit(pSum(5), 0)), atom(bit(line(6), 0)))))).\n" +
                "ac(atom(43), and(neg(iff(atom(bit(z, 12)), or(and(atom(bit(pSum(5), 11)), atom(bit(line(6), 11))), and(atom(carry(pSum(5), line(6), 11)), xor(atom(bit(pSum(5), 11)), atom(bit(line(6), 11))))))), neg(atom(43)))).\n" +
                "ac(atom(carry(pSum(5), line(6), 2)), or(and(atom(bit(pSum(5), 1)), atom(bit(line(6), 1))), and(atom(carry(pSum(5), line(6), 1)), xor(atom(bit(pSum(5), 1)), atom(bit(line(6), 1)))))).\n" +
                "ac(atom(carry(pSum(5), line(6), 3)), or(and(atom(bit(pSum(5), 2)), atom(bit(line(6), 2))), and(atom(carry(pSum(5), line(6), 2)), xor(atom(bit(pSum(5), 2)), atom(bit(line(6), 2)))))).\n" +
                "ac(atom(carry(pSum(5), line(6), 4)), or(and(atom(bit(pSum(5), 3)), atom(bit(line(6), 3))), and(atom(carry(pSum(5), line(6), 3)), xor(atom(bit(pSum(5), 3)), atom(bit(line(6), 3)))))).\n" +
                "ac(atom(bit(pSum(1), 0)), xor(xor(atom(bit(line(0), 0)), atom(bit(line(1), 0))), atom(carry(line(0), line(1), 0)))).\n" +
                "ac(atom(carry(pSum(5), line(6), 5)), or(and(atom(bit(pSum(5), 4)), atom(bit(line(6), 4))), and(atom(carry(pSum(5), line(6), 4)), xor(atom(bit(pSum(5), 4)), atom(bit(line(6), 4)))))).\n" +
                "ac(atom(bit(pSum(1), 1)), xor(xor(atom(bit(line(0), 1)), atom(bit(line(1), 1))), atom(carry(line(0), line(1), 1)))).\n" +
                "ac(atom(carry(pSum(5), line(6), 6)), or(and(atom(bit(pSum(5), 5)), atom(bit(line(6), 5))), and(atom(carry(pSum(5), line(6), 5)), xor(atom(bit(pSum(5), 5)), atom(bit(line(6), 5)))))).\n" +
                "ac(atom(bit(pSum(1), 2)), xor(xor(atom(bit(line(0), 2)), atom(bit(line(1), 2))), atom(carry(line(0), line(1), 2)))).\n" +
                "ac(atom(47), and(neg(neg(atom(carry(line(0), line(1), 0)))), neg(atom(47)))).\n" +
                "ac(atom(carry(pSum(5), line(6), 7)), or(and(atom(bit(pSum(5), 6)), atom(bit(line(6), 6))), and(atom(carry(pSum(5), line(6), 6)), xor(atom(bit(pSum(5), 6)), atom(bit(line(6), 6)))))).\n" +
                "ac(atom(bit(pSum(1), 3)), xor(xor(atom(bit(line(0), 3)), atom(bit(line(1), 3))), atom(carry(line(0), line(1), 3)))).\n" +
                "ac(atom(carry(pSum(5), line(6), 8)), or(and(atom(bit(pSum(5), 7)), atom(bit(line(6), 7))), and(atom(carry(pSum(5), line(6), 7)), xor(atom(bit(pSum(5), 7)), atom(bit(line(6), 7)))))).\n" +
                "ac(atom(bit(pSum(1), 4)), xor(xor(atom(bit(line(0), 4)), atom(bit(line(1), 4))), atom(carry(line(0), line(1), 4)))).\n" +
                "ac(atom(carry(pSum(5), line(6), 9)), or(and(atom(bit(pSum(5), 8)), atom(bit(line(6), 8))), and(atom(carry(pSum(5), line(6), 8)), xor(atom(bit(pSum(5), 8)), atom(bit(line(6), 8)))))).\n" +
                "ac(atom(bit(pSum(1), 5)), xor(xor(atom(bit(line(0), 5)), atom(bit(line(1), 5))), atom(carry(line(0), line(1), 5)))).\n" +
                "ac(atom(carry(pSum(5), line(6), 10)), or(and(atom(bit(pSum(5), 9)), atom(bit(line(6), 9))), and(atom(carry(pSum(5), line(6), 9)), xor(atom(bit(pSum(5), 9)), atom(bit(line(6), 9)))))).\n" +
                "ac(atom(bit(pSum(1), 6)), xor(xor(atom(bit(line(0), 6)), atom(bit(line(1), 6))), atom(carry(line(0), line(1), 6)))).\n" +
                "ac(atom(carry(pSum(5), line(6), 11)), or(and(atom(bit(pSum(5), 10)), atom(bit(line(6), 10))), and(atom(carry(pSum(5), line(6), 10)), xor(atom(bit(pSum(5), 10)), atom(bit(line(6), 10)))))).\n" +
                "ac(atom(bit(pSum(1), 7)), or(and(atom(bit(line(0), 6)), atom(bit(line(1), 6))), and(atom(carry(line(0), line(1), 6)), xor(atom(bit(line(0), 6)), atom(bit(line(1), 6)))))).\n" +
                "ac(atom(bit(pSum(2), 0)), xor(xor(atom(bit(pSum(1), 0)), atom(bit(line(2), 0))), atom(carry(pSum(1), line(2), 0)))).\n" +
                "ac(atom(bit(pSum(2), 1)), xor(xor(atom(bit(pSum(1), 1)), atom(bit(line(2), 1))), atom(carry(pSum(1), line(2), 1)))).\n" +
                "ac(atom(bit(pSum(2), 2)), xor(xor(atom(bit(pSum(1), 2)), atom(bit(line(2), 2))), atom(carry(pSum(1), line(2), 2)))).\n" +
                "ac(atom(bit(pSum(2), 3)), xor(xor(atom(bit(pSum(1), 3)), atom(bit(line(2), 3))), atom(carry(pSum(1), line(2), 3)))).\n" +
                "ac(atom(bit(pSum(2), 4)), xor(xor(atom(bit(pSum(1), 4)), atom(bit(line(2), 4))), atom(carry(pSum(1), line(2), 4)))).\n" +
                "ac(atom(bit(pSum(2), 5)), xor(xor(atom(bit(pSum(1), 5)), atom(bit(line(2), 5))), atom(carry(pSum(1), line(2), 5)))).\n" +
                "ac(atom(bit(pSum(2), 6)), xor(xor(atom(bit(pSum(1), 6)), atom(bit(line(2), 6))), atom(carry(pSum(1), line(2), 6)))).\n" +
                "ac(atom(61), and(neg(iff(atom(carry(line(0), line(1), 6)), or(and(atom(bit(line(0), 5)), atom(bit(line(1), 5))), and(atom(carry(line(0), line(1), 5)), xor(atom(bit(line(0), 5)), atom(bit(line(1), 5))))))), neg(atom(61)))).\n" +
                "ac(atom(bit(pSum(2), 7)), xor(xor(atom(bit(pSum(1), 7)), atom(bit(line(2), 7))), atom(carry(pSum(1), line(2), 7)))).\n" +
                "ac(atom(bit(pSum(2), 8)), or(and(atom(bit(pSum(1), 7)), atom(bit(line(2), 7))), and(atom(carry(pSum(1), line(2), 7)), xor(atom(bit(pSum(1), 7)), atom(bit(line(2), 7)))))).\n" +
                "ac(atom(bit(pSum(3), 0)), xor(xor(atom(bit(pSum(2), 0)), atom(bit(line(3), 0))), atom(carry(pSum(2), line(3), 0)))).\n" +
                "ac(atom(64), and(neg(iff(atom(bit(z, 0)), xor(xor(atom(bit(pSum(5), 0)), atom(bit(line(6), 0))), atom(carry(pSum(5), line(6), 0))))), neg(atom(64)))).\n" +
                "ac(atom(bit(pSum(3), 1)), xor(xor(atom(bit(pSum(2), 1)), atom(bit(line(3), 1))), atom(carry(pSum(2), line(3), 1)))).\n" +
                "ac(atom(bit(pSum(3), 2)), xor(xor(atom(bit(pSum(2), 2)), atom(bit(line(3), 2))), atom(carry(pSum(2), line(3), 2)))).\n" +
                "ac(atom(bit(pSum(3), 3)), xor(xor(atom(bit(pSum(2), 3)), atom(bit(line(3), 3))), atom(carry(pSum(2), line(3), 3)))).\n" +
                "ac(atom(bit(pSum(3), 4)), xor(xor(atom(bit(pSum(2), 4)), atom(bit(line(3), 4))), atom(carry(pSum(2), line(3), 4)))).\n" +
                "ac(atom(bit(pSum(3), 5)), xor(xor(atom(bit(pSum(2), 5)), atom(bit(line(3), 5))), atom(carry(pSum(2), line(3), 5)))).\n" +
                "ac(atom(bit(pSum(3), 6)), xor(xor(atom(bit(pSum(2), 6)), atom(bit(line(3), 6))), atom(carry(pSum(2), line(3), 6)))).\n" +
                "ac(atom(bit(pSum(3), 7)), xor(xor(atom(bit(pSum(2), 7)), atom(bit(line(3), 7))), atom(carry(pSum(2), line(3), 7)))).\n" +
                "ac(atom(bit(pSum(3), 8)), xor(xor(atom(bit(pSum(2), 8)), atom(bit(line(3), 8))), atom(carry(pSum(2), line(3), 8)))).\n" +
                "ac(atom(bit(pSum(3), 9)), or(and(atom(bit(pSum(2), 8)), atom(bit(line(3), 8))), and(atom(carry(pSum(2), line(3), 8)), xor(atom(bit(pSum(2), 8)), atom(bit(line(3), 8)))))).\n" +
                "ac(atom(bit(pSum(4), 0)), xor(xor(atom(bit(pSum(3), 0)), atom(bit(line(4), 0))), atom(carry(pSum(3), line(4), 0)))).\n" +
                "ac(atom(bit(pSum(4), 1)), xor(xor(atom(bit(pSum(3), 1)), atom(bit(line(4), 1))), atom(carry(pSum(3), line(4), 1)))).\n" +
                "ac(atom(bit(pSum(4), 2)), xor(xor(atom(bit(pSum(3), 2)), atom(bit(line(4), 2))), atom(carry(pSum(3), line(4), 2)))).\n" +
                "ac(atom(75), and(neg(iff(atom(bit(pSum(1), 7)), or(and(atom(bit(line(0), 6)), atom(bit(line(1), 6))), and(atom(carry(line(0), line(1), 6)), xor(atom(bit(line(0), 6)), atom(bit(line(1), 6))))))), neg(atom(75)))).\n" +
                "ac(atom(bit(pSum(4), 3)), xor(xor(atom(bit(pSum(3), 3)), atom(bit(line(4), 3))), atom(carry(pSum(3), line(4), 3)))).\n" +
                "ac(atom(bit(pSum(4), 4)), xor(xor(atom(bit(pSum(3), 4)), atom(bit(line(4), 4))), atom(carry(pSum(3), line(4), 4)))).\n" +
                "ac(atom(bit(pSum(4), 5)), xor(xor(atom(bit(pSum(3), 5)), atom(bit(line(4), 5))), atom(carry(pSum(3), line(4), 5)))).\n" +
                "ac(atom(bit(pSum(4), 6)), xor(xor(atom(bit(pSum(3), 6)), atom(bit(line(4), 6))), atom(carry(pSum(3), line(4), 6)))).\n" +
                "ac(atom(bit(pSum(4), 7)), xor(xor(atom(bit(pSum(3), 7)), atom(bit(line(4), 7))), atom(carry(pSum(3), line(4), 7)))).\n" +
                "ac(atom(bit(pSum(4), 8)), xor(xor(atom(bit(pSum(3), 8)), atom(bit(line(4), 8))), atom(carry(pSum(3), line(4), 8)))).\n" +
                "ac(atom(bit(pSum(4), 9)), xor(xor(atom(bit(pSum(3), 9)), atom(bit(line(4), 9))), atom(carry(pSum(3), line(4), 9)))).\n" +
                "ac(atom(bit(pSum(5), 0)), xor(xor(atom(bit(pSum(4), 0)), atom(bit(line(5), 0))), atom(carry(pSum(4), line(5), 0)))).\n" +
                "ac(atom(bit(pSum(4), 10)), or(and(atom(bit(pSum(3), 9)), atom(bit(line(4), 9))), and(atom(carry(pSum(3), line(4), 9)), xor(atom(bit(pSum(3), 9)), atom(bit(line(4), 9)))))).\n" +
                "ac(atom(bit(pSum(5), 1)), xor(xor(atom(bit(pSum(4), 1)), atom(bit(line(5), 1))), atom(carry(pSum(4), line(5), 1)))).\n" +
                "ac(atom(bit(pSum(5), 2)), xor(xor(atom(bit(pSum(4), 2)), atom(bit(line(5), 2))), atom(carry(pSum(4), line(5), 2)))).\n" +
                "ac(atom(bit(pSum(5), 3)), xor(xor(atom(bit(pSum(4), 3)), atom(bit(line(5), 3))), atom(carry(pSum(4), line(5), 3)))).\n" +
                "ac(atom(bit(pSum(5), 4)), xor(xor(atom(bit(pSum(4), 4)), atom(bit(line(5), 4))), atom(carry(pSum(4), line(5), 4)))).\n" +
                "ac(atom(bit(pSum(5), 5)), xor(xor(atom(bit(pSum(4), 5)), atom(bit(line(5), 5))), atom(carry(pSum(4), line(5), 5)))).\n" +
                "ac(atom(bit(pSum(5), 6)), xor(xor(atom(bit(pSum(4), 6)), atom(bit(line(5), 6))), atom(carry(pSum(4), line(5), 6)))).\n" +
                "ac(atom(bit(pSum(5), 7)), xor(xor(atom(bit(pSum(4), 7)), atom(bit(line(5), 7))), atom(carry(pSum(4), line(5), 7)))).\n" +
                "ac(atom(bit(pSum(5), 8)), xor(xor(atom(bit(pSum(4), 8)), atom(bit(line(5), 8))), atom(carry(pSum(4), line(5), 8)))).\n" +
                "ac(atom(bit(pSum(5), 9)), xor(xor(atom(bit(pSum(4), 9)), atom(bit(line(5), 9))), atom(carry(pSum(4), line(5), 9)))).\n" +
                "ac(atom(bit(pSum(5), 10)), xor(xor(atom(bit(pSum(4), 10)), atom(bit(line(5), 10))), atom(carry(pSum(4), line(5), 10)))).\n" +
                "ac(atom(bit(pSum(5), 11)), or(and(atom(bit(pSum(4), 10)), atom(bit(line(5), 10))), and(atom(carry(pSum(4), line(5), 10)), xor(atom(bit(pSum(4), 10)), atom(bit(line(5), 10)))))).\n" +
                "ac(atom(95), and(neg(iff(atom(bit(z, 6)), xor(xor(atom(bit(pSum(5), 6)), atom(bit(line(6), 6))), atom(carry(pSum(5), line(6), 6))))), neg(atom(95)))).\n" +
                "ac(atom(carry(pSum(3), line(4), 0)), c(f)).\n" +
                "ac(atom(carry(pSum(3), line(4), 1)), or(and(atom(bit(pSum(3), 0)), atom(bit(line(4), 0))), and(atom(carry(pSum(3), line(4), 0)), xor(atom(bit(pSum(3), 0)), atom(bit(line(4), 0)))))).\n" +
                "ac(atom(carry(pSum(3), line(4), 2)), or(and(atom(bit(pSum(3), 1)), atom(bit(line(4), 1))), and(atom(carry(pSum(3), line(4), 1)), xor(atom(bit(pSum(3), 1)), atom(bit(line(4), 1)))))).\n" +
                "ac(atom(carry(pSum(3), line(4), 3)), or(and(atom(bit(pSum(3), 2)), atom(bit(line(4), 2))), and(atom(carry(pSum(3), line(4), 2)), xor(atom(bit(pSum(3), 2)), atom(bit(line(4), 2)))))).\n" +
                "ac(atom(carry(pSum(3), line(4), 4)), or(and(atom(bit(pSum(3), 3)), atom(bit(line(4), 3))), and(atom(carry(pSum(3), line(4), 3)), xor(atom(bit(pSum(3), 3)), atom(bit(line(4), 3)))))).\n" +
                "ac(atom(carry(pSum(3), line(4), 5)), or(and(atom(bit(pSum(3), 4)), atom(bit(line(4), 4))), and(atom(carry(pSum(3), line(4), 4)), xor(atom(bit(pSum(3), 4)), atom(bit(line(4), 4)))))).\n" +
                "ac(atom(carry(pSum(3), line(4), 6)), or(and(atom(bit(pSum(3), 5)), atom(bit(line(4), 5))), and(atom(carry(pSum(3), line(4), 5)), xor(atom(bit(pSum(3), 5)), atom(bit(line(4), 5)))))).\n" +
                "ac(atom(carry(pSum(3), line(4), 7)), or(and(atom(bit(pSum(3), 6)), atom(bit(line(4), 6))), and(atom(carry(pSum(3), line(4), 6)), xor(atom(bit(pSum(3), 6)), atom(bit(line(4), 6)))))).\n" +
                "ac(atom(carry(pSum(3), line(4), 8)), or(and(atom(bit(pSum(3), 7)), atom(bit(line(4), 7))), and(atom(carry(pSum(3), line(4), 7)), xor(atom(bit(pSum(3), 7)), atom(bit(line(4), 7)))))).\n" +
                "ac(atom(carry(pSum(3), line(4), 9)), or(and(atom(bit(pSum(3), 8)), atom(bit(line(4), 8))), and(atom(carry(pSum(3), line(4), 8)), xor(atom(bit(pSum(3), 8)), atom(bit(line(4), 8)))))).\n" +
                "ac(atom(bit(x, 6)), c(f)).\n" +
                "ac(atom(bit(y, 6)), c(f)).\n" +
                "ac(atom(bit(z, 0)), c(v)).\n" +
                "ac(atom(bit(z, 1)), c(v)).\n" +
                "ac(atom(bit(z, 2)), c(f)).\n" +
                "ac(atom(bit(z, 3)), c(v)).\n" +
                "ac(atom(bit(z, 4)), c(v)).\n" +
                "ac(atom(bit(z, 5)), c(v)).\n" +
                "ac(atom(bit(z, 6)), c(f)).\n" +
                "ac(atom(bit(z, 7)), c(v)).\n" +
                "ac(atom(bit(z, 8)), c(v)).\n" +
                "ac(atom(bit(z, 9)), c(f)).\n" +
                "ac(atom(bit(z, 10)), c(f)).\n" +
                "ac(atom(bit(z, 11)), c(v)).\n" +
                "ac(atom(bit(z, 12)), c(f)).\n" +
                "ac(atom(bit(y, 0)), atom(bit(y, 0))).\n" +
                "ac(atom(bit(y, 1)), atom(bit(y, 1))).\n" +
                "ac(atom(bit(y, 2)), atom(bit(y, 2))).\n" +
                "ac(atom(bit(y, 3)), atom(bit(y, 3))).\n" +
                "ac(atom(bit(y, 4)), atom(bit(y, 4))).\n" +
                "ac(atom(bit(y, 5)), atom(bit(y, 5))).\n" +
                "ac(atom(bit(x, 0)), atom(bit(x, 0))).\n" +
                "ac(atom(bit(x, 1)), atom(bit(x, 1))).\n" +
                "ac(atom(bit(x, 2)), atom(bit(x, 2))).\n" +
                "ac(atom(bit(x, 3)), atom(bit(x, 3))).\n" +
                "ac(atom(bit(x, 4)), atom(bit(x, 4))).\n" +
                "ac(atom(bit(x, 5)), atom(bit(x, 5))).";

        processStatements();
        System.out.println(completeADF);
    }
}
