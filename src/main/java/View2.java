import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Vector;

public class View2 implements ActionListener
{
    //todo: freeze rest of interface while minimizing
    //todo: adjust sizes of buttons etc.

    //todo: import several ADFs at once from a folder
    private JFrame frame;

    JPanel radioPanel;
    JRadioButton rb1, rb2, rb3;
    JLabel label1, label2, label3, label4;
    JButton chooseButton, minimizeButton, exportButton;
    ButtonGroup radioGroup;

    private JFileChooser fileChooser;

    private Vector<FormulaTree> formulae;
    private Vector<String> minimizedFormulae;
    private String completeADF;
    private Vector<String> statements;
    private Vector<String> substitutes;

    public View2()
    {
        frame = new JFrame();
        GridLayout gL = new GridLayout(4,2);
        frame.setLayout(gL);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // labels & buttons
        label1 = new JLabel("1. Choose ADF:");
        frame.add(label1);

        chooseButton = new JButton("Import");
        frame.add(chooseButton);

        label2 = new JLabel("2. Select minimization algorithm (Scherzo is default)");
        frame.add(label2);

        // radio menu
        radioPanel = new JPanel();
        radioGroup = new ButtonGroup();
        rb1 = new JRadioButton("Scherzo");
        rb2 = new JRadioButton("Karnaugh-Veitch");
        rb3 = new JRadioButton("Quine-McCluskey");
        radioGroup.add(rb1); radioGroup.add(rb2); radioGroup.add(rb3);
        radioPanel.add(rb1); radioPanel.add(rb2); radioPanel.add(rb3);
        frame.add(radioPanel);

        label3 = new JLabel("3. Minimize ADF");
        frame.add(label3);

        minimizeButton = new JButton("Minimize");
        frame.add(minimizeButton);

        label4 = new JLabel("4. Export minimized ADF");
        frame.add(label4);

        exportButton = new JButton("Export");
        frame.add(exportButton);

        rb1.addActionListener(this); rb2.addActionListener(this); rb3.addActionListener(this);
        chooseButton.addActionListener(this); minimizeButton.addActionListener(this); exportButton.addActionListener(this);;

        fileChooser = new JFileChooser();

        minimizedFormulae = new Vector<String>();

        frame.setTitle("ADF Minimizing");
        frame.setSize(500,500);  //todo
        //frame.pack();
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent event)
    {
        if( event.getSource().equals(chooseButton) )
        {
            int returnVal = fileChooser.showOpenDialog(frame);

            if( returnVal == JFileChooser.APPROVE_OPTION )
            {
                formulae = null;
                File file = fileChooser.getSelectedFile();

                try
                {
                    BufferedReader reader = new BufferedReader(new FileReader(file));
                    completeADF = new String();
                    while(reader.readLine() != null)
                        completeADF += reader.readLine();
                    reader.close();
                } catch (IOException e)
                {
                    //errorLabel.setText("Error while reading file.");
                    e.printStackTrace();
                }

                //statement processing
                processStatements();

                minimizedFormulae = new Vector<String>();
                formulae = new Vector<FormulaTree>();
                for(String s : completeADF.split("\\n"))
                {
                    //System.out.println(s);
                    if( s.startsWith("ac(") )
                        formulae.add(new FormulaTree(s));
                }
            }
        }
        else if( event.getSource().equals(minimizeButton) )
        {
            if( completeADF != null )
            {
                minimizedFormulae = new Vector<String>();
                if( rb1.isSelected() )
                    for( FormulaTree formula : formulae )
                    {
                        ScherzoAlgorithms s = new ScherzoAlgorithms(0,0, formula);
                        minimizedFormulae.add(s.minimize());
                    }

                else if( rb2.isSelected() )
                    for( FormulaTree formula : formulae )
                    {
                        KVMinimizer k = new KVMinimizer(formula);
                        minimizedFormulae.add(k.karnaughVeitchMinimization());
                    }

                else if( rb3.isSelected() )
                    for( FormulaTree formula : formulae )
                    {
                        QMCMinimizer q = new QMCMinimizer(formula);
                        minimizedFormulae.add(q.quineMccluskeyMinimization());
                    }
            }
        }
        else if( event.getSource().equals(exportButton) )
        {
            int returnVal = fileChooser.showOpenDialog(frame);
            if( returnVal == JFileChooser.APPROVE_OPTION )
            {
                File file = fileChooser.getSelectedFile();
                String minimizedADF = constructExportFormula();

                if( minimizedADF != null )
                    try
                    {
                        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                        writer.write(minimizedADF);
                        writer.close();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
            }
        }
        /*
        else if( event.getSource().equals(rb1) )
        {
        }
        else if( event.getSource().equals(rb2) )
        {
        }
        else if( event.getSource().equals(rb3) )
        {
        }
        */
        /* //TEMPLATE
        else if( event.getSource().equals() )
        {
            //Todo: add action
        }
        */
    }

    /**
     * Substitutes each statement by a computed Integer number, which are later handled as names of atoms
     */
    private void processStatements()
    {
        if( completeADF == null ) return;

        String[] adfLines = completeADF.split("\\.");
        statements = new Vector<String>();

        //System.out.println(adfLines.length);
        for(int i=0; i < adfLines.length; i++)
        {
            adfLines[i] = adfLines[i].replace("\n", "");
            adfLines[i] += ".";
            if( adfLines[i].startsWith("s(")/* adfLines[i].matches("s(.+)\\.")*/ )
            {
                //System.out.println("Match");
                statements.add(adfLines[i].substring(2, adfLines[i].length()-2));
            }
            //System.out.println("tested: " + adfLines[i]);
        }

        substitutes = new Vector<String>();
        Integer i = 1;
        for( String s : statements )
        {
            //System.out.println("Running: " + s + " -> " + i);
            substitutes.add("<" + i.toString() + ">");
            s = s.trim();

            CharSequence replacement = substitutes.lastElement();
            CharSequence target = s;
            completeADF = completeADF.replace(target, replacement);
            //completeADF = completeADF.replaceAll(s, i.toString());
            i++;
        }

        //System.out.println(completeADF);
    }

    private String constructExportFormula()
    {
        if( completeADF != null && minimizedFormulae.size() != 0 )
        {
            String minimizedADF = "";
            String adfNoWhites = "";
            for(char c : completeADF.toCharArray())
            {
                if(c != ' ') adfNoWhites += c;
            }

            /*int i = 0;
            for(String s : minimizedFormulae)
            {
                adfNoWhites.indexOf("ac(");
                int index = adfNoWhites.indexOf("ac(");
                String nodeName = adfNoWhites.substring(index+3, adfNoWhites.indexOf(",", index+3));
                adfNoWhites = adfNoWhites.replaceFirst("ac\\(.+\\)" , "ac(" + nodeName + "," + s + ").\n");
                //adfNoWhites = adfNoWhites.replaceFirst("ac.+\\." , "ac(" + nodeName + "," + s + ").\n");

                //minimizedADF += adfNoWhites.substring(0, adfNoWhites.indexOf('.')+1); //or +2 to include \n?
                minimizedADF += adfNoWhites.substring(0, index + s.length()+4 + nodeName.length() + 1);
                //line above could also work with minimizedADF = adfNoWhites.substring(0, adfNoWhites.indexOf(".")+1);

                adfNoWhites = adfNoWhites.substring(index + s.length()+2);
                //adfNoWhites = adfNoWhites.substring(0, adfNoWhites.indexOf('.')+1);
                i++;
            }*/

            int i = 0;
            for(String s : adfNoWhites.split("\\n"))
            {

                if( s.startsWith("s(") )
                {
                    minimizedADF += s + "\n";
                    //adfNoWhites = adfNoWhites.substring(0, adfNoWhites.indexOf('.'));
                }
                else if( s.startsWith("ac(") )
                {
                    s = "ac(" + s.substring(3, s.indexOf(',')+1) + minimizedFormulae.get(i) + ").";
                    minimizedADF += s + "\n";
                    i++;
                }
            }

            //resubstitutes original statements back into the formula
            Integer j = 0;
            //System.out.println("CompleteADF before subs.: \n" + minimizedADF);
            for(String s : substitutes)
            {
                //CharSequence target = "<" + j.toString() + ">";
                CharSequence target = s;
                CharSequence replacement = statements.get(j).trim();
                //System.out.println("Substitution: " + target + " -> " + replacement);

                minimizedADF = minimizedADF.replace(target, replacement);

                //minimizedADF = minimizedADF.replaceAll(s, statements.get(i));
                j++;
            }

            //label.setText(completeADF);
            return minimizedADF;
        }
        else return null;
    }

    public void printError(String errMessage)
    {
        /*errorLabel.setText(errMessage);*/
    }
}
