import jdd.zdd.ZDDGraph;

import jdd.bdd.BDD;
import jdd.bdd.NodeTable;
import jdd.bdd.sets.BDDSet;
import jdd.bdd.sets.BDDUniverse;
import jdd.util.Allocator;
import jdd.util.JDDConsole;
import jdd.util.NodeName;
import jdd.zdd.ZDDGraph;
import jdd.zdd.ZDDPrinter;
import sun.reflect.generics.tree.Tree;

import java.util.Arrays;
import java.util.TreeSet;
import java.util.Vector;



//note: 1 could also be zdd.base() and 0 could be zdd.empty()

public class ScherzoAlgorithms extends ZDDGraph
{
    private FormulaTree formula;
    private boolean finished;
    private long finTime;
    private int varNum;
    private Vector<Integer> varsPos;
    private Vector<Integer> varsNeg;

    public ScherzoAlgorithms(int nodesize, int cachesize, FormulaTree formula)
    {
        super(30000, 5000); //todo
        this.formula = formula;
        varsPos = new Vector<Integer>();
        varsNeg = new Vector<Integer>();
        minimize();
    }

    //todo: Q and P are build with inverse variable ordering

    public String minimize()
    {
        //special cases
        if (formula.getTruthTable().isEmpty()) {
            finTime = System.currentTimeMillis();
            finished = true;
            return formula.getTruthTableAsList().get(0).getResult() ? "true" : "false";
        }

        if (!formula.getTruthTable().isFound()) formula.findTruthTable();

        if (formula.getErrorOccured()) {
            finished = true;
            finTime = System.currentTimeMillis();
            return formula.getRepresentedFormula();
        }

        //tautology and contradiction handling
        if (formula.getTruthTable().isTautology() == 0) {
            finished = true;
            finTime = System.currentTimeMillis();
            return "c(v)";
        } else if (formula.getTruthTable().isTautology() == -1) {
            finished = true;
            finTime = System.currentTimeMillis();
            return "c(f)";
        }

        if (formula.getTruthTable().getTruthTable().size() == 2) {
            finished = true;
            finTime = System.currentTimeMillis();
            return formula.getFormulaAtoms().first().getName();
        }

        //----------------------------------------------------------//
        // only keep minterms
        TruthTable onlyTruesTable = new TruthTable();
        int j = 0;
        for(int i = 0; i < formula.getTruthTableAsList().size(); i++)
        {
            if( formula.getTruthTableAsList().get(i).getResult() )
            {
                onlyTruesTable.addRow(formula.getTruthTableAsList().get(i));
                //Vector<Integer> cover = new Vector<Integer>();
                //cover.add(j);
                //onlyTruesTable.getTruthTable().get(j).setCovers(cover);
                j++;
            }
        }
        j = 0;
        onlyTruesTable.setAtomOrder(formula.getTruthTable().getAtomOrder());
        //----------------------------------------------------------//

        //BDD bdd = new BDD(10000,10000);    //todo

        varNum = onlyTruesTable.getAtomOrder().size();

        Vector<Integer> zddVariablesPos = new Vector<Integer>();
        Vector<Integer> zddVariablesNeg = new Vector<Integer>();
        Vector<Integer> bddVariables = new Vector<Integer>();

        for(String s : onlyTruesTable.getAtomOrder())
        {
            zddVariablesPos.add(this.createVar());      //v2, v4, v6, ... (but kept as ints 1, 3, 5, ...)
            zddVariablesNeg.add(this.createVar());      //v1, v3, v5, ... (but kept as ints 0, 2, 4, ...)
            //bddVariables.add(bdd.createVar());
        }
        //System.out.println("zddVariablesPos=" + zddVariablesPos);
        //System.out.println("zddVariablesNeg=" + zddVariablesNeg);

        //varsPos = zddVariablesPos;
        //varsNeg = zddVariablesNeg;

        //invert variable ordering here by simply interchanging vP[0] <-> vP[last]
        for(int l = varNum-1; l >= 0; l--)
        {
            varsPos.add( zddVariablesPos.get(l));
            varsNeg.add( zddVariablesNeg.get(l));
        }
        //System.out.println("varsPos=" + varsPos);

        //Create BDD of f = (A or B) -> (A and C and not D)
        //int tmp1 = bdd.or(bddVariables.get(3), bddVariables.get(2));
        //bdd.ref(tmp1);
        //int tmp2 = bdd.and(bddVariables.get(3), bddVariables.get(1));
        //bdd.ref(tmp2);
        //int tmp3 = bdd.not(bddVariables.get(0));
        //bdd.ref(tmp3);
        //int tmp4 = bdd.and(tmp2, tmp3);
        //bdd.ref(tmp4);
        //bdd.deref(tmp2); bdd.deref(tmp3);
        //int f = bdd.imp(tmp1, tmp4);
        //bdd.ref(f);
        //bdd.deref(tmp1); bdd.deref(tmp4);

        //int[] domains = new int[bddVariables.size()];
        //for(int i = 0; i < bddVariables.size(); i++) domains[i] = bddVariables.get(i);
        //BDDUniverse universe = new BDDUniverse(domains);
        //BDDSet fSet = new BDDSet(universe, f);

        //bdd.printDot("f", f);
        //bdd.printSet(f);
        //System.out.print("bdd.printS(f) = ");
        //bdd.print(f);
        //bdd.print(bdd.getHigh(f));

        //formula.printTruthTable();

        //int Q = mk( 2, diveMinIntoP(bdd.getLow(f), 0, bdd), diveMinIntoP(bdd.getHigh(f), 0, bdd) );
        //int Q = diveMinIntoP(f, 0, varsPos.get(0), bdd);    //todo: check for correctness
        //System.out.println(onlyTruesTable.toZDDString());
        int Q = cubes_union(onlyTruesTable.toZDDString());
        int P = prime2(Q);

        printDot("QReallyFinalQ", Q);
        printDot("PReallyFinalP", P);

        int qTest = Q;
        int pTest = P;

        //compute cyclic core of <Q,P,subseteq>
        TreeSet<Integer> essSet = new TreeSet<Integer>();
        int essentials = 0;
        int qSave, pSave;
        int i = 0;
        do
        {
            //pSave = mk(P, getLow(P), getHigh(P));
            //qSave = mk(Q, getLow(Q), getHigh(Q));
            //System.out.println("_______________________________________");
            pSave = P;
            qSave = Q;
            //System.out.println("Running!");
            //qCubesSave = getCubesAsString(Q);
            //pCubesSave = getCubesAsString(P);

            //try{ Thread.sleep(50); } catch(InterruptedException e) {}

            Q = maxTauP2(P, Q/*, 0*/);
            //System.out.print("Q" + i + ": "); printCubes(Q); //System.out.println();
            int E = intersect(Q, P);

            P = diff(P, E);
            Q = diff(Q, E);
            P = maxTauQ2(Q, P/*, 0*/);  //fixed to-do: P, P???
            if(i == 0) essentials = E;      //save essential prime implicants
            else essentials = unionTo(essentials, E);
            essSet.add(E);

            //System.out.print("P" + i + ": "); printCubes(P);
            //System.out.print("E" + i + ": "); printCubes(E);
            i++;
        } while( diff(qSave, Q) != 0 || diff(pSave, P) != 0);

        //matrix to String
        String minFormula = "";

        //essentials = cubes_intersect("01000110");
        //printDot("01000110", essentials);

        //printSet(Q);
        //printSet(P);
        //cyclic core reduction
        if( P != 0 && Q != 0 )       //branch&bound
        {
            //construct covering matrix
            //System.out.println("count(Q)=" +count(Q) + "    " + "count(P)=" +count(P));
            boolean[][] matrix = new boolean[count(Q)][count(P)];
            Vector<String> pCubes = processCubes(P);
            Vector<String> qCubes = processCubes(Q);

            //System.out.println("q = " + qCubes);
            //System.out.println("p = " + pCubes);
            //System.out.println("q.length:" + qCubes.size() + "    p.length:" + pCubes.size());
            for(i = 0; i < qCubes.size(); i++)
                for(j = 0; j < pCubes.size(); j++)
                    //System.out.println("Testing (" +i+","+j+")");
                    if( testSubset(qCubes.get(i), pCubes.get(j)) ) matrix[i][j] = true;
                    else matrix[i][j] = false;
            //for(i = 0; i < qCubes.size(); i++)
            //{
            //    for(j = 0; j < pCubes.size(); j++)
            //        System.out.print(matrix[i][j] + " ");
            //    System.out.println();
            //}

            //System.out.println("____________________________________________________");
            //for(i = 0; i < matrix.length; i++)
            //{
            //    for(j = 0; j < matrix[0].length; j++)
            //        System.out.print(matrix[i][j] + " ");
            //    System.out.println();
            //}

            Vector<Integer> indices = new Vector<Integer>();
            for(int k=0; k<pCubes.size(); k++) indices.add(k);
            //minSolution = new TreeSet<Integer>();
            solveMatrix(matrix, 0, new TreeSet<Integer>(), computeCosts(pCubes), indices);

            //System.out.println("Solution: " +minSolution);
            Vector<String> solCubes = new Vector<String>();
            for(Integer p : minSolution) solCubes.add(pCubes.get(p));

            minFormula = parseCubes(solCubes);
        }
        else                    //essentials are enough
        {
            //minFormula = parseEssentials(essentials);

            //todo: fix essential parsing
            Vector<String> essCubes = processCubes(essentials);
            //printDot("essentialsTestFormula6", essentials);
            //printSet(essentials);
            //printCubes(essentials);
            //System.out.println(formula.getTruthTable().toZDDString());
            //formula.printTruthTable();
            minFormula = parseCubes(essCubes);
        }


        System.out.println("returning: " + minFormula);
        return minFormula;
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    private float upper = 0;
    //private Vector<Integer> costs;
    private TreeSet<Integer> minSolution;
    //private TreeSet<Integer> removedCols;
    private /*boolean[][]*/void solveMatrix(boolean[][] matrix, float path, TreeSet<Integer> solution, Vector<Integer> costs, Vector<Integer> indices)
    {
        // if maxP is in the min. sol., then the rows it covers can be removed
        // if not, then they still have to be covered
        //if( matrix.length == 0 ) System.out.println("Possible minSol here.");
        if( matrix.length == 0 && (path < upper || minSolution == null) )
        { minSolution = new TreeSet<Integer>(solution); /*System.out.println("MinSol set!");*/ return; }
        //System.out.println("Initial costs: " + costs.toString());
        if( matrix.length == 0 ) return ;
        for( boolean[] m : matrix ) if( m.length == 0 ) return; //dirty fix for an ArrayOutOfBoundsException...

        int maxP = choose(matrix);
        //System.out.println("Element " + maxP +" chosen.");
        Vector<Integer> costCopy = new Vector<Integer>(costs);
        costCopy.removeElementAt(maxP);
        boolean[][] cL = removeColumn(matrix, maxP, false);
        float cLLower = computeLower(computeIndependentSet(cL), costCopy);
        if( path + cLLower >= upper && upper != 0 ) { /*System.out.println("Returning....303");*/ return; }  //left-hand side lower bound
        boolean[][] cR = removeColumn(matrix, maxP, true);


        //System.out.println("Costs later: " + costs.toString());

        boolean[][] indSet = computeIndependentSet(matrix);
        float min=0, lower=computeLower(/*matrix*/indSet, costs);
        Vector<Integer> pDash = new Vector<Integer>();
        for(int i=0; i<indices.size(); i++)
            if( !removedColumns.contains(indices.get(i))
                    && (path + lower + costs.get(indices.get(i)) >= upper) )
                pDash.add(indices.get(i));
        for(Integer p : pDash) { removeColumn(matrix, p, false); removedColumns.add(p); }

        //terminal conditions
        //check if minimal solution has been found
        //if( matrix.length == 0 ) System.out.println("Possible minSol here.");
        if( matrix.length == 0 && (path < upper || minSolution == null) )
            { minSolution = solution; return; }
        //if( (cL.length == 0 && cR.length == 0) || cL[0].length == 0 || cR[0].length == 0 ) { /*System.out.println("Returning....320");*/ return; }

        //branch
        int costP = matrix[0].length;
        for(int i=0; i < matrix[0].length; i++)
            if( matrix[maxP][i] ) costP--;

        Vector<Integer> indicesCopy = indices;
        for(Integer col : removedColumns) indicesCopy.remove(col);
        indicesCopy.removeElement(maxP);
        solveMatrix(cL, path, new TreeSet<Integer>(solution), costs, indicesCopy);
        solution.add(maxP);
        solveMatrix(cR, path + costP, new TreeSet<Integer>(solution), costs, indicesCopy);
    }

    /**
     * Computes the lower bound = \sum_{x\in X'} \min_{y \ xRY} Cost(y)
     */
    private float computeLower(boolean[][] indSet, Vector<Integer> costs)
    {
        float lower = 0;

        //System.out.println("Costs: " + costs.toString());

        for(int i=0; i<indSet.length; i++)
        {
            Vector<Integer> covers = new Vector<Integer>();
            for(int j=0; j<indSet[0].length; j++)
                if( indSet[i][j] ) covers.add(j);

            if( covers.size() == 0 ) return -1;

            float minCost = costs.get(covers.firstElement());
            for(Integer y : covers)
                if( costs.get(y) < minCost ) minCost = costs.get(y);
            lower += minCost;
        }

        return lower;
    }

    private Vector<Integer> removedColumns;
    /**
     * A greedy algorithm computing an independet set of the scp c = <a,b,\in> where c is represented by matrix.
     * Source: "On Solving Covering Problems", Olivier Coudert
     */
    private /*Vector<Vector<Boolean>>*/boolean[][] computeIndependentSet(boolean[][] matrix)
    {
        //System.out.println("Computing independent set.");
        //for(int i = 0; i < matrix.length; i++)
        //{
        //    for(int j = 0; j < matrix[0].length; j++)
        //        System.out.print(matrix[i][j] + " ");
        //    System.out.println();
        //}

        if( matrix[0].length == 1 ) return new boolean[][]{{matrix[0][0]}};

        removedColumns = new Vector<Integer>();
        Vector<Vector<Boolean>> indSet = new Vector<Vector<Boolean>>();
        Vector<Vector<Boolean>> m = new Vector<Vector<Boolean>>();
        for(int j=0; j<matrix.length; j++)
        {
            Vector<Boolean> r = new Vector<Boolean>();
            for(int i=0; i<matrix[0].length; i++) r.add(matrix[j][i]);
            m.add(r);
        }
        while(m.size() > 0)
        {
            Vector<Boolean> y = m.firstElement();
            indSet.add(y);
            Vector<Integer> ys = new Vector<Integer>();
            for(int j=0; j < m.size(); j++)
                for(int i=y.size()-1; i>=0; i--)
                    if( m.get(j).get(i) && m.get(0).get(i) ) { ys.insertElementAt(j,0); /*removedColumns.add(i);*/ }
            int j = 0;
            for(Integer i : ys)
                //for(Vector<Boolean> v : m)
                //    v.removeElementAt(i);
                m.removeElementAt(i);
            m.remove(y);
        }

        boolean[][] ret = new boolean[indSet.size()][indSet.get(0).size()];
        for(int i=0; i<ret.length; i++)
            for(int j=0; j<ret[0].length; j++)
                if( indSet.get(i).get(j) ) ret[i][j] = true;

        //System.out.println("Independent set found!");
        return /*indSet*/ret;
    }

    /**
     * Computes the values of the vector containing the cost of each prime term.
     * The cost of a prime is the number of variables needed to represent it.
     */
    private Vector<Integer> computeCosts(Vector<String> pCubes)
    {
        //if( costs != null ) return;

        Vector<Integer> costs = new Vector<Integer>();
        for(String cube : pCubes)
        {
            int count = 0;
            for(char c : cube.toCharArray())
                if( c == '1' ) count++;
            costs.add(count);
        }
        return costs;
    }

    /**
     * Removes column with index i from matrix.
     * If removeRows is set, then all rows covered by the column i are removed too.
     */
    private boolean[][] removeColumn(boolean[][] matrix, int i, boolean removeRows)
    {
        TreeSet<Integer> rows = new TreeSet<Integer>();
        boolean[][] newMatrix;

        if( removeRows )
        {
            for(int j = 0; j < matrix.length; j++)
                if( matrix[j][i] ) rows.add(j);
            //newMatrix = new boolean[matrix.length-rows.size()][matrix[0].length-1];
        }
        newMatrix = new boolean[matrix.length][matrix[0].length-1];

        boolean continued = false;
        for(int j = 0; j < newMatrix.length; j++)
        {
            int l = 0;
            for(int k = 0; k < newMatrix[0].length + 1; k++)
            {
                if( k == i ) { continued = true; continue;}
                if( !continued ) { newMatrix[j][l] = matrix[j][k]; l++; }
                else if( continued ) { newMatrix[j][l] = matrix[j][k]; l++; }
            }
        }
        if( !removeRows ) return newMatrix;

        boolean[][] newNewMatrix = new boolean[matrix.length-rows.size()][matrix[0].length-1];
        int j=0, l=0, k=0;
        while( j<newNewMatrix.length && l < matrix.length )
        {
            if( !rows.contains(l) )
            {
                k = 0;
                while( k < newNewMatrix[0].length )
                {
                    newNewMatrix[j][k] = newMatrix[l][k];
                    k++;
                }
                j++; l++;
            }
            else l++;
        }

        return newNewMatrix;
    }

    /**
     * Removes row with index i from matrix
     */
    private boolean[][] removeRow(boolean[][] matrix, int i)
    {
        boolean[][] newMatrix = new boolean[matrix.length-1][matrix[0].length];

        boolean continued = false;
        for(int j = 0; j < newMatrix.length+1; j++) {
            if (j == i) { continued = true; continue; }
            for (int k = 0; k < newMatrix[0].length; k++)
            {
                if (!continued) newMatrix[j][k] = matrix[j][k];
                else newMatrix[j-1][k] = matrix[j][k];
            }
        }

        return newMatrix;
    }

    /**
     * Parses a set of cubes (Strings of the form {0,1}^n) into a corresponding formula.
     */
    private String parseCubes(Vector<String> cubes)
    {
        String minFormula = "";

        String[] products = new String[cubes.size()];
        cubes.copyInto(products);
        if( products.length > 1 ) minFormula += "or(";

        //System.out.println(Arrays.toString(products));

        int j = 0;
        for(String cube : products)
        {
            //sometimes leading 0s are removed from cube and this causes problems later on, so add them back
            //System.out.println(varNum);
            while( cube.length() < 2*varNum ) cube = new String("0".concat(cube));
            //System.out.println("cube:" + cube);

            //String s = "0".concat(cube);
            //System.out.println("cube:" + s);

            int ones = 0;
            for(char c : cube.toCharArray()) if( c == '1' ) ones++;

            if( ones > 1 ) minFormula += "and(";
            int i = 0;
            int ones2 = ones;
            while(cube.length() >= 2)
            {
                if( cube.charAt(0) == '0' && cube.charAt(1) == '1' )
                {
                    minFormula += formula.getTruthTable().getAtomOrder().get(i);
                    ones2--;
                    if( ones2 >= 1 ) minFormula += ",";
                }
                else if( cube.charAt(0) == '1' && cube.charAt(1) == '0' )
                {
                    minFormula += "neg(" + formula.getTruthTable().getAtomOrder().get(i) + ")";
                    ones2--;
                    if( ones2 >= 1 ) minFormula += ",";
                }

                //if(minFormula.endsWith("neg(a)")) System.out.println( cube.charAt(2) + " " + cube.charAt(3));
                i++;
                //if( cube.length() >= 4 && i > 0)
                //    if( (cube.charAt(2) != '0' || cube.charAt(3) != '0') ) minFormula += ",";

                cube = cube.substring(2);
            }
            if( ones > 1 ) minFormula += ")";
            if( j < products.length-1 ) minFormula += ",";
            j++;
        }
        if( products.length > 1 ) minFormula += ")";

        return minFormula;
    }

    /**
     * Parses the ZDD essentials into a formula representing the cubes contained in it.
     */
    private String parseEssentials(int essentials)
    {
        String minFormula = "";
        if( count(essentials) == 0 ) return "c(f)";    //or true?
        else if( count(essentials) == 1 )
        {
            minFormula += "and(";
            int var = getVar(essentials);
            int node = essentials;
            while(var >= 0)
            {
                //System.out.println(var);
                if( varsPos.contains(var) ) minFormula += formula.getTruthTable().getAtomOrder().get(varsPos.indexOf(var));
                else if( varsNeg.contains(var) ) minFormula += "neg(" +formula.getTruthTable().getAtomOrder().get(varsNeg.indexOf(var)) + ")";
                node = getHigh(node);
                var = getVar(node);
                //System.out.println(var);
                if( var >= 0 ) minFormula+=",";
            }
            minFormula += ")";
        }
        else
        {
            String str = getCubesAsString(essentials);
            String cubes = "";

            for( char c : str.toCharArray() )
                if( !(c == ',' || c == '{' || c == '}') ) cubes += c;
            cubes = cubes.trim();
            String[] products = cubes.split("\\s");

            minFormula += "or(";

            int j = 0;
            for(String cube : products)
            {
                minFormula += "and(";
                int i = 0;
                while(cube.length() >= 2)
                {
                    if( cube.charAt(0) == '0' && cube.charAt(1) == '1' )
                        minFormula += formula.getTruthTable().getAtomOrder().get(i);
                    else if( cube.charAt(0) == '1' && cube.charAt(1) == '0' )
                        minFormula += "neg(" + formula.getTruthTable().getAtomOrder().get(i) + ")";

                    //if(minFormula.endsWith("neg(a)")) System.out.println( cube.charAt(2) + " " + cube.charAt(3));

                    i++;
                    if( cube.length() >= 4 && i > 0)
                        if( (cube.charAt(2) != '0' || cube.charAt(3) != '0') ) minFormula += ",";

                    cube = cube.substring(2);
                }
                minFormula += ")";
                if( j < products.length-1 ) minFormula += ",";
                j++;
            }


            //minFormula += "or(";

            //minFormula = cubes;

            //minFormula += ")";
        }

        //System.out.println(minFormula);

        return minFormula;
    }

    /**
     * computes max Choose(p) = Sum_{q \subseteq p}{1/(countOfAllPCoveringq-1)}
     * and returns the index of the maximal element
     */
    private int choose(boolean[][] matrix)
    {
        //System.out.println("matrix[0].length in choose(): " + matrix[0].length);

        float[] choose = new float[matrix[0].length];
        int[] coverCount = new int[matrix.length];
        for(int i=0; i < coverCount.length; i++)
            for(int j=0; j<choose.length; j++)
                if( matrix[i][j] ) coverCount[i]++;

        for(int k = 0; k < choose.length; k++)
            for(int i=0; i<coverCount.length; i++)
                if( matrix[i][k] ) //choose[k] += (coverCount[i] !=0)? 1/(coverCount[i]-1) : 0.1;
                    if( coverCount[i] != 1 ) choose[k] += 1/(coverCount[i]-1);
                    else choose[k] += 1/coverCount[i];

        /*for( int k = 0; k < choose.length; k++)
        {
            float sum = 0;
            for (int j = 0; j < matrix.length; j++)

                for (int i = 0; i < matrix[0].length; i++)   //for each q \subseteq p
                    if (matrix[j][i]) {
                        int count = 0;
                        for(int l = 0; l < matrix[0].length; l++)
                            if( matrix[j][l] ) count++;
                        //count--;
                        if( count-1 == 0 ) sum += 0.1;
                        else sum += 1 / (count-1);
                    }
            choose[k] = sum;
        }*/

        float max = choose[0];
        int maxIndex = 0;
        for(int i = 1; i < choose.length; i++)
            if( choose[i] > max ) { max = choose[i]; maxIndex = i; }

        //System.out.println("Choose: " + Arrays.toString(choose));
        //return choose;
        return maxIndex;
    }

    /**
     * Tests whether q \subseteq p is true for two cubes p and q
     * todo: test this
     */
    private boolean testSubset(String q, String p)
    {
        //System.out.println("comparing: "); System.out.println(q + "\n" + p);
        int i = 0;
        for(char c : q.toCharArray())
        {
            //int diff = 0;
            //if( c != p.charAt(i) ) diff++;
            if( c == '0' && p.charAt(i) == '1' ) return false;
            i++;
        }
        return true;
    }

    /**
     * Returns a String-token for each cube in the ZDD Q
     */
    private Vector<String> processCubes(int Q)
    {
        String str = getCubesAsString(Q);
        //printSet(Q);
        String cubes = "";
        //System.out.println("str= " +str + "// ");

        for( char c : str.toCharArray() )
            if( !(c == ',' || c == '{' || c == '}') ) cubes += c;
        cubes = cubes.trim();
        //System.out.println("cubes= " +cubes);
        String[] products = cubes.split("\\s");

        Vector<String> vector = new Vector<String>();
        for(String p : products)
                vector.add(p);

        //System.out.println(vector);
        //System.out.println("processCubes returning: ");
        //for(int i=0; i<products.length; i++) System.out.print(products[i] + " ");

        return vector;
    }

    //private String essRec(int essentials, String currFormula)
    //{
    //    if( essentials == 0 ) { currFormula = ""; return ""; }
    //    if( essentials == 1 ) return currFormula;

    //    int var = getVar(essentials);
    //    if( !currFormula.endsWith("(") ) currFormula += ",";
    //    if( varsPos.contains(var) ) currFormula += formula.getTruthTable().getAtomOrder().get( varsPos.indexOf(var));
    //    else if( varsNeg.contains(var) ) currFormula += "neg(" +formula.getTruthTable().getAtomOrder().get(varsNeg.indexOf(var)) + ")";

    //    String highFormula = currFormula;
    //    highFormula = essRec(getHigh(essentials), highFormula);
    //    String lowFormula = currFormula;
    //    lowFormula = essRec(getLow(essentials), lowFormula);

    //    //if( highFormula != "" && lowFormula != "") lowFormula+=",";
    //    return lowFormula+highFormula;
    //}

    //private float global;

    public String buildProducts(int zdd)
    {
        String products = "";

        //printCubes(zdd);
        //printDot("zddBP", zdd);

        String cubes = getCubesAsString(zdd);
        //System.out.println("cubes: " + cubes);
        cubes = cubes.substring(1, cubes.length()-2);
        cubes = cubes.trim();

        String noWhitesCubes = "";
        for(char c : cubes.toCharArray())
            if( c != ',' ) noWhitesCubes += c;

        //System.out.println("noWhitesCubes: " + noWhitesCubes);

        return products;
    }
    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * Returns the ZDD of Q from the BDD of f
     */
    public int diveMinIntoP(int f, int k, int currVar/*, int aNode*/, BDD bdd)
    {
        /*
        if( aNode == -1 )
        {
            aNode = add(2, 0, 0);
            //create new node...//todo: continue here
        }*/

        if( f == 0 ) return 0;
        if( k+1 == varNum )
        {
            int low = 1;
            int high = 1;
            if( bdd.getLow(f) == 0 ) low = 0;
            if( bdd.getHigh(f) == 0 ) high = 0;
            return mk(currVar, low, high);
            //return 1;
        }

        return mk(currVar,
                diveMinIntoP(bdd.getLow(f), k+1, varsNeg.get(k+1),/*getLow(aNode),*/ bdd),
                diveMinIntoP(bdd.getHigh(f), k+1, varsPos.get(k+1),/*getHigh(aNode),*/ bdd));
    }

    /**
     * Returns the ZDD representing the set of prime terms of the BDD f
     */
    public int prime(int f, BDD bdd, int k)
    {
        if(f == 0) return 0;
        else if(f == 1) return 1;

        //if( k >= varsPos.size() ) { return 0; } //....
        //int f0 = subset0(f, vars.get(k));
        //int f1 = subset1(f, vars.get(k));
        int f0 = bdd.getLow(f);
        int f1 = bdd.getHigh(f);

       // int p = prime( bdd.not(bdd.nand(f0, f1)), zdd, bdd, vars, k+2);
        int p = prime(bdd.and(bdd.getLow(f), bdd.getHigh(f)), bdd, k + 1);       //todo: possible error here
        //int p = prime( intersect(f0,f1), zdd, bdd, vars, k+2 );
        int p0 = diff( prime(f0, bdd, k+1), p );
        int p1 = diff( prime(f1, bdd, k+1), p );
        //System.out.println("Returning mk(...)");
        //return mk( varsPos.get(k), mk(varsNeg.get(k), p, p0), p1);
        return union(p, union(change(p0, varsNeg.get(k)), change(p1, varsPos.get(0))));
    }

    public int prime2(int f)
    {
        if( f == 0 ) return 0;
        if( f == 1 ) return 1;

        int varNeg, varPos;
        if( varsPos.contains(getVar(f)) )
        {
            varPos = getVar(f);
            varNeg = varsNeg.get( varsPos.indexOf(varPos) );
        }
        else if( varsNeg.contains(getVar(f)) ) //getVar(P) in varsNeg
        {
            varPos = varsPos.get( varsNeg.indexOf(getVar(f)) );
            varNeg = getVar(f);
        }
        else {
            System.out.println("Error in prime2");
            return -1;        //error
        }



        int fNoK, fNegK, fPosK;
        fNegK = subset1(f, varNeg);
        fPosK = subset1(f, varPos);
        fNoK = subset0(subset0(f, varPos), varNeg);

        int P = prime2( intersect(fNegK, fPosK) );
        int P0 = diff( prime2(fNegK), P );
        int P1 = diff( prime2(fPosK), P );

        /*System.out.println("_________________________________________________________");
        System.out.println("Returning in Prime2!");
        System.out.println("varPos = " + varPos);
        System.out.println("varNeg = " + varNeg);
        System.out.print("f = "); printSet(f);
        System.out.print("P = "); printSet(P);
        System.out.print("P0 = "); printSet(P0);
        System.out.print("P1 = "); printSet(P1);
        System.out.println("__________________________________________________________");*/

        //return mk( varPos, mk(varNeg, P, P0), P1);
        return union(P,
                union(change(P0, varNeg),
                        change(P1, varPos)));
    }

    public int maxTauP(int P, int Q,  int k)
    {
        /*if( k+1 == varNum )
        {
            // continue here with fix from diveMinIntoP --- shouldnt be working
            //check what this function would return for maximal k and return it directly from here
        }*/

        if( P == 0 || Q == 0 )
        {
            //System.out.println("End0!");
            return 0;
        }
        if( P == 1 )
        {
            //System.out.println("End1");
            return 1;
        }
        if( !emptyIn(P) && Q == 1 ) {
            //System.out.println("Fire!");
            return 0;
        }

        if( k == varsPos.size() ) return 0; //todo

        int lowP = subset0(P, k);
        int lowQ = 0;
        int lowLowP = 0, lowLowQ = 0;
        if( Q != 1 && Q != 0 )
        {
            lowQ = subset0(Q, varsPos.get(k));
            //int lowQ = getLow(Q);
            //int lowQ = subset0(Q, vars.get(k));
            if (lowQ == 0 || lowQ == 1) lowLowQ = 0;
            //else lowLowQ = getLow(lowQ);
            else lowLowQ = subset0(lowQ, varsNeg.get(k));
        }

        /*int K = mk(getLow(getLow(Q)),
                zdd.noSubset(getLow(Q), getLow(P)),
                zdd.noSubset(getHigh(Q), getHigh(P)));*/
        /*int K = mk( vars.get(k),
                mk(vars.get(k+1), getLow(getLow(Q)), notSubSet(getLow(Q), getLow(P), zdd, vars, k+2)),
                notSubSet(getHigh(Q), getHigh(P), zdd, vars, k+2));*/
        /*Vector<Integer> reducedVars1 = vars;
        reducedVars1.remove(0);
        Vector<Integer> reducedVars2 = vars;
        reducedVars2.remove(0);
        reducedVars2.remove(0);*/
        /*int K = mk(vars.get(k),
                mk(vars.get(k + 1), lowLowQ, notSubSet(lowQ, lowP, zdd, vars, k + 2)),
                notSubSet(getHigh(Q), getHigh(P), zdd, vars, k + 2));
        int R = maxTauP(lowLowP, getVar(K), zdd, vars, k + 2);      //todo: getVar(K)???
        int R0 = maxTauP(zdd.union(lowLowP, lowP),
                lowQ, zdd, vars, k + 2);
        int R1 = maxTauP( zdd.union(lowLowP, getHigh(P)),
                getHigh(Q), zdd, vars, k+2);int K = mk(vars.get(k),
                mk(vars.get(k + 1), lowLowQ, notSubSet(lowQ, lowP, zdd, vars, k + 2)),
                notSubSet(getHigh(Q), getHigh(P), zdd, vars, k + 2));
        int R = maxTauP(lowLowP, getVar(K), zdd, vars, k + 2);      //todo: getVar(K)???
        int R0 = maxTauP(zdd.union(lowLowP, lowP),
                lowQ, zdd, vars, k + 2);
        int R1 = maxTauP( zdd.union(lowLowP, getHigh(P)),
                getHigh(Q), zdd, vars, k+2);*/
        /*int K = mk(varsPos.get(k),
                mk(varsNeg.get(k), lowLowQ, notSubSet(lowQ, lowP, k+1)),
                notSubSet(getHigh(Q), getHigh(P), k+1));*/
        int K = union( lowLowQ, union(notSubSet2(lowQ, lowP/*, k*/), notSubSet2(getHigh(Q), getHigh(P)/*, k+1*/)));    //k? or k+1?
        int R = maxTauP(lowLowP, K, k+1);      //todo: getVar(K)???
        int R0 = maxTauP(union(lowLowP, lowP),
                lowQ, k+1);
        int R1 = maxTauP( union(lowLowP, getHigh(P)),
                getHigh(Q), k+1);

       // return mk(R, noSubset(R0, R), noSubset(R1, R));
        return mk( varsPos.get(k),
                   mk( varsNeg.get(k), R, notSubSet2(R0, R/*, k+1*/) ),    //k? or k+1?
                   notSubSet2(R1, R/*, k+1*/) );                           // ^
    }
//

    public int notSubSet(int P, int Q, int k)
    {
        if( Q == 0 ) return P;
        if( P == 0 || emptyIn(Q) ) return 0;
        if( P == 1 ) return 1;

        if( k == varsPos.size() )
        {
            System.out.println("notSubset: k == varsPos.size()");
            return -1; //todo
        }

        /*int ret = mk( notSubSet(getLow(getLow(P)), getLow(getLow(Q)), zdd),
                notSubSet(getLow(P), union(getLow(getLow(Q)), getLow(Q)), zdd),
                notSubSet(getHigh(P), union(getLow(getLow(Q)), getHigh(Q)), zdd) );*/

        int noK = notSubSet( getLow(getLow(P)), getLow(getLow(Q)), k+1 );
        int negK = notSubSet( getLow(P), union(getLow(getLow(Q)), getLow(Q)), k+1 );
        int posK = notSubSet( getHigh(P), union(getLow(getLow(Q)), getHigh(Q)), k+1 );

        int ret;
        ret = mk(varsPos.get(k),
                mk(varsNeg.get(k), noK, negK),
                posK);

        return ret;
    }

    public int maxTauP2(int P, int Q)
    {
        if( P == 0 || Q == 0 ) return 0;
        if( P == 1 ) return 1;
        if( !(emptyIn(P)) && Q == 1 ) return 0;

        int varNeg, varPos;
        if( varsPos.contains(getVar(P)) )   //todo: what about Q?
        {
            varPos = getVar(P);
            varNeg = varsNeg.get( varsPos.indexOf(varPos) );
        }
        else if( varsNeg.contains(getVar(P)) ) //getVar(P) in varsNeg
        {
            varPos = varsPos.get( varsNeg.indexOf(getVar(P)) );
            varNeg = getVar(P);
        }
        else {
            System.out.println("Error in maxTauP2");
            return -1;        //error
        }

        //System.out.println("______________maxTauP2________________");
        //System.out.print("Q: "); printSet(Q);
        //System.out.print("P: "); printSet(P);

        //System.out.println("varPos = " + varPos);
        //System.out.println("varNeg = " + varNeg);

        int PNoK, PNegK, PPosK, QNoK, QNegK, QPosK;
        PNegK = subset1(P, varNeg);
        PPosK = subset1(P, varPos);
        PNoK = subset0(subset0(P, varPos), varNeg);
        QNegK = subset1(Q, varNeg);
        QPosK = subset1(Q, varPos);
        QNoK = subset0(subset0(Q, varPos), varNeg);

        //System.out.print("PNegK: "); printSet(PNegK);       //part above is working!
        //System.out.print("PPosK: "); printSet(PPosK);
        //System.out.print("PNoK: "); printSet(PNoK);
        /*System.out.print("QNegK: "); printSet(QNegK);
        System.out.print("QPosK: "); printSet(QPosK);
        System.out.print("QNoK: "); printSet(QNoK);*/

        //System.out.print("NegK x PNegK: "); printSet(change(PNegK, varNeg));
        //System.out.print("PosK x PPosK: "); printSet(change(PPosK, varPos));

        //int KTmp1 = notSubSet2(PNegK, QNegK);
        //int KTmp2 = notSubSet2(PPosK, QPosK);
        //int KTmp =  union(KTmp1, KTmp2);
        int K = union( QNoK,  union(notSubSet2(QNegK, PNegK), notSubSet2(QPosK, PPosK)) );  //tick  //todo; switched PNegK<->QNegK and PPosK<->QPosK
        int R = maxTauP2(PNoK, K);                                                          //
        int R0 = maxTauP2( union(PNoK, PNegK), QNegK );                                     //
        int R1 = maxTauP2( union(PNoK, PPosK), QPosK );                                     //

        //System.out.print("KTmp: "); printSet(KTmp);
        //System.out.print("K: "); printSet(K);
        //System.out.print("R: "); printSet(R);
        //System.out.print("R0: "); printSet(R0);
        //System.out.print("R1: "); printSet(R1);

        //System.out.println("______________/maxTauP2________________");

        /*return mk( varPos,
                mk(varNeg, R, notSubSet2(R0, R)),
                notSubSet2(R1, R) );*/
        return union( R,
                union( change(notSubSet2(R0,R), varNeg),
                        change(notSubSet2(R1,R), varPos) ) );
    }

    public int notSubSet2(int P, int Q)
    {
        if( Q == 0 ) return P; //todo
        if( P == 0 || emptyIn(Q) ) return 0;
        if( P == 1 ) return 1;

        int varNeg, varPos;
        if( varsPos.contains(getVar(P)) )   //todo: what about Q?
        {
            varPos = getVar(P);
            varNeg = varsNeg.get( varsPos.indexOf(varPos) );
        }
        else //getVar(P) in varsNeg
        {
            varPos = varsPos.get( varsNeg.indexOf(getVar(P)) );
            varNeg = getVar(P);
        }

        int PNoK, PNegK, PPosK, QNoK, QNegK, QPosK;
        PNegK = subset1(P, varNeg);
        PPosK = subset1(P, varPos);
        PNoK = subset0( subset0(P, varPos), varNeg);
        QNegK = subset1(Q, varNeg);
        QPosK = subset1(Q, varPos);
        QNoK = subset0(subset0(Q, varPos), varNeg);

        /*return mk( varPos,      //todo: for swapped varorder, should varPos be swapped with varNeg too?
                mk( varNeg, notSubSet2(PNoK, QNoK), notSubSet2(PNegK, union(QNoK, QNegK)) ),
                notSubSet2(PPosK, union(QNoK, QPosK) )
        );*/
        return union( notSubSet2(PNoK, QNoK),
                union( change(notSubSet2(PNegK,union(QNoK, QNegK)), varNeg),
                        change(notSubSet2(PPosK,union(QNoK, QPosK)), varPos)) );
    }

    public int maxTauQ(int Q, int P, int k)
    {
        /*if( k+1 == varNum )
        {
            // continue here with fix from diveMinIntoP --- should not be working here
            //check what this function would return for maximal k and return it directly from here
        }*/
        if( Q == 0 || P == 0 ) return 0;
        if( emptyIn(Q) || emptyIn(P) ) return 1;

        int lowLowP, lowLowQ;
        if( !(P == 0 || P == 1 || P < 0) )
        {
            if (getLow(P) == 0 || getLow(P) == 1) lowLowP = 0;
            else lowLowP = getLow(getLow(P));
        } else { return 0; } //todo
        if( !(Q == 0 || Q == 1 || Q < 0) )
        {
            //System.out.println("Q error: " + Q);
            if (getLow(Q) == 0 || getLow(Q) == 1) lowLowQ = 0;
            else lowLowQ = getLow(getLow(Q));
        } else { return 0; } //todo
        /*int K = zdd.union(this.supSet(getLow(getLow(P)), getLow(getLow(Q)), this),
         zdd.intersect(this.supSet(getLow(getLow(P)), getLow(Q), this), supSet(getLow(getLow(P)), getHigh(Q), this)));*/
        int K = union(supSet(lowLowP, lowLowQ, k + 1),
                intersect(supSet(lowLowP, getLow(Q), k + 1), supSet(lowLowP, getHigh(Q), k + 1)));
        int R = maxTauQ(union(union(lowLowQ, getLow(Q)), getHigh(Q)),
                K,  k+1);
        int R0 = maxTauQ(getLow(Q), union(lowLowP, getLow(P)),
                 k+1);
        int R1 = maxTauQ(getHigh(Q), union(lowLowP, getHigh(P)),
                 k+1);
        //getLow(getLow(X)) = subset0(subset0(X, posVar(k)), negVar(k))
        //

        /*int lowLowP = subset0(subset0(P, getVar(varsPos.get(k))), getVar(varsNeg.get(k)));
        int lowLowQ = subset0(subset0(Q, getVar(varsPos.get(k))), getVar(varsNeg.get(k)));
        int lowP = subset0(P, varsPos.get(k));
        int lowQ = subset0(Q, varsPos.get(k));

        int K = union(supSet(lowLowP, lowLowQ, k + 1),
                intersect(supSet(lowLowP, lowQ, k + 1), supSet(lowLowP, getHigh(Q), k + 1)));
        int R = maxTauQ(union(union(lowLowQ, lowQ), getHigh(Q)),
                getVar(K),  k+1);
        int R0 = maxTauQ(lowQ, union(lowLowP, lowP),
                k+1);
        int R1 = maxTauQ(getHigh(Q), union(lowLowP, getHigh(P)),
                k+1);*/

        //return mk(R, noSubset(R0, R), noSubset(R1, R));
        return mk(varsPos.get(k),
                mk(varsNeg.get(k), R, notSubSet(R0, R, k + 1)),
                notSubSet(R1, R, k + 1) //todo: possibly only k
        );
    }

    public int supSet(int P, int Q, int k)
    {
        if( P == 0 || Q == 0 ) return 0;
        if( P == 1 ) return 1;
        if( !emptyIn(P) && Q == 1 ) return 0;

        /*return mk( supSet(getLow(getLow(P)), mk(getLow(getLow(Q)), getLow(Q), getHigh(Q)), zdd),
                        supSet(getLow(P), getLow(Q), zdd),
                        supSet(getHigh(P), getHigh(Q), zdd));*/

        int noK = supSet( getLow(getLow(P)), union(union(getLow(getLow(Q)), getLow(Q)), getHigh(Q)), k+1 );
        int negK = supSet( getLow(P), getLow(Q), k+1);
        int posK = supSet( getHigh(P), getHigh(Q), k+1);

/*        int lowP = subset0(P, getVar(varsPos.get(k)));
        int lowQ = subset0(Q, getVar(varsPos.get(k)));
        int lowLowP = subset0(subset0(P, getVar(varsPos.get(k))), getVar(varsNeg.get(k)));
        int lowLowQ = subset0(subset0(Q, getVar(varsPos.get(k))), getVar(varsNeg.get(k)));

        int noK = supSet( lowLowP, union(union(lowLowQ, lowQ), getHigh(Q)), k+1 );
        int negK = supSet( lowP, lowQ, k+1);
        int posK = supSet( getHigh(P), getHigh(Q), k+1);*/

        return mk( varsPos.get(k),
                   mk( varsNeg.get(k), noK, negK),
                   posK );
    }

    public int maxTauQ2(int Q, int P)
    {
        if( Q == 0 || P == 0 ) return 0;
        if( emptyIn(Q) && emptyIn(P) ) return 1;

        int varNeg, varPos;
        if( varsPos.contains(getVar(Q)) )   //todo: what about Q?
        {
            varPos = getVar(Q);
            varNeg = varsNeg.get( varsPos.indexOf(varPos) );
        }
        else //getVar(P) in varsNeg
        {
            varPos = varsPos.get( varsNeg.indexOf(getVar(Q)) );
            varNeg = getVar(Q);
        }

        int PNoK, PNegK, PPosK, QNoK, QNegK, QPosK;
        PNegK = subset1(P, varNeg);
        PPosK = subset1(P, varPos);
        PNoK = subset0(subset0(P, varPos), varNeg);
        QNegK = subset1(Q, varNeg);
        QPosK = subset1(Q, varPos);
        QNoK = subset0(subset0(Q, varPos), varNeg);

        int K = union( supSet2(PNoK, QNoK), intersect(supSet2(PNoK, QNegK), supSet2(PNoK, QPosK)) );
        int R = maxTauQ2( union(QNoK, union(QNegK, QPosK)), K );
        int R0 = maxTauQ2( QNegK, union(PNoK, PNegK) );
        int R1 = maxTauQ2( QPosK, union(PNoK, PPosK) );

        /*return mk( varPos,
                mk(varNeg, R, notSubSet2(R0, R)),
                notSubSet2(R1, R));*/
        return union( R,
                union( change(notSubSet2(R0,R), varNeg),
                        change(notSubSet2(R1,R), varPos)));
    }

    public int supSet2(int P, int Q)
    {
        if( P == 0 || Q == 0 ) return 0;
        if( P == 1 ) return 1;
        if( !emptyIn(P) && Q == 1 ) return 0;

        int varNeg, varPos;
        if( varsPos.contains(getVar(P)) )   //todo: what about Q?
        {
            varPos = getVar(P);
            varNeg = varsNeg.get( varsPos.indexOf(varPos) );
        }
        else //getVar(P) in varsNeg
        {
            varPos = varsPos.get( varsNeg.indexOf(getVar(P)) );
            varNeg = getVar(P);
        }

        int PNoK, PNegK, PPosK, QNoK, QNegK, QPosK;
        PNegK = subset1(P, varNeg);
        PPosK = subset1(P, varPos);
        PNoK = subset0( subset0(P, varPos), varNeg);
        QNegK = subset1(Q, varNeg);
        QPosK = subset1(Q, varPos);
        QNoK = subset0(subset0(Q, varPos), varNeg);

        /*return mk( varPos,
                mk(varNeg, supSet2(PNoK, union(union(QNoK, QNegK), QPosK)), supSet2(PNegK, QNegK)),
                supSet2(PPosK, QPosK)
                );*/
        return union( supSet2(PNoK, union(QNoK,union(QNegK,QPosK))),
                union( change(supSet2(PNegK, QNegK), varNeg),
                        change(supSet2(PPosK, QPosK), varPos)));

    }

    public boolean baseIn(int P)
    {
        boolean ret;

        if( getLow(P) == 1 || getHigh(P) == 1 ) return true;
        if( getLow(P) == 0 && getHigh(P) == 0 ) return false;

        return (baseIn(getLow(P)) || baseIn(getHigh(P)));
    }

    /* -------------------------------------------------------------------------------------------------------------- */

    public String getCubesAsString(int var1) {
        return ZDDPrinterTool.printSet(var1, this, (NodeName) null, num_vars);
    }

    @Override
    public void print(int zdd) { /*ZDDPrinterMod.print(zdd, this, nodeNames);*/ super.print(zdd);	}

    @Override
    public void printDot(String fil, int bdd) {	/*ZDDPrinterMod.printDot(fil, bdd, this, nodeNames);*/ super.printDot(fil, bdd);	}

    @Override
    public void printSet(int bdd) {	/*ZDDPrinterMod.printSet(bdd, this, nodeNames, varNum);*/ super.printSet(bdd);	}

    @Override
    public void printCubes(int bdd) {	/*ZDDPrinterMod.printSet(bdd, this, null, varNum);*/super.printCubes(bdd);	}

    /* -------------------------------------------------------------------------------------------------------------- */

    public static void test()
    {
        //Don't completly trust these tests. Minimizer can sometimes produce a formula with swapped disjunctions.
        String f1 = "ac(s,or(and(<2>,<3>), and(<4>, xor(<2>,<3>))))";
        String s1 = "or(and(<2>,<3>),and(<2>,<4>),and(<3>,<4>))";
        ScherzoAlgorithms t1 = new ScherzoAlgorithms(1000,1000, new FormulaTree(f1));
        String m1 = t1.minimize();
        System.out.println("Control result: " + s1);
        System.out.println("Minimized formula: " + m1);
        if( s1.equals(m1) ) System.out.println("Formula 1 ..... successful.");
        else System.out.println("Formula 1 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f2 = "ac(s,and(A, or(neg(A),B)))";
        String s2 = "and(A,B)".toLowerCase();
        ScherzoAlgorithms t2 = new ScherzoAlgorithms(1000,1000, new FormulaTree(f2));
        String m2 = t2.minimize();
        System.out.println("Control result: " + s2);
        System.out.println("Minimized formula: " + m2);
        if( s2.equals(m2) ) System.out.println("Formula 2 ..... successful.");
        else System.out.println("Formula 2 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f3 = "ac(s,or(c(f),c(v)))";
        String s3 = "c(v)";
        ScherzoAlgorithms t3 = new ScherzoAlgorithms(1000,1000, new FormulaTree(f3));
        String m3 = t3.minimize();
        System.out.println("Control result: " + s3);
        System.out.println("Minimized formula: " + m3);
        if( s3.equals(m3) ) System.out.println("Formula 3 ..... successful.");
        else System.out.println("Formula 3 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f4 = "ac(s,and(A,B,C,neg(D)))";
        String s4 = "and(A,B,C,neg(D))".toLowerCase();
        ScherzoAlgorithms t4 = new ScherzoAlgorithms(1000,1000, new FormulaTree(f4));
        String m4 = t4.minimize();
        System.out.println("Control result: " + s4);
        System.out.println("Minimized formula: " + m4);
        if( s4.equals(m4) ) System.out.println("Formula 4 ..... successful.");
        else System.out.println("Formula 4 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f5 = "ac(s,and(A,B,C,neg(D)))";
        String s5 = "and(A,B,C,neg(D))".toLowerCase();
        ScherzoAlgorithms t5 = new ScherzoAlgorithms(1000,1000, new FormulaTree(f5));
        String m5 = t5.minimize();
        System.out.println("Control result: " + s5);
        System.out.println("Minimized formula: " + m5);
        if( s5.equals(m5) ) System.out.println("Formula 5 ..... successful.");
        else System.out.println("Formula 5 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f6 = "ac(s,or(a,and(A,B)))";
        String s6 = "A".toLowerCase();
        ScherzoAlgorithms t6 = new ScherzoAlgorithms(1000,1000, new FormulaTree(f6));
        String m6 = t6.minimize();
        System.out.println("Control result: " + s6);
        System.out.println("Minimized formula: " + m6);
        if( s6.equals(m6) ) System.out.println("Formula 6 ..... successful.");
        else System.out.println("Formula 6 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f7 = "ac(s,iff(imp(A,B),imp(A,and(A,B))))";
        String s7 = "c(v)";
        ScherzoAlgorithms t7 = new ScherzoAlgorithms(1000,1000, new FormulaTree(f7));
        String m7 = t7.minimize();
        System.out.println("Control result: " + s7);
        System.out.println("Minimized formula: " + m7);
        if( s7.equals(m7) ) System.out.println("Formula 7 ..... successful.");
        else System.out.println("Formula 7 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        // xor(xor(atom(bit(pSum(1), 3)), atom(bit(line(2), 3))), atom(carry(pSum(1), line(2), 3)))
        String f8 = "ac(s,xor(xor(A,B),C))";
        String s8 = "or(and(A,B,C),and(A,neg(B),neg(C)),and(neg(A),B,neg(C)),and(neg(A),neg(B),C))".toLowerCase();
        ScherzoAlgorithms t8 = new ScherzoAlgorithms(1000,1000, new FormulaTree(f8));
        String m8 = t8.minimize();
        System.out.println("Control result: " + s8);
        System.out.println("Minimized formula: " + m8);
        if( s8.equals(m8) ) System.out.println("Formula 8 ..... successful.");
        else System.out.println("Formula 8 ..... wrong.");
        System.out.println("_________________________________________________________________________");


        String f9 = "ac(s, or(and(A, B), and(C, xor(A, B))))";
        String s9 = "or(and(A,B),and(A,C),and(B,C))".toLowerCase();
        KVMinimizer t9 = new KVMinimizer(new FormulaTree(f9));
        String m9 = t9.karnaughVeitchMinimization();
        System.out.println("Control result: " + s9);
        System.out.println("Minimized formula: " + m9);
        if( s9.equals(m9) ) System.out.println("Formula 9 ..... successful.");
        else System.out.println("Formula 9 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f10 = "ac(s, and(A, B))";
        String s10 = "and(A, B)".toLowerCase();
        KVMinimizer t10 = new KVMinimizer(new FormulaTree(f10));
        String m10 = t10.karnaughVeitchMinimization();
        System.out.println("Control result: " + s10);
        System.out.println("Minimized formula: " + m10);
        if( s10.equals(m10) ) System.out.println("Formula  10..... successful.");
        else System.out.println("Formula 10..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f11 = "ac(s, and(neg(iff(A, xor(xor(B, C), D))), neg(E)))";
        String s11 = "or(and(neg(A),neg(B),neg(C),D,neg(E)),and(neg(A),neg(B),C,neg(D),neg(E)),and(neg(A),B,neg(C),neg(D),neg(E)),and(neg(A),B,C,D,neg(E)),and(A,neg(B),neg(C),neg(D),neg(E)),and(A,neg(B),C,D,neg(E)),and(A,B,neg(C),D,neg(E)),and(A,B,C,neg(D),neg(E)))".toLowerCase();
        KVMinimizer t11 = new KVMinimizer(new FormulaTree(f11));
        String m11 = t11.karnaughVeitchMinimization();
        System.out.println("Control result: " + s11);
        System.out.println("Minimized formula: " + m11);
        if( s11.equals(m11) ) System.out.println("Formula  11..... successful.");
        else System.out.println("Formula 11..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f12 = "ac(s, and(neg(neg(A)), neg(B)))";
        String s12 = "and(A,neg(B))".toLowerCase();
        KVMinimizer t12 = new KVMinimizer(new FormulaTree(f12));
        String m12 = t12.karnaughVeitchMinimization();
        System.out.println("Control result: " + s12);
        System.out.println("Minimized formula: " + m12);
        if( s12.equals(m12) ) System.out.println("Formula  12..... successful.");
        else System.out.println("Formula 12..... wrong.");
        System.out.println("_________________________________________________________________________");
    }

    public static void generateExamples()
    {

    }

}
