import jdd.zdd.ZDD;
import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

import java.io.*;
import java.util.*;

/**
 * Class that allows to use different minimization algorithms on a FormulaTree.
 * IMPORTANT: Only use the espresso methods from here. The other minimizers are found in their own classes.
 */
public class FormulaMinimizer
{
    private static boolean qmChangeIndicator;

    /**
     * This is an alternative implementation of the Quine-McCluskey algorithm. The difference to the other one below is
     * that alterQMC uses more basic data structures and thus should be more space efficient.
     * @param formula
     * @return
     */
    public static String alterQMcC(FormulaTree formula)
    {
        if( formula.getTruthTable().isEmpty() )
            return formula.getTruthTableAsList().get(0).getResult()? "true" : "false";

        if( !formula.getTruthTable().isFound() ) formula.findTruthTable();

        if( formula.getErrorOccured() ) return formula.getRepresentedFormula();

        //contradiction and tautology handling
        if( formula.getTruthTable().isTautology() == 0 ) return "c(v)";
        else if( formula.getTruthTable().isTautology() == -1 ) return "c(f)";

        if( formula.getTruthTable().getTruthTable().size() == 2 ) return formula.getFormulaAtoms().first().getName();

        //find prime implicants
        TruthTable onlyTruesTable = new TruthTable();
        int j = 0;
        for(int i = 0; i < formula.getTruthTableAsList().size(); i++)
            if( formula.getTruthTableAsList().get(i).getResult() )
            {
                onlyTruesTable.addRow(formula.getTruthTableAsList().get(i));
                Vector<Integer> cover = new Vector<Integer>();
                cover.add(j);
                onlyTruesTable.getTruthTable().get(j).setCovers(cover);
                j++;
            }

        j = 0;
        onlyTruesTable.setAtomOrder(formula.getTruthTable().getAtomOrder());
        int minTerms = onlyTruesTable.getTruthTable().size();

        TruthTable primeTable = onlyTruesTable;
        primeTable.setAtomOrder(formula.getTruthTable().getAtomOrder());
        primeTable.append(qmRecursion(onlyTruesTable));

        //primeTable.print();

        primeTable = qmFilterTableForPrimeTerms(primeTable);

        int[][] coveringMatrix
                = new int[primeTable.getTruthTable().size()][minTerms];

        //build covering matrix
        j = 0;
        int k = 0;
        for(TruthTableRow row : primeTable.getTruthTable())
        {
            System.out.println(row.getCovers());
            //for(Integer c : row.getCovers())
            //    coveringMatrix[k][c] = 1;
            for(j = 0; j < coveringMatrix[k].length; j++)
                if( row.getCovers().contains(j) ) coveringMatrix[k][j] = 1;
                else coveringMatrix[k][j] = 0;
            k++;
        }

        //!! arr[row][col] !!//

        LinkedList<Integer> remainingPrimes = new LinkedList<Integer>();
        for(TruthTableRow row : primeTable.getTruthTable())
            remainingPrimes.add(row.getRowNumber());

        System.out.println("remaining primes: " + remainingPrimes);

        //apply dominance relations
        LinkedList<String> atomOrder = primeTable.getAtomOrder();
        LinkedList<Integer> scrappedCols = new LinkedList<Integer>();
        int[][] copy = coveringMatrix;
        do
        {
            copy = coveringMatrix.clone();

            //column dominance
            TreeSet<Integer> domColumns = new TreeSet<Integer>();
            for(int i = 0; i < copy[0].length; i++)    //for each column...
                for(j = 0; j < copy[0].length; j++)   //for every other column...
                {
                    if( i == j || scrappedCols.contains(j) ) continue;
                    //boolean jDomI = true;
                    TreeSet<Integer> jCol = new TreeSet<Integer>();
                    TreeSet<Integer> iCol = new TreeSet<Integer>();
                    for(k = 0; k < copy.length; k++)
                    {
                        if( coveringMatrix[k][j] == 1 ) jCol.add(k);
                        if( coveringMatrix[k][i] == 1 ) iCol.add(k);
                    }

                    if( iCol.containsAll(jCol) )
                    {
                        //save I
                        domColumns.add(i);
                        //System.out.print("dom col found!");
                        scrappedCols.add(i);
                        break;
                    }
                }


            //scrap dominated columns
            int[][] newMatrix = new int[coveringMatrix.length][coveringMatrix[0].length-domColumns.size()];
            k = 0;
            for(int i = 0; i < coveringMatrix[0].length; i++)
                if( !domColumns.contains(i) )
                {
                    for(j = 0; j < newMatrix.length; j++)
                        newMatrix[j][k]
                                = coveringMatrix[j][i];
                    k++;
                }
                //else atomOrder.remove(i);

            coveringMatrix = new int[newMatrix[0].length][newMatrix.length];
            coveringMatrix = newMatrix.clone();

            /*System.out.println("new coveringMatrix: ");
            for(int i=0; i<coveringMatrix.length; i++)
            {
                for( j=0; j<coveringMatrix[0].length;j++)
                    System.out.print(coveringMatrix[i][j] + " ");
                System.out.println();
            }*/

            //row dominance
            LinkedList<Integer> domRows = new LinkedList<Integer>();
            for(int i = 0; i < coveringMatrix.length; i++)
            {
                TreeSet<Integer> covers = new TreeSet<Integer>();
                for(j = 0; j < coveringMatrix[0].length; j++)
                    if( coveringMatrix[i][j] == 1 ) covers.add(j);

                TreeSet<Integer> coversCopy = new TreeSet<Integer>();
                for(Integer one : coversCopy)
                    for(j = 0; j < coveringMatrix.length; j++)
                        if( coveringMatrix[j][one] == 1 && j != i ) coversCopy.add(one);
                if( coversCopy.size() == covers.size() )
                    domRows.add(i);
            }

            //scrap dominated rows
            newMatrix = new int[coveringMatrix.length-domRows.size()][coveringMatrix[0].length];
            k = 0;
            for(int i = 0; i < coveringMatrix.length; i++)
                if( !domRows.contains(i) )
                {
                    for(j = 0; j < coveringMatrix[0].length; j++)
                        newMatrix[k][j] = coveringMatrix[i][j];
                    k++;
                }
                else remainingPrimes.remove(i);

            //for(int i : domRows)
            //    remainingPrimes.remove(new Integer(i));

            coveringMatrix = new int[newMatrix[0].length][newMatrix.length];
            coveringMatrix = newMatrix.clone();

            domRows.sort(new Comparator<Integer>() {
                @Override
                public int compare(Integer o1, Integer o2) {
                    if (o1 > o2) return -1;
                    else if (o1 == o2) return 0;
                    else return 1;
                }
            });
            //for(int i : domRows)
                //primeTable.getTruthTable().remove(i);
                //atomOrder.remove(i);

            /*System.out.println("covering matrix: ");
            for(int i=0; i<coveringMatrix.length; i++)
            {
                for( j=0; j<coveringMatrix[0].length;j++)
                    System.out.print(coveringMatrix[i][j] + " ");
                System.out.println();
            }*/

            coveringMatrix = new int[newMatrix[0].length][newMatrix.length];
            coveringMatrix = newMatrix.clone();
        } while (!Arrays.deepEquals(copy, coveringMatrix));

        /*System.out.println("covering matrix: ");
        for(int i=0; i<coveringMatrix.length; i++)
        {
            for( j=0; j<coveringMatrix[0].length;j++)
                System.out.print(coveringMatrix[i][j] + " ");
            System.out.println();
        }

        System.out.println("remaining primes: " + remainingPrimes);*/

        //solve resulting covering matrix
        TruthTable table = new TruthTable();

        for(Integer i : remainingPrimes)
        {
            table.addRow(primeTable.getRowWithNr(i));
        }
        table.setAtomOrder(formula.getTruthTable().getAtomOrder());

        System.out.println(table.getAtomOrder());

        //System.out.println("Prime table:");
        //primeTable.print();

        //for each row in the cov. matrix: find matching prime term
        /*int i = 0;
        for(i = 0; i < coveringMatrix[0].length; i++)
        {

        }*/

        /*for(i = 0; i < coveringMatrix.length; i++)
        {
            LinkedList<String> rowAssignment = new LinkedList<String>();
            for(j = 0; j < coveringMatrix[i].length; j++)
                if( coveringMatrix[i][j] == 1 ) rowAssignment.add("true");
                else if( coveringMatrix[i][j] == 0 ) rowAssignment.add("false");
            table.addRow(new TruthTableRow(rowAssignment, "true"));
        }*/

        String minimizedFormula = "";
        if( table.getTruthTable().size() >= 2 ) minimizedFormula = "or(";

        int i = 0;
        j = 0;
        for(TruthTableRow row : table.getTruthTable())
        {
            minimizedFormula += "and(";
            i = 0;
            k = 0;
            for(String s : row.getAssignment())
            {
                if( s.equals("true") )
                {
                    if( i != 0 ) minimizedFormula += ",";
                    minimizedFormula += table.getAtomOrder().get(k);
                    i++;
                }

                else if( s.equals("false") )
                {
                    if( i != 0 ) minimizedFormula += ",";
                    minimizedFormula += "neg(" + table.getAtomOrder().get(k) + ")";
                    i++;
                }
                k++;
            }

            minimizedFormula += ")";
            if( j != table.getTruthTable().size()-1 )  minimizedFormula += ",";
            j++;
        }
        if( table.getTruthTable().size() >= 2 ) minimizedFormula += ")";

        System.out.println(minimizedFormula);

        return minimizedFormula;
    }

    /**
     * Minimizes the given formula using the Quine-McCluskey algorithm
     * @return Returns a String representing the minimized formula using the DIAMOND format
     */
    @Deprecated
    public static String quineMccluskeyMinimization(FormulaTree formula)
    {
        if( formula.getTruthTable().isEmpty() )
            return formula.getTruthTableAsList().get(0).getResult()? "true" : "false";

        if( !formula.getTruthTable().isFound() ) formula.findTruthTable();

        if( formula.getErrorOccured() ) return formula.getRepresentedFormula();

        //tautology and contradiction handling
        if( formula.getTruthTable().isTautology() == 0 ) return "c(v)";
        else if( formula.getTruthTable().isTautology() == -1 ) return "c(f)";

        if( formula.getTruthTable().getTruthTable().size() == 2 ) return formula.getFormulaAtoms().first().getName();

        // keeps numbers of minterms which need to be covered later
        TreeSet<Integer> minterms = new TreeSet<Integer>();

        //only keep assignments evaluating to true
        TruthTable onlyTruesTable = new TruthTable();
        int j = 0;
        for(int i = 0; i < formula.getTruthTableAsList().size(); i++)
        {
            if( formula.getTruthTableAsList().get(i).getResult() )
            {
                onlyTruesTable.addRow(formula.getTruthTableAsList().get(i));
                Vector<Integer> cover = new Vector<Integer>();
                cover.add(j);
                onlyTruesTable.getTruthTable().get(j).setCovers(cover);
                minterms.add(formula.getTruthTableAsList().get(i).getRowNumber());
                j++;
            }
        }
        j = 0;
        onlyTruesTable.setAtomOrder(formula.getTruthTable().getAtomOrder());

        //handling for special cases, when formula has 0 or only 1 minterm
        if( onlyTruesTable.getTruthTable().size() == 0 ) return "c(f)";
        else if ( onlyTruesTable.getTruthTable().size() == 1 )
        {
            String finishedFormula = "and(";

            int i = 0;
            for(String s : onlyTruesTable.getTruthTable().get(0).getAssignment())
            {
                if( s.equals("true") ) finishedFormula += onlyTruesTable.getAtomOrder().get(i);
                else if( s.equals("false") ) finishedFormula += "neg(" + onlyTruesTable.getAtomOrder().get(i) + ")";
                if( i < onlyTruesTable.getTruthTable().get(0).getAssignment().size()-1 ) finishedFormula += ",";
                i++;
            }

            finishedFormula += ")";
            return finishedFormula;
        }

        //builds First Quinean Table.
        TruthTable firstTable = onlyTruesTable;

        firstTable.setAtomOrder(formula.getTruthTable().getAtomOrder());
        firstTable.append(qmRecursion(onlyTruesTable));

        //build Second Quinean Table
        //TruthTable secondTable = qmChoosePrimeTerms(firstTable);
        TruthTable secondTable = qmFilterTableForPrimeTerms(firstTable);
        secondTable.setAtomOrder(firstTable.getAtomOrder());
        secondTable.setIsFound();

        //find core prime implicants and check dominance relations
        do
        {
            qmChangeIndicator = true;
            secondTable = qmCheckForRowDominance(secondTable);
            secondTable = qmCheckForColumnDominance(secondTable);
        } while( qmChangeIndicator );

        secondTable = qmChoosePrimeTerms(secondTable);
        secondTable.setAtomOrder(firstTable.getAtomOrder());

        String minimizedFormula = "";
        if( secondTable.getTruthTable().size() >= 2 ) minimizedFormula = "or(";

        j = 0;
        for(TruthTableRow row : secondTable.getTruthTable())
        {
            minimizedFormula += "and(";
            int i = 0;
            int k = 0;
            for(String s : row.getAssignment())
            {
                if( s.equals("true") )
                {
                    if( i != 0 ) minimizedFormula += ",";
                    minimizedFormula += secondTable.getAtomOrder().get(k);
                    i++;
                }

                else if( s.equals("false") )
                {
                    if( i != 0 ) minimizedFormula += ",";
                    minimizedFormula += "neg(" + secondTable.getAtomOrder().get(k) + ")";
                    i++;
                }
                k++;
            }

            minimizedFormula += ")";
            if( j != secondTable.getTruthTable().size()-1 )  minimizedFormula += ",";
            j++;
        }
        if( secondTable.getTruthTable().size() >= 2 ) minimizedFormula += ")";

        return minimizedFormula;
    }

    @Deprecated
    private static TruthTable qmFilterTableForPrimeTerms(TruthTable firstTable)
    {
        TruthTable primeTermTable = new TruthTable();

        for(TruthTableRow row : firstTable.getTruthTable())
        {
            if( row.getIsPrimeTerm() )
                primeTermTable.addRow(row);
        }

        return primeTermTable;
    }

    /**
     * Check whether some columns are dominated by others and scraps them.
     * @param secondTable
     * @return
     */
    @Deprecated
    private static TruthTable qmCheckForColumnDominance(TruthTable secondTable) {
        if( secondTable.getTruthTable().size() == 1 ) {
            qmChangeIndicator = false;
            //System.out.println("Table to small to minimize further");
            return secondTable;
        }
        else
        {
            TruthTable reducedTable = new TruthTable();
            TransformedPrimeTable transformedPrimeTable = new TransformedPrimeTable(secondTable);
            TreeSet<Integer> scrappableMinTerms = new TreeSet<Integer>();

            for (TransformedPrimeTableRow row1 : transformedPrimeTable.getMinTerms())
                for (TransformedPrimeTableRow row2 : transformedPrimeTable.getMinTerms())
                    if ( //todo: possibly some wrong conditions in here
                            !row2.equals(row1)
                                    && !scrappableMinTerms.contains(row2.getPrimeTermNumber())      //possibly working without this as well
                                    && row2.getCoveredBy().containsAll(row1.getCoveredBy())
                                    && row2.getCoveredBy().size() > row1.getCoveredBy().size()
                            )
                        scrappableMinTerms.add(row2.getPrimeTermNumber());

            reducedTable.setIsFound();
            reducedTable.setAtomOrder(secondTable.getAtomOrder());

            for (TruthTableRow row : secondTable.getTruthTable())
            {
                Vector<Integer> cover = new Vector<Integer>();

                for (Integer i : row.getCovers())
                    if (!scrappableMinTerms.contains(i))
                        cover.add(i);

                if (cover.size() != 0)      //kick out rows that don't cover anything anymore
                    reducedTable.addRow(row);
            }

            if (reducedTable.getTruthTable().size() == secondTable.getTruthTable().size())
                qmChangeIndicator = false;

            return reducedTable;
        }
    }

    /**
     * Checks whether some rows are dominated by other rows and scraps the dominated ones.
     * @param secondTable the second Quinean Table
     * @return TruthTable containing core prime implicants only
     */
    @Deprecated
    private static TruthTable qmCheckForRowDominance(TruthTable secondTable)
    {
        if( secondTable.getTruthTable().size() == 1 )
        {
            qmChangeIndicator = false;
            //System.out.println("Table to small to minimize further");
            return secondTable;
        }

        //TruthTable tableCopy = new TruthTable();

        /*int i = 0;
        for(TruthTableRow row : secondTable.getTruthTable())        //create a new table with new row numbers from 0 straight to n
        {
            TruthTableRow newRow = row;
            newRow.setRowNumber(i);
            tableCopy.addRow(newRow);
            i++;
        }*/

        TreeSet<Integer> scrappableRows = new TreeSet<Integer>();     //contains numbers of rows that can be scrapped
        for(TruthTableRow row1 : secondTable.getTruthTable())
        {
            TreeSet<Integer> coveredWithoutRow1 = new TreeSet<Integer>();
            for(TruthTableRow row2 : secondTable.getTruthTable())
                if( !row2.equals(row1) )
                    coveredWithoutRow1.addAll(row2.getCovers());

            if( coveredWithoutRow1.containsAll(row1.getCovers()) )   //row1 can be scrapped
                scrappableRows.add(row1.getRowNumber());
        }

        //scrap rows
        TruthTable reducedTable = new TruthTable();
        reducedTable.setAtomOrder(secondTable.getAtomOrder());
        reducedTable.setIsFound();
        for(TruthTableRow row : secondTable.getTruthTable())
        {
            if( !scrappableRows.contains(row.getRowNumber()) )
                reducedTable.addRow(row);
        }

        if(reducedTable.getTruthTable().size() == secondTable.getTruthTable().size())
            qmChangeIndicator = false;

        return reducedTable;
    }

    /**
     * Finds the minimal set of prime terms for QM-algorithm using a generate-and-test-algorithm. Problem is NP-complete.
     * //todo dont just test all combinations
     * @param firstTable First Quinean Table
     * @return Second Quinean Table
     */
    @Deprecated
    private static TruthTable qmChoosePrimeTerms(TruthTable firstTable)
    {
        TruthTable secondTable = new TruthTable();

        ICombinatoricsVector<TruthTableRow> vector = Factory.createVector(firstTable.getTruthTable());
        Generator<TruthTableRow> generator = Factory.createSimpleCombinationGenerator(vector, firstTable.getTruthTable().size());

        int minterms[] = new int[firstTable.getTruthTable().size()];
        for(int i = 0; i < firstTable.getTruthTable().size(); i++)
            minterms[i] = firstTable.getTruthTable().get(i).getRowNumber();

        boolean firstFound = false;
        ICombinatoricsVector<TruthTableRow> smallestFoundCombination = null;
        int i = 0;
        for(ICombinatoricsVector<TruthTableRow> combination : generator)
        {
            //if( smallestFoundCombination == null ) smallestFoundCombination = combination;
            //check whether all minterms are covered
            boolean invalidCombination = false;
            Vector<Integer> completeCover = new Vector<Integer>();

            //collect all covered minterms first
            for(TruthTableRow row : combination)
            {
                completeCover.add(row.getRowNumber());
                completeCover.addAll(row.getCovers());
            }
            //check if all minterms are in the cover
            for(int j = 0; j < minterms.length; j++)
                if( !completeCover.contains(minterms[j]) ) invalidCombination = true;


            if( !invalidCombination )
            {
                //smaller than the already found minimal set?
                if( !firstFound )
                {
                    smallestFoundCombination = combination;
                    firstFound = true;
                }
                else
                    if( combination.getSize() < smallestFoundCombination.getSize() )
                        smallestFoundCombination = combination;
            }
            i++;
        }

        //if( smallestFoundCombination != null) System.out.println(smallestFoundCombination.getSize());
        //todo: smallestFoundCombination throwing NPE?

        if( smallestFoundCombination != null )
        {
            for(TruthTableRow row : smallestFoundCombination)
                secondTable.addRow(row);
            return secondTable;
        }
        else return firstTable;
}

    /**
     * possible to-do: reduce number of loops by classifying rows before the loops (classification by number of "true"s)
     * and only compare neighboured classes
     * @param lowOrderTable
     * @return
     */
    @Deprecated
    private static TruthTable qmRecursion(TruthTable lowOrderTable)
    {
        TruthTable highOrderTable = new TruthTable();

        for(int i = 0; i < lowOrderTable.getTruthTable().size(); i++)
            for(int j = 0; j < lowOrderTable.getTruthTable().size(); j++)
            {
                //lowOrderTable.getTruthTable().get(i).getAssignment().get(0).equals(lowOrderTable.getTruthTable().get(j).getAssignment().get(0));

                if( lowOrderTable.compareRows(i, j) == 1 )
                {
                    LinkedList<String> assignment = new LinkedList<String>();

                    for(int k = 0; k < lowOrderTable.getTruthTable().getFirst().getAssignment().size(); k++)
                    {
                        if( k != lowOrderTable.getDifferentPosition(i,j) )
                            assignment.add(lowOrderTable.getTruthTable().get(i).getAssignment().get(k));
                        else
                            assignment.add("d.c.");     //don't care
                    }

                    Vector<Integer> cover = new Vector<Integer>();

                    if( lowOrderTable.getTruthTable().get(i).getCovers() != null )
                        cover.addAll(lowOrderTable.getTruthTable().get(i).getCovers());
                    else
                        cover.add(lowOrderTable.getTruthTable().get(i).getRowNumber());
                    if( lowOrderTable.getTruthTable().get(j).getCovers() != null )
                        cover.addAll(lowOrderTable.getTruthTable().get(j).getCovers());
                    else
                        cover.add(lowOrderTable.getTruthTable().get(j).getRowNumber());

                    cover.sort(new Comparator<Integer>() {
                        @Override
                        public int compare(Integer o1, Integer o2) {
                            return o1.intValue() - o2.intValue();
                        }
                    });

                    lowOrderTable.getTruthTable().get(i).setIsPrimeTerm(false);
                    lowOrderTable.getTruthTable().get(j).setIsPrimeTerm(false);

                    int number;
                    if( highOrderTable.getTruthTable().size() == 0 )
                        number = lowOrderTable.getTruthTable().getLast().getRowNumber() + 1;
                    else number = Math.max(lowOrderTable.getTruthTable().getLast().getRowNumber() + 1,
                            highOrderTable.getTruthTable().getLast().getRowNumber() + 1);

                    TruthTableRow newRow = new TruthTableRow(assignment, "true", number, cover);
                    //s++;
                    highOrderTable.addRow(newRow);          // ????
                    //lowOrderTable.addRow(newRow);         // ????
                }
            }

        lowOrderTable.append(highOrderTable);
        //if( highOrderTable.equals(lowOrderTable) ) return lowOrderTable;    //no changes, end
        if( highOrderTable.getTruthTable().size() == 0) return lowOrderTable;
        else return qmRecursion(highOrderTable);                            //tables changed, do recursion
    }

    /**
     * Minimizes the given formula using the Karnaugh-Veitch algorithm
     * @return A String object representing the minimized formula
     */
    @Deprecated
    public static String karnaughVeitchMinimization(FormulaTree formula)
    {
        if( formula.getTruthTable().isEmpty() )
            return formula.getTruthTableAsList().get(0).getResult()? "true" : "false";

        if( !formula.getTruthTable().isFound() ) formula.findTruthTable();

        if( formula.getErrorOccured() ) return formula.getRepresentedFormula();

        //tautology and contradiction handling
        if( formula.getTruthTable().isTautology() == 0 ) return "c(v)";
        else if( formula.getTruthTable().isTautology() == -1 ) return "c(f)";

        if( formula.getTruthTable().getTruthTable().size() == 2 ) return formula.getFormulaAtoms().first().getName();

        kvHyperCube hyperCube = new kvHyperCube(formula.getTruthTable());

        //A set, where each List represents one block and each Vector represents a node in this block
        //todo: nicer solution? possibly a tool class as wrapper?
        //todo: possibly causing performance issues
        LinkedList<List<kvHyperCubeNode>> validBlocks = new LinkedList<List<kvHyperCubeNode>>();

        //generate blocks and test them
        TreeSet<Integer> validSizes = new TreeSet<Integer>();
        for (int i = formula.getTruthTable().getAtomOrder().size(); i >= 0; i--)
        {
            int pow = 1;

            for(int j = 0; j < i; j++) pow *= 2;

            validSizes.add(pow);
            //System.out.println("Size added: " + pow);
        }

        ICombinatoricsVector<kvHyperCubeNode> vector = Factory.createVector(hyperCube.getHyperCube());
        for(Integer i : validSizes)
        {
            Generator<kvHyperCubeNode> generator = Factory.createSimpleCombinationGenerator(vector, i);

            for(ICombinatoricsVector<kvHyperCubeNode> block : generator)
                if( hyperCube.validateBlock(block.getVector()) && !validBlocks.contains(block.getVector()))
                {
                    validBlocks.add(block.getVector());

                    System.out.println("New block:");
                    int l = 0;
                    for (kvHyperCubeNode node : block) {
                        System.out.print(l + " |");
                        for (String s : node.getAssignment()) System.out.print(" " + s);
                        l++;
                        System.out.println();
                    }
                }
        }

        //find minimal set of blocks so that all minterms are covered
        LinkedList<LinkedList<String>> minTerms = new LinkedList<LinkedList<String>>();
        for(TruthTableRow row : formula.getTruthTable().getTruthTable())
            if( row.getResult() ) minTerms.add(row.getAssignment());

        /*
        LinkedList<Vector<String>> coveredMinTerms = new LinkedList<Vector<String>>();
        int i = validBlocks.size()-1;
        while( !coveredMinTerms.containsAll(minTerms) && i >= 0 )
        {
            LinkedList<LinkedList<String>> newMinTerms = new LinkedList<LinkedList<String>>();

            //int k = 0;
            for(kvHyperCubeNode node : validBlocks.get(i))
            {
                newMinTerms.add(new LinkedList<String>());
                for(String s : node.getAssignment()) newMinTerms.getLast().add(s);
                //k++;
            }

            if( !coveredMinTerms.contains(newMinTerms) ) minimalBlockSet.add(validBlocks.get(i));


            System.out.println("Added block #" + i);
            for(kvHyperCubeNode minTermNode : validBlocks.get(i))
            {
                coveredMinTerms.add(minTermNode.getAssignment());
            }
            i--;
        }
        */
        List<List<kvHyperCubeNode>> minimalBlockSet = new LinkedList<List<kvHyperCubeNode>>();

        if( validBlocks.size() == 0 ) return "c(f)";
        else if( validBlocks.size() == 1 )
            minimalBlockSet.addAll(validBlocks);
        else
        {
            //generate and test
            ICombinatoricsVector<List<kvHyperCubeNode>> blockVector = Factory.createVector(validBlocks);
            //Factory.createVector(validBlocks);
            for (int i = 0; i < validBlocks.size(); i++) //i=0 was actually i=1??
            {
                Generator<List<kvHyperCubeNode>> generator = Factory.createSimpleCombinationGenerator(blockVector, i);

                for (ICombinatoricsVector<List<kvHyperCubeNode>> blocks : generator)
                {
                    LinkedList<LinkedList<String>> coveredMinTerms = new LinkedList<LinkedList<String>>();

                    for (List<kvHyperCubeNode> block : blocks)
                        for (kvHyperCubeNode node : block)
                        {
                            coveredMinTerms.add(new LinkedList<String>());
                            for (String s : node.getAssignment()) coveredMinTerms.getLast().add(s);
                        }

                    if (coveredMinTerms.containsAll(minTerms) &&
                            (blocks.getSize() < minimalBlockSet.size() || minimalBlockSet.size() == 0))
                        minimalBlockSet = blocks.getVector();
                }
            }
        }
        //build a String representing the found minimal set
        String minimizedFormula = "";
        if( minimalBlockSet.size() >= 2 ) minimizedFormula += "or(";
        //else if( minimalBlockSet.size() == 0 ) minimizedFormula += "c(f)";

        int i = 0;
        int j = 0;
        for(List<kvHyperCubeNode> block : minimalBlockSet) //for each block
        {
            //find shifting variables to leave them out later
            Vector<Integer> changingVars = new Vector<Integer>();
            for(kvHyperCubeNode node1 : block)
                for(kvHyperCubeNode node2 : block)
                {
                    int k = 0;
                    for (String s : node1.getAssignment())
                    {
                        if (!s.equals(node2.getAssignment().get(k)) && !changingVars.contains(k)) {
                            changingVars.add(k);}
                        k++;
                    }
                }

            i = 0;
            if( block.get(0).getAssignment().size() >= 2 ) minimizedFormula += "and(";
            int k = 0;
            for(String s : block.get(0).getAssignment())
            {
                if( s.equals("true") && !changingVars.contains(k) )
                {
                    if( i != 0 ) minimizedFormula += ",";
                    minimizedFormula += formula.getTruthTable().getAtomOrder().get(k);
                    i++;
                }
                else if( s.equals("false") && !changingVars.contains(k) )
                {
                    if (i != 0) minimizedFormula += ",";
                    minimizedFormula += "neg(" + formula.getTruthTable().getAtomOrder().get(k) + ")";
                    i++;
                }
                k++;
            }
            if( block.get(0).getAssignment().size() >= 2 ) minimizedFormula += ")";
            if( j < minimalBlockSet.size()-1 )  minimizedFormula += ",";
            j++;
        }
        if( minimalBlockSet.size() >= 2 ) minimizedFormula += ")";

        return minimizedFormula;
    }

    /*public static String scherzoMinimization(FormulaTree formula)
    {
        if( formula.getTruthTable().isEmpty() )
            return formula.getTruthTableAsList().get(0).getResult()? "true" : "false";

        if( !formula.getTruthTable().isFound() ) formula.findTruthTable();

        if( formula.getErrorOccured() ) return formula.getRepresentedFormula();

        if( formula.getTruthTable().isTautology() == 0 ) return "c(v)";
        else if( formula.getTruthTable().isTautology() == -1 ) return "c(f)";


        //find the set of all prime implicants of the given formula
        ZDD q = new ZDD(10000);
        for(String s : formula.getTruthTable().getAtomOrder()) q.createVar();

        //find the CSs Q and P

        //compute cyclic core of the SCP (Q,P,subset)

        //solve the covering matrix

        //transform solution into output String
        String minimizedFormula = "";

        return minimizedFormula;
    }*/

    public static String espressoMinimization(FormulaTree formula)
    {
        if( !formula.getTruthTable().isFound() ) formula.findTruthTable();

        if( formula.getErrorOccured() ) return formula.getRepresentedFormula();

        if( formula.getTruthTable().isTautology() == 0 ) return "c(v)";
        else if( formula.getTruthTable().isTautology() == -1 ) return "c(f)";

        String result = "";
        try
        {
            //transform formula into espresso's format and write it to a file
            File f = File.createTempFile("tmp", "tmp");
            f.deleteOnExit();
            BufferedWriter writer = new BufferedWriter(new FileWriter(f));
            String s = espConverter(formula);
            writer.write(espConverter(formula));
            writer.close();

            //run espresso
            Process p = Runtime.getRuntime().exec("C:\\Users\\Kevin\\Studium\\Bachelorarbeit\\espresso\\espresso.exe -Dexact " + f.getAbsolutePath());

            //get result
            try { p.waitFor(); }
            catch (InterruptedException e)
            {
                System.err.println("Espresso was interrupted.");
                return formula.getRepresentedFormula();
            }
            String str = "";
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while((str = reader.readLine()) != null)
                result += str + "\n";

            BufferedReader errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            while(errorReader.readLine() != null) System.out.println(errorReader.readLine());
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.err.println("IOException during espresso minimization.");
            return formula.getRepresentedFormula();
        }

        //build output formula
        String minimizedFormula = "";
        String p = "";
        for(String s : result.split("\\n"))
            if(s.startsWith(".p "))
            {
                p = s.substring(3);
                if( Integer.parseInt(p) == 1 ) ;
                break;
            }

        if(Integer.parseInt(p) == 1)
        {
            /* ... */
            for(String s : result.split("\\n"))
            {
                if( !s.startsWith(".") )
                {
                    int dcs = 0;
                    int vars = 0;

                    for(char c : s.toCharArray())
                    {
                        if(c == '-') dcs++;
                        else if( c == '0' || c == '1') vars++;
                        else if( c == ' ' ) break;
                    }

                    int i = 0;
                    minimizedFormula += "and(";

                    for(char c : s.toCharArray())
                    {
                        if( c == ' ' ) break;

                        if( c == '0' )
                        {
                            minimizedFormula += "neg(" + formula.getTruthTable().getAtomOrder().get(i) + ")";
                            i++;
                        }
                        else if( c == '1' )
                        {
                            minimizedFormula += formula.getTruthTable().getAtomOrder().get(i);
                            i++;
                        }
                        else if( c == '-' )
                        {
                            //do nothing
                        }
                        if( i < vars
                                && minimizedFormula.toCharArray()[minimizedFormula.length()-1] != '(' )
                            minimizedFormula += ",";
                    }

                    minimizedFormula += ")";
                    return minimizedFormula;
                }
            }
        }
        else if(Integer.parseInt(p) == 0)
            return "c(f)";

        minimizedFormula += "or(";
        int j = 0;
        for(String s : result.split("\\n"))
        {
            if( !s.startsWith(".") )
            {
                int dcs = 0;
                int vars = 0;

                for(char c : s.toCharArray())
                {
                    if(c == '-') dcs++;
                    else if( c == '0' || c == '1') vars++;
                    else if( c == ' ' ) break;
                }

                int i = 0;
                minimizedFormula += "and(";
                for(char c : s.toCharArray())
                {
                    if( c == ' ' ) break;

                    if( c == '0' )
                    {
                        minimizedFormula += "neg(" + formula.getTruthTable().getAtomOrder().get(i) + ")";
                        i++;
                    }
                    else if( c == '1' )
                    {
                        minimizedFormula += formula.getTruthTable().getAtomOrder().get(i);
                        i++;
                    }
                    else if( c == '-' )
                    {
                        //do nothing
                    }
                    if( i < vars
                            && minimizedFormula.toCharArray()[minimizedFormula.length()-1] != '(' )
                        minimizedFormula += ",";
                    //i++;
                }
                minimizedFormula += ")";
                if( j < (Integer.parseInt(p)-1) ) minimizedFormula += ",";
                j++;
            }
        }
        minimizedFormula += ")";

        return minimizedFormula;

    }

    private static String espConverter(FormulaTree formula)
    {
        String espressoFormula = "";

        espressoFormula += ".i " + formula.getFormulaAtoms().size() + "\n";
        espressoFormula += ".o 1" + "\n";
        //specify options
        /* ... */

        espressoFormula += ".lib ";
        int i = 0; int j = formula.getFormulaAtoms().size();
        for(PropAtom a : formula.getFormulaAtoms())
        {
            if( i != j-1 ) espressoFormula += a.getName() + " ";
            else espressoFormula += a.getName();
        }
        espressoFormula += "\n";

        for(TruthTableRow row : formula.getTruthTableAsList())
        {
            for(String s : row.getAssignment())
            {
                if( s.equals("true") ) espressoFormula += "1";
                else if( s.equals("false") ) espressoFormula += "0";
                else if( s.equals("d.c.") ) espressoFormula += "-";
            }
            if( row.getResult() ) espressoFormula += " 1\n";
            else espressoFormula += " 0\n";
        }

        espressoFormula += ".e\n";

        return espressoFormula;
    }
}
