import java.io.*;
import java.nio.file.Path;
import java.util.Scanner;
import java.util.Vector;

public class FormulaReader
{
    /*private String processedFormula;
    private String rawFormula;

    public void setRawFormula(String formula)  { rawFormula = formula; }
    public String getRawFormula() { return rawFormula; }
    public void setProcessedFormula(String formula) { processedFormula = formula; }
    public String getProcessedFormula() { return processedFormula; }
    */
    /*public  void processFormula()
    {
        if( rawFormula.length() > 0 )
        {
            String formula = rawFormula;
            formula = formula.toLowerCase();

            formula = formula.replaceAll("\\s", "");
            //cuts "ac(s," and closing bracket
            processedFormula = formula.substring(formula.indexOf(',')+1, formula.length()-1);
        }
    }*/

    /**
     * Trims formula and returns it without "ac(s," and the fitting closing bracket
     * @param formula
     * @return
     */
    public static String processFormula(String formula)
    {
        if( formula.length() > 0 )
        {
            String f = "";
            if( formula.endsWith(".") ) f = formula.substring(0, formula.length()-1);
            else f = formula.toLowerCase();

            f = f.toLowerCase();

            f = f.replaceAll("\\s+", "");
            //cuts "ac(s," and closing bracket
            System.out.println("f="+f);
            f = f.substring(f.indexOf(',')+1, f.length()-1);
            System.out.println("trimmed formula: " + f);

            if( f.equals("c(f)") ) return "c(f)";
            else if( f.equals("c(v)") ) return "c(v)";
            else return f;
        }
        else return "INVALID";
    }

    /**
     * @return first operator, if processedFormula exists. Else returns "INVALID"
     */
    /*public String getFirstOperator()
    {
        if( processedFormula != null && processedFormula.length() > 0 )
        {
            String firstOp = processedFormula.substring(0, processedFormula.indexOf('('));
            return firstOp;
        }
        else return "INVALID";
    }*/

    /**
     * Finds the first operator used in the formula
     * @return first operator found in formula
     */
    public static String getFirstOperator(String formula)
    {
        if( formula != null && formula.length() > 0 && formula.indexOf('(') != -1 )
        {
            String firstOp = formula.substring(0, formula.indexOf('('));
            return firstOp;
        }
        else return "INVALID";
    }

    /**
     * Reads rawFormula from a console input
     */
    /*public static void readFormulaFromConsole()
    {
        Scanner scanner = new Scanner(System.in);
        rawFormula = scanner.nextLine();
    }*/

    /**
     * Reads the file found at the given path and returns all formulae found in this file
     * @param path
     * @return
     */
    /*public Vector<String> readADFFromFile(String path)
    {
        return readADFFromFile(new File(path));
    }*/

    /**
     * Returns the formulae found within the file containing the ADF as Vector
     * @return a Vector of processed formulae representing the imported ADF
     */
    public static Vector<String> readADF(File file)
    {
        Vector<String> formulae = new Vector<String>();

        BufferedReader reader;
        try { reader = new BufferedReader(new FileReader(file)); }
        catch (FileNotFoundException e)
        {
            //todo error handling
            e.printStackTrace();
            return null;
        }

        try
        {
            String s = reader.readLine();
            while( s != null)
            {
                if( s.startsWith("ac(") ) formulae.add(s);
                s = reader.readLine();
            }
        }
        catch(IOException e)
        {
            //todo error handling
            e.printStackTrace();
            return null;
        }

        Vector<String> processedFormulae = new Vector<String>();

        for(String s : formulae)
        {
            processedFormulae.add( processFormula(s) );
        }

        return processedFormulae;
    }

    public static Vector<FormulaTree> readADFFromFile(File file)
    {
        Vector<FormulaTree> formulas = new Vector<FormulaTree>();

        for(String s : readADF(file))
        {
            formulas.add(new FormulaTree(s));
        }

        return formulas;
    }
}
