import sun.awt.image.ImageWatched;

import java.util.LinkedList;
import java.util.Vector;

public class TruthTable
{
    private LinkedList<String> atomOrder;
    private LinkedList<TruthTableRow> truthTable;
    private boolean isFound;
    private final boolean isEmpty;

    /**
     * Adds a new row to the existing table while avoiding duplication
     * @param row
     */
    public void addRow(TruthTableRow row)
    {
        if( row.getAssignment() == null ) truthTable.add(row);
        else
        {
            boolean insert = true;
            for (TruthTableRow existingRow : truthTable)
                if (existingRow.getAssignment().equals(row.getAssignment())
                        && existingRow.getCovers().equals(row.getCovers()))
                    insert = false;
            if (insert) truthTable.add(row);
        }
    }

    public LinkedList<TruthTableRow> getTruthTable() { return truthTable; }

    @Override
    public String toString()
    {
        String table = "";

        for(TruthTableRow row : truthTable)
        {
            for(String s : row.getAssignment())
                if( s.equals("true") ) table += "1";
                else table += "0";
            table += " ";
        }

        return table = table.trim();
    }

    /**
     * Returns a String of cubes representing the whole truth table
     * to be used with ZDD.cubes_union()
     * Syntax: "a+a-b+b- a+a-b+b-..." for two variables a and b
     * new: "d+d-c+c-...a+a-
     */
    public String toZDDString()
    {
        String zddString = "";

        int i = 0;
        for(TruthTableRow row : truthTable)
        {
            //for(/*String s : row.getAssignment()*/ int j = row.getAssignment().size()-1; j>=0; j--)
            for(String s : row.getAssignment())
            {
                //if( row.getAssignment().get(j).equals("true") ) zddString += "10";
                if( s.equals("true") ) zddString += "01";   //10
                else zddString += "10";                     //01
            }
            if( i < truthTable.size() ) zddString += " ";
            i++;
        }

        return zddString;
    }

    public void setAtomOrder(LinkedList<String> order) { atomOrder = order; }

    public LinkedList<String> getAtomOrder() { return atomOrder; }

    public void addRow(boolean[] assignment, boolean result)
    {
        if( assignment == null ) truthTable.add(new TruthTableRow(null, Boolean.toString(result)));

        else if( assignment.length == truthTable.getFirst().getAssignment().size() )
        {
            LinkedList<String> newRow = new LinkedList<String>();
            String newRowResult = Boolean.toString(result);

            for(int i = 0; i < assignment.length; i++)
                newRow.add( Boolean.toString(assignment[i]) );

            truthTable.add(new TruthTableRow(newRow, newRowResult, truthTable.size()));
        }
    }

    public TruthTableRow getRowWithNr(int nr)
    {
        for(TruthTableRow row : truthTable)
            if( row.getRowNumber() == nr ) return row;
        return null;
    }

    public void print()
    {
        //System.out.println("TruthTable: " + this);
        //System.out.println("Empty? " + Boolean.toString(truthTable.isEmpty()));
        //if( truthTable.isEmpty() ) System.out.println("Still empty!");
        if( this.isEmpty() )
        {
            if( truthTable.get(0).getResult() ) System.out.println("No truth table to be printed, formula is constantly true");
            else System.out.println("No truth table to be printed, formula is constantly false");
        }
        else
        {
            System.out.println(atomOrder.toString());
            for(TruthTableRow row : truthTable)
            {
                System.out.print(row.getRowNumber() + " | ");
                for(int i = 0; i < row.getAssignment().size(); i++)
                    System.out.print(row.getAssignment().get(i) + " ");
                //if(row.getResult() != null)
                    System.out.println("| " + row.getResult());
            }
        }
    }

    public int compareRows(int firstRow, int secondRow)
    {
        int difference = 0;
        for(int i = 0; i < truthTable.get(firstRow).getAssignment().size(); i++)
        {
            if( !truthTable.get(firstRow).getAssignment().get(i).equals(truthTable.get(secondRow).getAssignment().get(i)) )
                difference++;
        }
        return difference;
    }

    public int getDifferentPosition(int firstRow, int secondRow)
    {
        if( compareRows(firstRow, secondRow) == 1 )
        {
            int j = -1;
            for(int i = 0; i < truthTable.get(firstRow).getAssignment().size(); i++)
                if( !truthTable.get(firstRow).getAssignment().get(i).equals(truthTable.get(secondRow).getAssignment().get(i)) )
                    j = i;

            return j;
        }
        else return -1;
    }

    public boolean isFound()
    {
        return isFound;
    }

    public void setIsFound()
    {
        isFound = true;
    }

    //public void setIsEmpty() { isEmpty = true; }

    public boolean isEmpty() { return isEmpty; }

    /**
     * Checks whether the formula is tautology or a contradiction
     * @return 0 for tautology, -1 for contradiction, 1 else
     */
    public int isTautology()
    {
        boolean taut = true;
        boolean contr = true;
        for(TruthTableRow row : truthTable)
        {
            if (row.getResult()) contr = false;
            if (!row.getResult()) taut = false;
        }
        if( taut ) return 0;
        else if( contr ) return -1;
        else if( !taut && !contr )return 1;
        return 1;
    }

    /**
     * Appends table to the current table
     */
    public void append(TruthTable table)
    {
        for(TruthTableRow row : table.getTruthTable())
            addRow(row);
    }

    public TruthTable()
    {
        truthTable = new LinkedList<TruthTableRow>();
        isFound = false;
        isEmpty = false;
    }

    public TruthTable(boolean empty)
    {
        truthTable = new LinkedList<TruthTableRow>();
        isFound = false;
        isEmpty = empty;
    }
}
