import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.Vector;

public class KVMinimizer extends MinimizerTemplate implements Runnable
{
    @Override
    public void run()
    {
        finished = false;
        startTime = System.currentTimeMillis();
        karnaughVeitchMinimization();
    }

    /**
     * Minimizes the given formula using the Karnaugh-Veitch algorithm
     * @return A String object representing the minimized formula
     */
    public String karnaughVeitchMinimization()
    {
        if( formula.getTruthTable().isEmpty() )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return formula.getTruthTableAsList().get(0).getResult()? "true" : "false";
        }

        if( !formula.getTruthTable().isFound() ) formula.findTruthTable();

        if( formula.getErrorOccured() )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return formula.getRepresentedFormula();
        }

        //tautology and contradiction handling
        if( formula.getTruthTable().isTautology() == 0 )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return "c(v)";
        }
        else if( formula.getTruthTable().isTautology() == -1 )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return "c(f)";
        }

        if( formula.getTruthTable().getTruthTable().size() == 2 )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return formula.getFormulaAtoms().first().getName();
        }

        kvHyperCube hyperCube = new kvHyperCube(formula.getTruthTable());

        //A set, where each List represents one block and each Vector represents a node in this block
        //todo: nicer solution? possibly a tool class as wrapper?
        //todo: possibly causing performance issues
        LinkedList<List<kvHyperCubeNode>> validBlocks = new LinkedList<List<kvHyperCubeNode>>();

        //generate blocks and test them
        TreeSet<Integer> validSizes = new TreeSet<Integer>();
        for (int i = formula.getTruthTable().getAtomOrder().size(); i >= 0; i--)
        {
            int pow = 1;

            for(int j = 0; j < i; j++) pow *= 2;

            validSizes.add(pow);
            //System.out.println("Size added: " + pow);
        }

        ICombinatoricsVector<kvHyperCubeNode> vector = Factory.createVector(hyperCube.getHyperCube());
        for(Integer i : validSizes)
        {
            Generator<kvHyperCubeNode> generator = Factory.createSimpleCombinationGenerator(vector, i);

            for(ICombinatoricsVector<kvHyperCubeNode> block : generator)
                if( hyperCube.validateBlock(block.getVector()) && !validBlocks.contains(block.getVector()))
                {
                    validBlocks.add(block.getVector());

                    //System.out.println("New block:");
                    //int l = 0;
                    //for (kvHyperCubeNode node : block)
                    //{
                    //    System.out.print(l + " |");
                    //    for (String s : node.getAssignment()) System.out.print(" " + s);
                    //    l++;
                    //    System.out.println();
                    //}
                }
        }

        //find minimal set of blocks so that all minterms are covered
        LinkedList<LinkedList<String>> minTerms = new LinkedList<LinkedList<String>>();
        for(TruthTableRow row : formula.getTruthTable().getTruthTable())
            if( row.getResult() ) minTerms.add(row.getAssignment());

        /*
        LinkedList<Vector<String>> coveredMinTerms = new LinkedList<Vector<String>>();
        int i = validBlocks.size()-1;
        while( !coveredMinTerms.containsAll(minTerms) && i >= 0 )
        {
            LinkedList<LinkedList<String>> newMinTerms = new LinkedList<LinkedList<String>>();

            //int k = 0;
            for(kvHyperCubeNode node : validBlocks.get(i))
            {
                newMinTerms.add(new LinkedList<String>());
                for(String s : node.getAssignment()) newMinTerms.getLast().add(s);
                //k++;
            }

            if( !coveredMinTerms.contains(newMinTerms) ) minimalBlockSet.add(validBlocks.get(i));


            System.out.println("Added block #" + i);
            for(kvHyperCubeNode minTermNode : validBlocks.get(i))
            {
                coveredMinTerms.add(minTermNode.getAssignment());
            }
            i--;
        }
        */
        List<List<kvHyperCubeNode>> minimalBlockSet = new LinkedList<List<kvHyperCubeNode>>();
        int minimalChVars = 0;
        float minCosts = 0;

        if( validBlocks.size() == 0 )
        {
            finished = true;
            finTime = System.currentTimeMillis();
            return "c(f)";
        }
        else if( validBlocks.size() == 1 )
            minimalBlockSet.addAll(validBlocks);
        else
        {
            //generate and test
            ICombinatoricsVector<List<kvHyperCubeNode>> blockVector = Factory.createVector(validBlocks);    //possible solutions
            //Factory.createVector(validBlocks);
            for (int i = 1; i <= validBlocks.size(); i++) //i=0 was actually i=1??
            {
                //System.out.println("Test");
                Generator<List<kvHyperCubeNode>> generator = Factory.createSimpleCombinationGenerator(blockVector, i);

                for (ICombinatoricsVector<List<kvHyperCubeNode>> blocks : generator)
                {
                    LinkedList<LinkedList<String>> coveredMinTerms = new LinkedList<LinkedList<String>>();
                    int changingVars = 0;
                    float costs = 0;

                    int x = 1;
                    for (List<kvHyperCubeNode> block : blocks)
                    {
                        for (kvHyperCubeNode node : block)
                        {
                            coveredMinTerms.add(new LinkedList<String>());
                            for (String s : node.getAssignment()) coveredMinTerms.getLast().add(s);
                        }
                        changingVars += Math.log10(block.size())/Math.log10(2);
                        //System.out.println("Changing vars: " + changingVars);
                        x++;
                    }
                    costs += formula.getTruthTable().getAtomOrder().size() - changingVars;

                    //System.out.println(minTerms.toString());

                    if( minimalBlockSet.size() == 0 && coveredMinTerms.containsAll(minTerms) )
                    {
                        //System.out.println("Hello, i == 0!");
                        minimalBlockSet = blocks.getVector();
                        minCosts = costs;
                    }

                    //System.out.println("Blocks: ");
                    //if(coveredMinTerms.containsAll(minTerms))
                    //{
                    //for(List<kvHyperCubeNode> block : minimalBlockSet)
                    //{
                    //    for(kvHyperCubeNode node : block)
                    //        System.out.println(node.getAssignment().toString());
                    //    System.out.println("__________________________________________");
                    //}
                    //}

                    if (coveredMinTerms.containsAll(minTerms) && //(blocks.getSize() < minimalBlockSet.size() || || minimalBlockSet.size()
                            (costs < minCosts && blocks.getSize() <= minimalBlockSet.size())/*|| minimalChVars == 0)*/)
                    {
                        //System.out.println("Hello!");
                        minimalBlockSet = blocks.getVector();
                        minimalChVars = changingVars;
                    }
                }
            }
        }
        //build a String representing the found minimal set
        String minimizedFormula = "";
        if( minimalBlockSet.size() >= 2 ) minimizedFormula += "or(";
        //else if( minimalBlockSet.size() == 0 ) minimizedFormula += "c(f)";

        //System.out.println("Minimal block set: ");
        //for(List<kvHyperCubeNode> block : minimalBlockSet)
        //{
        //    for(kvHyperCubeNode node : block)
        //        System.out.println(node.getAssignment().toString());
        //    System.out.println("__________________________________________");
        //}

        int i = 0;
        int j = 0;
        for(List<kvHyperCubeNode> block : minimalBlockSet) //for each block
        {
            //find shifting variables to leave them out later
            Vector<Integer> changingVars = new Vector<Integer>();
            for(kvHyperCubeNode node1 : block)
                for(kvHyperCubeNode node2 : block)
                {
                    int k = 0;
                    for (String s : node1.getAssignment())
                    {
                        if (!s.equals(node2.getAssignment().get(k)) && !changingVars.contains(k)) {
                            changingVars.add(k);}
                        k++;
                    }
                }

            i = 0;
            int ones = 0;
            for(String s : block.get(0).getAssignment() )
            {
                if( (s.equals("true") || s.equals("false")) && !changingVars.contains(i) ) ones++;
                i++;
            }
            //if( block.get(0).getAssignment().size() >= 2 ) minimizedFormula += "and(";
            if( ones > 1 ) minimizedFormula += "and(";
            i = 0;
            int k = 0;
            for(String s : block.get(0).getAssignment())
            {
                if( s.equals("true") && !changingVars.contains(k) )
                {
                    if( i != 0 ) minimizedFormula += ",";
                    minimizedFormula += formula.getTruthTable().getAtomOrder().get(k);
                    i++;
                }
                else if( s.equals("false") && !changingVars.contains(k) )
                {
                    if (i != 0) minimizedFormula += ",";
                    minimizedFormula += "neg(" + formula.getTruthTable().getAtomOrder().get(k) + ")";
                    i++;
                }
                k++;
            }
            //if( block.get(0).getAssignment().size() >= 2 ) minimizedFormula += ")";
            if( ones > 1 ) minimizedFormula += ")";
            if( j < minimalBlockSet.size()-1 )  minimizedFormula += ",";
            j++;
        }
        if( minimalBlockSet.size() >= 2 ) minimizedFormula += ")";

        this.minimizedFormula = minimizedFormula;
        finTime = System.currentTimeMillis();
        finished = true;
        //System.out.println("Returning.");
        return minimizedFormula;
    }

    public KVMinimizer(FormulaTree formula)
    {
        this.formula = formula;
        minimizedFormula = "";
    }

    /* -------------------------------------------------------------------------------------------------------------- */
    public static void test()
    {
        //Don't completly trust these tests. Minimizer can sometimes produce a formula with swapped disjunctions.
        String f1 = "ac(s,or(and(<2>,<3>), and(<4>, xor(<2>,<3>))))";
        String s1 = "or(and(<2>,<3>),and(<2>,<4>),and(<3>,<4>))";
        KVMinimizer t1 = new KVMinimizer(new FormulaTree(f1));
        String m1 = t1.karnaughVeitchMinimization();
        System.out.println("Control result: " + s1);
        System.out.println("Minimized formula: " + m1);
        if( s1.equals(m1) ) System.out.println("Formula 1 ..... successful.");
        else System.out.println("Formula 1 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f2 = "ac(s,and(A, or(neg(A),B)))";
        String s2 = "and(A,B)".toLowerCase();
        KVMinimizer t2 = new KVMinimizer(new FormulaTree(f2));
        String m2 = t2.karnaughVeitchMinimization();
        System.out.println("Control result: " + s2);
        System.out.println("Minimized formula: " + m2);
        if( s2.equals(m2) ) System.out.println("Formula 2 ..... successful.");
        else System.out.println("Formula 2 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f3 = "ac(s,or(c(f),c(v)))";
        String s3 = "c(v)";
        KVMinimizer t3 = new KVMinimizer(new FormulaTree(f3));
        String m3 = t3.karnaughVeitchMinimization();
        System.out.println("Control result: " + s3);
        System.out.println("Minimized formula: " + m3);
        if( s3.equals(m3) ) System.out.println("Formula 3 ..... successful.");
        else System.out.println("Formula 3 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f4 = "ac(s,and(A,B,C,neg(D)))";
        String s4 = "and(A,B,C,neg(D))".toLowerCase();
        KVMinimizer t4 = new KVMinimizer(new FormulaTree(f4));
        String m4 = t4.karnaughVeitchMinimization();
        System.out.println("Control result: " + s4);
        System.out.println("Minimized formula: " + m4);
        if( s4.equals(m4) ) System.out.println("Formula 4 ..... successful.");
        else System.out.println("Formula 4 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f5 = "ac(s,and(A,B,C,neg(D)))";
        String s5 = "and(A,B,C,neg(D))".toLowerCase();
        KVMinimizer t5 = new KVMinimizer(new FormulaTree(f5));
        String m5 = t5.karnaughVeitchMinimization();
        System.out.println("Control result: " + s5);
        System.out.println("Minimized formula: " + m5);
        if( s5.equals(m5) ) System.out.println("Formula 5 ..... successful.");
        else System.out.println("Formula 5 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f6 = "ac(s,or(a,and(A,B)))";
        String s6 = "A".toLowerCase();
        KVMinimizer t6 = new KVMinimizer(new FormulaTree(f6));
        String m6 = t6.karnaughVeitchMinimization();
        System.out.println("Control result: " + s6);
        System.out.println("Minimized formula: " + m6);
        if( s6.equals(m6) ) System.out.println("Formula 6 ..... successful.");
        else System.out.println("Formula 6 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f7 = "ac(s,iff(imp(A,B),imp(A,and(A,B))))";
        String s7 = "c(v)";
        KVMinimizer t7 = new KVMinimizer(new FormulaTree(f7));
        String m7 = t7.karnaughVeitchMinimization();
        System.out.println("Control result: " + s7);
        System.out.println("Minimized formula: " + m7);
        if( s7.equals(m7) ) System.out.println("Formula 7 ..... successful.");
        else System.out.println("Formula 7 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        // xor(xor(atom(bit(pSum(1), 3)), atom(bit(line(2), 3))), atom(carry(pSum(1), line(2), 3)))
        String f8 = "ac(s,xor(xor(A,B),C))";
        String s8 = "or(and(A,B,C),and(A,neg(B),neg(C)),and(neg(A),B,neg(C)),and(neg(A),neg(B),C))".toLowerCase();
        KVMinimizer t8 = new KVMinimizer(new FormulaTree(f8));
        String m8 = t8.karnaughVeitchMinimization();
        System.out.println("Control result: " + s8);
        System.out.println("Minimized formula: " + m8);
        if( s8.equals(m8) ) System.out.println("Formula 8 ..... successful.");
        else System.out.println("Formula 8 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f9 = "ac(s, or(and(A, B), and(C, xor(A, B))))";
        String s9 = "or(and(A,B),and(A,C),and(B,C))".toLowerCase();
        KVMinimizer t9 = new KVMinimizer(new FormulaTree(f9));
        String m9 = t9.karnaughVeitchMinimization();
        System.out.println("Control result: " + s9);
        System.out.println("Minimized formula: " + m9);
        if( s9.equals(m9) ) System.out.println("Formula 9 ..... successful.");
        else System.out.println("Formula 9 ..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f10 = "ac(s, and(A, B))";
        String s10 = "and(A, B)".toLowerCase();
        KVMinimizer t10 = new KVMinimizer(new FormulaTree(f10));
        String m10 = t10.karnaughVeitchMinimization();
        System.out.println("Control result: " + s10);
        System.out.println("Minimized formula: " + m10);
        if( s10.equals(m10) ) System.out.println("Formula  10..... successful.");
        else System.out.println("Formula 10..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f11 = "ac(s, and(neg(iff(A, xor(xor(B, C), D))), neg(E)))";
        String s11 = "or(and(neg(A),neg(B),neg(C),D,neg(E)),and(neg(A),neg(B),C,neg(D),neg(E)),and(neg(A),B,neg(C),neg(D),neg(E)),and(neg(A),B,C,D,neg(E)),and(A,neg(B),neg(C),neg(D),neg(E)),and(A,neg(B),C,D,neg(E)),and(A,B,neg(C),D,neg(E)),and(A,B,C,neg(D),neg(E)))".toLowerCase();
        KVMinimizer t11 = new KVMinimizer(new FormulaTree(f11));
        String m11 = t11.karnaughVeitchMinimization();
        System.out.println("Control result: " + s11);
        System.out.println("Minimized formula: " + m11);
        if( s11.equals(m11) ) System.out.println("Formula  11..... successful.");
        else System.out.println("Formula 11..... wrong.");
        System.out.println("_________________________________________________________________________");

        String f12 = "ac(s, and(neg(neg(A)), neg(B)))";
        String s12 = "and(A,neg(B))".toLowerCase();
        KVMinimizer t12 = new KVMinimizer(new FormulaTree(f12));
        String m12 = t12.karnaughVeitchMinimization();
        System.out.println("Control result: " + s12);
        System.out.println("Minimized formula: " + m12);
        if( s12.equals(m12) ) System.out.println("Formula  12..... successful.");
        else System.out.println("Formula 12..... wrong.");
        System.out.println("_________________________________________________________________________");
    }
}
