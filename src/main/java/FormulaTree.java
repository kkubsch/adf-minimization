import org.paukov.combinatorics.Factory;
import org.paukov.combinatorics.Generator;
import org.paukov.combinatorics.ICombinatoricsVector;

import java.util.*;

public class FormulaTree
{
    public static final String testFormula = "ac(s,imp(or(A,B),and(B,neg(C),D)))";
    public static final String testFormula2 = "ac(s, imp(or(A, B), and(B, neg(C), D))) ";
    public static final String testFormula3 = "ac(s, imp(or(c(v), B), and(B, neg(C), D))) ";
    public static final String testFormula4 = "ac(s, and(A,B,C,neg(D))) ";
    public static final String testFormula5 = "ac(s, or(c(v), B)) ";
    public static final String testFormula6 = "ac(s, and(A, or(neg(A),B)))";
    public static final String testFormula7 = "ac(<1>, or(and(<2>,<3>), and(<4>, xor(<2>,<3>)))).";
    public static final String testFormula8 = "ac(<1>, or(c(f),c(v))).";

    private PropOperator treeRoot;
    private FormulaReader reader;
    private TreeSet<PropAtom> formulaAtoms;
    private TruthTable truthTable;
    private String representedFormula;

    private boolean errorOccured;

    public void cleanup()
    {
        truthTable = null;
        formulaAtoms = null;
        representedFormula = null;
        treeRoot = null;
    }

    public boolean getErrorOccured() { return errorOccured; }

    public String getRepresentedFormula() { return representedFormula; }

    public PropOperator getTreeRoot() { return treeRoot; }

    public void printTruthTable()
    {
        if( !truthTable.isFound() ) findTruthTable();
        truthTable.print();
    }

    public LinkedList<TruthTableRow> getTruthTableAsList() { return truthTable.getTruthTable(); }
    public TruthTable getTruthTable() { return truthTable; }

    public void findTruthTable() {
        //System.out.print("Computing truth table...");
        if (!truthTable.isFound() && !truthTable.isEmpty())
        {
            //truthTable.setAtomOrder((LinkedList<String>) formulaAtoms);
            LinkedList<String> atomOrder = new LinkedList<String>();
            for (PropAtom atom : formulaAtoms)
                atomOrder.add(atom.getName());
            truthTable.setAtomOrder(atomOrder);
            //System.out.println(atomOrder.toString());

            ICombinatoricsVector<String> vector = Factory.createVector(new String[]{"true", "false"});
            Generator<String> generator = Factory.createPermutationWithRepetitionGenerator(vector, formulaAtoms.size());

            if (truthTable == null) truthTable = new TruthTable();
            int i = 0;
            for (ICombinatoricsVector<String> combination : generator) {
                Boolean b = evaluateFormula(combination.getVector());
                TruthTableRow newRow = new TruthTableRow(combination.getVector(), b.toString(), i);
                truthTable.addRow(newRow);
                i++;
                //System.out.println("done " + i);
            }
            truthTable.setIsFound();
        }
        //System.out.println("done.");
    }

    /**
     * Assigns given values to formulaAtoms and then calls recursive method evaluate()
     * Returns true or false for a given assignment. Assignment must be correctly sorted!
     */
    public boolean evaluateFormula(boolean[] assignment) throws IllegalArgumentException
    {
        if(assignment.length != formulaAtoms.size()) throw new IllegalArgumentException("Size of assignment invalid!");

        int i = 0;
        for(PropAtom atom : formulaAtoms)
        {
            atom.setValue(assignment[i]);
            i++;
        }
        setChildAtomValues(formulaAtoms, treeRoot);
        try
        {
            treeRoot.evaluateOperator();
        }
        catch (Exception e)
        {
            errorOccured = true;
        }

        if( treeRoot.getValue() ) return true;
        else return false;
    }

    /**
     * Assigns given values to formulaAtoms and then calls recursive method evaluate()
     * Returns true or false for a given assignment. Assignment must be correctly sorted!
     */
    public boolean evaluateFormula(List<String> assignment) throws  IllegalArgumentException
    {
        if(assignment.size() != formulaAtoms.size()) throw new IllegalArgumentException("Size of assignment invalid!");

        int i = 0;
        for(PropAtom atom : formulaAtoms)
        {
            atom.setValue(Boolean.parseBoolean(assignment.get(i)));
            i++;
        }
        setChildAtomValues(formulaAtoms, treeRoot);
        try
        {
            treeRoot.evaluateOperator();
        }
        catch (Exception e)
        {
            errorOccured = true;
        }

        if( treeRoot.getValue() ) return true;
        else return false;
    }

    /**
     * Recursively sets all atom's values according to assignment
     * @param assignment
     * @param currNode
     */
    public void setChildAtomValues(TreeSet<PropAtom> assignment, PropOperator currNode)
    {
        for(PropAtom a : assignment)
        {
            for(int i = 0; i < currNode.getChildAtoms().size(); i++)
            {
                if( a.getName().equals(currNode.getChildAtoms().elementAt(i).getName()) )
                    currNode.getChildAtoms().elementAt(i).setValue(a.getValue());
            }
        }
        for(PropOperator op : currNode.getChildOperators()) setChildAtomValues(assignment, op);
    }

    /**
     * Prints a subtree.
     * @param root The subtrees root
     * @param offSet (current) offset used in console
     */
    public void printTree(PropOperator root, int offSet)
    {
        //String off = String.format("%" + offSet + "s", "");
        String off = "";
        for(int j = 0; j < offSet; j++) off.concat(" ");    //todo: should be off = off.concat(" ")


        System.out.println(root.getOperatorTypeAsString() + " {");

        for(int i = 0; i < root.getChildAtoms().size(); ++i)
        {
            if( i == 0 ) System.out.print("ChildAtoms: ");
            System.out.print(off + root.getChildAtoms().elementAt(i).getName() + "  ");
            if( i == root.getChildAtoms().size()-1 ) System.out.println();
        }
        for(int i = 0; i < root.getChildOperators().size(); ++i)
        {
            if( i == 0 ) System.out.print("ChildOperators: ");
            System.out.print(off + root.getChildOperators().elementAt(i).getOperatorTypeAsString() + "  ");
            if( i == root.getChildOperators().size()-1 ) System.out.println();
        }
        System.out.println();
        for(int i = 0; i < root.getChildOperators().size(); ++i)
        {
            printTree(root.getChildOperators().elementAt(i), offSet+3);
        }

        System.out.println(off + "}");
    }

    /**
     * Prints the complete FormulaTree
     */
    public void printTree()
    {
        printTree(treeRoot, 0);
    }

    /**
     * Recursively builds the FormulaTree
     * @param currNode
     * @param currFormula
     */
    public void buildTree(PropOperator currNode, String currFormula)
    {
        //System.out.println("Formula in buildTree: " + currFormula);

        //todo: currFormula = c(f) and c(v) arent handled correctly
        //currFormula.trim();

        int i = currFormula.indexOf('(');
        //if( currFormula.startsWith("c(v)") || currFormula.startsWith("c(f)"))
        //{
        //    String type = currFormula.substring(0, currFormula.indexOf(')')+1);
        //    currNode.addChildOperator(new PropOperator(type, ""));

        //    String newFormula = currFormula.substring(5/*type.length()+1*/);
        //    if( newFormula.length() > 0 ) buildTree(currNode, newFormula);
        //}
        //else if( i < currFormula.indexOf(',') && currFormula.indexOf(',') != -1 && i != -1) // "(" occurs before "," so formula starts with operator
        /*else*/ if( (i != -1 && i < currFormula.indexOf(',')) || (i != -1 && currFormula.indexOf(',') == -1) )
        {
            if( (currFormula.startsWith("c(v)") || currFormula.startsWith("c(f)")) && currFormula.indexOf(',') != -1)
            {
                String type = currFormula.substring(0, currFormula.indexOf(')')+1);
                currNode.addChildOperator(new PropOperator(type, ""));

                String newFormula = currFormula.substring(5);  //type.length()+1
                if( newFormula.length() > 0 ) buildTree(currNode, newFormula);
            }
            else if( (currFormula.startsWith("c(v)") || currFormula.startsWith("c(f)")) && currFormula.indexOf(',') == -1 )
            {
                String type = currFormula.substring(0, currFormula.indexOf(')')+1);
                currNode.addChildOperator(new PropOperator(type, ""));

                //String newFormula = currFormula.substring(type.length()+1);  //5
                //if( newFormula.length() > 0 ) buildTree(currNode, newFormula);
            }
            else
            {
                String type = currFormula.substring(0, currFormula.indexOf('('));

                //System.out.println(currFormula);

                //throw away the operator name, but keep its brackets
                String newFormula = currFormula.substring(type.length());
                //newFormula = newFormula.substring(type.length());

                int countLeftBracket, countRightBracket, j;
                countLeftBracket = 0;
                countRightBracket = 0;
                j = 0;

                //find the operators closing bracket
                while (newFormula.charAt(j) != '(' && j < newFormula.length()) j++;
                do {
                    if (newFormula.charAt(j) == '(') countLeftBracket++;
                    else if (newFormula.charAt(j) == ')') countRightBracket++;
                    j++;
                } while (countLeftBracket != countRightBracket);
                //delete everything out of the operators brackets, but save residual formula before
                //newFormula = newFormula.substring(1, j-1);
                PropOperator newOp = new PropOperator(type, newFormula.substring(1, j - 1));
                currNode.addChildOperator(newOp);

                buildTree(newOp, newFormula.substring(1, j - 1));
                if (newFormula.length() > j)
                {
                    String leftover = newFormula.substring(j + 1);
                    buildTree(currNode, leftover);
                }
            }
        }
        else if( i > currFormula.indexOf(',') && currFormula.indexOf(',') != -1 ) // "," occurs before "(" so formula starts with atom
        {
            String name = currFormula.substring(0, currFormula.indexOf(','));

            buildTree(currNode, currFormula.substring(0, currFormula.indexOf(',')));
            if( currFormula.indexOf(',') != -1 )
            {
                //todo: error here
                String leftover = currFormula.substring(currFormula.indexOf(',')+1/*+name.length()*/);
                //System.out.println("currFormula: " + currFormula);
                //System.out.println("Leftover: " + leftover);
                buildTree(currNode, leftover);
            }
        }
        else if( currFormula.indexOf(',') != -1 && i == -1 )        //list of atoms found... e.g. currFormula = "a,b"
        {
            String name = currFormula.substring(0, currFormula.indexOf(','));
            currNode.addChildAtom(name);
            formulaAtoms.add(new PropAtom(name));
            //System.out.println("Atom added: " + name);

            buildTree(currNode, currFormula.substring(currFormula.indexOf(',') + 1));
        }
        else if( currFormula.indexOf(',') == -1 && i == -1 && currFormula.length() != 0 ) // last atom in formula found. No ","s or "("s anymore
        {
            String name = currFormula;
            currNode.addChildAtom(name);
            formulaAtoms.add(new PropAtom(name));
            //System.out.println("Atom added (last case): " + name);
        }
    }

    /**
     * Currently designed for testing! Use FormulaTree(String formula) instead!
     */
    /*public FormulaTree()
    {
        formulaAtoms = new TreeSet<PropAtom>(new PropAtomComparator());

        reader = new FormulaReader();
        reader.setRawFormula(testFormula3);
        reader.processFormula();

        String formula = reader.getProcessedFormula();
        String firstOp = reader.getFirstOperator();
        treeRoot = new PropOperator(firstOp, reader.getProcessedFormula());

        truthTable = new TruthTable();

        formula = formula.substring(firstOp.length()+1, formula.length()-1);
        System.out.println(formula);
        buildTree(treeRoot, formula);
    }*/

    public FormulaTree(String inputFormula)
    {
        errorOccured = false;

        representedFormula = inputFormula;

        //System.out.println("Building tree for: " + inputFormula);
        formulaAtoms = new TreeSet<PropAtom>(new PropAtomComparator());

        String processedFormula = FormulaReader.processFormula(inputFormula);

        if( processedFormula.indexOf('(') != -1 && !( processedFormula.startsWith("c(v)") || processedFormula.startsWith("c(f)") ))
        {
            String firstOp = reader.getFirstOperator(processedFormula);
            treeRoot = new PropOperator(firstOp, processedFormula);

            truthTable = new TruthTable();

            String formula = processedFormula.substring(firstOp.length()+1, processedFormula.length()-1);
            //System.out.println(formula);
            buildTree(treeRoot, formula);
        }
        else if( processedFormula.startsWith("c(v)") )
        {
            treeRoot = new PropOperator("c(v)", "");
            truthTable = new TruthTable(true);
            truthTable.addRow(null,true);
            truthTable.setIsFound();
            //truthTable.setIsEmpty();
            //System.out.println("is empty! " + Boolean.toString(truthTable.isEmpty()));
        }
        else if( processedFormula.startsWith("c(f)") )
        {
            treeRoot = new PropOperator("c(f)", "");
            truthTable = new TruthTable(true);
            truthTable.addRow(null,false);
            truthTable.setIsFound();
            //System.out.println("TruthTable: " + truthTable);
            //truthTable.setIsEmpty();
            //System.out.println("is empty! " + Boolean.toString(truthTable.isEmpty()));
        }
        else    //formula doesn't have any operators. Create trutTable manually for one atom
        {
            formulaAtoms.add(new PropAtom(processedFormula));
            truthTable = new TruthTable();

            LinkedList<String> atom = new LinkedList<String>();
            atom.add(processedFormula);
            truthTable.setAtomOrder(atom);

            LinkedList<String> list = new LinkedList<String>();
            list.add("true");
            truthTable.addRow(new TruthTableRow(list, "true", 0));

            list = new LinkedList<String>();
            list.add("false");
            truthTable.addRow(new TruthTableRow(list, "false", 1));

            truthTable.setIsFound();
        }
    }

    public void printFormulaAtoms()
    {
        System.out.print("Found atoms: ");
        Iterator<PropAtom> iterator = formulaAtoms.iterator();
        //System.out.print(formulaAtoms.first().getName() + " ");
        while( iterator.hasNext() ) System.out.print(iterator.next().getName() + " ");
        System.out.println();
    }

    public void setTruthTable(TruthTable newTruthTable)
    {
        this.truthTable = newTruthTable;
    }

    public TreeSet<PropAtom> getFormulaAtoms() { return formulaAtoms; }
}
