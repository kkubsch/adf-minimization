
public class PropAtom extends PropNode
{
    private String name;

    public String getName() { return name; }
    public void setName(String newName) { name = new String(newName); }
    public PropAtom(String name) { this.name = name; }
}
