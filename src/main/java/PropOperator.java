import java.util.TreeSet;
import java.util.Vector;

public class PropOperator extends PropNode
{
    public static final short AND = 0;
    public static final short OR = 1;
    public static final short NEG = 2;
    public static final short IMP = 3;
    public static final short IFF = 4;
    public static final short TRUE = 5;
    public static final short FALSE = 6;
    public static final short XOR = 7;
    public static final short NOR = 8;
    public static final short XNOR = 9;
    public static final short NAND = 10;
    public static final short INVALID_TYPE = -1;

    private short operatorType;
    private Vector<PropOperator> childOperators;
    private Vector<PropAtom> childAtoms;
    private String representedSubFormula;

    public void cleanup()
    {
        for(PropOperator p : childOperators) p.cleanup();
        for(PropAtom a : childAtoms) a = null;
        representedSubFormula = null;
    }

    /**
     * Determines the (boolean) value of this operator
     */
    public void evaluateOperator() throws Exception
    {
        for(PropOperator op : childOperators) op.evaluateOperator();

        if( operatorType == AND )
        {
            this.setValue(true);
            for(PropAtom a : childAtoms)
            {
                if(!a.getValue())
                {
                    this.setValue(false);
                    break;
                }
            }
            for(PropOperator op : childOperators)
            {
                if(!op.getValue())
                {
                    this.setValue(false);
                    break;
                }
            }
        }
        else if( operatorType == OR )
        {
            this.setValue(false);
            for(PropAtom a : childAtoms)
            {
                if(a.getValue())
                {
                    this.setValue(true);
                    break;
                }
            }
            for(PropOperator op : childOperators)
            {
                if(op.getValue())
                {
                    this.setValue(true);
                    break;
                }
            }
        }
        else if( operatorType == NEG )
        {
            if(childOperators.size() + childAtoms.size() == 1)
            {
                for(PropAtom a : childAtoms)
                {
                    if(a.getValue()) this.setValue(false);
                    else this.setValue(true);
                }
                for(PropOperator op : childOperators)
                {
                    if(op.getValue()) this.setValue(false);
                    else this.setValue(true);
                }
            }
            else
            {
                // error... negation cant have more than 1 operand
                System.out.println("Error on evalution: neg has more than 1 operand");
                throw new Exception("neg has more than 1 operand");
            }
        }
        else if( operatorType == IMP )
        {
            //System.out.println("Evaluating IMP...");
            if( childOperators.size()+childAtoms.size() == 2)
            {
                this.setValue(true);
                if( childOperators.size() == 1 && childAtoms.size() == 1)
                {
                    //System.out.println("...IMP case 1");
                    if( representedSubFormula.indexOf(',') > representedSubFormula.indexOf('(')
                            && representedSubFormula.indexOf(',') != -1 && representedSubFormula.indexOf('(') != -1)
                    {
                        //System.out.println("...case 1");
                        if( childAtoms.firstElement().getValue() && !childOperators.firstElement().getValue() )
                            this.setValue(false);
                    }
                    else if( representedSubFormula.indexOf(',') < representedSubFormula.indexOf('(')
                            && representedSubFormula.indexOf(',') != -1 && representedSubFormula.indexOf('(') != -1 )
                    {
                        //System.out.println("...case 2"); //Todo
                        if( childAtoms.firstElement().getValue() && !childOperators.firstElement().getValue() )
                            this.setValue(false);
                    }
                    else
                    {
                        // error... formula has wrong format
                        System.out.println("Error on evaluation: imp subformula has wrong format");
                        throw new Exception("imp subformula has wrong format");
                    }
                }
                else if( childAtoms.size() == 2 )
                {
                    if( childAtoms.firstElement().getValue() && !childAtoms.lastElement().getValue())
                        this.setValue(false);
                }
                else if( childOperators.size() == 2 )
                {
                    if( childOperators.firstElement().getValue() && !childOperators.lastElement().getValue())
                        this.setValue(false);
                }
            }
            else
            {
                // error... implication has more than 2 operands
                System.out.println("Error on evaluation: implication has more than 2 operands");
            }
        }
        else if( operatorType == IFF )
        {
            //System.out.println("Evaluating IFF...");
            if( childOperators.size()+childAtoms.size() <= 2)
            {
                this.setValue(false);
                if( childOperators.size() == 1 && childAtoms.size() == 1)
                {
                    //System.out.println("... case 1");
                    if( childAtoms.firstElement().getValue() && childOperators.firstElement().getValue())
                        this.setValue(true);
                    else if( !childAtoms.firstElement().getValue() && !childOperators.firstElement().getValue() )
                        this.setValue(true);
                }
                else if( childAtoms.size() == 2 )
                {
                    //System.out.println("... case 2");
                    if( childAtoms.firstElement().getValue() && childAtoms.lastElement().getValue())
                        this.setValue(true);
                    else if( !childAtoms.firstElement().getValue() && !childAtoms.lastElement().getValue() )
                        this.setValue(true);
                }
                else if( childOperators.size() == 2 )
                {
                    //System.out.println("... case 3");
                    if( childOperators.firstElement().getValue() && childOperators.lastElement().getValue())
                        this.setValue(true);
                    else if( !childOperators.firstElement().getValue() && !childOperators.lastElement().getValue() )
                     this.setValue(true);
                        //System.out.println("... case 2");}
                }
            }
            else
            {
                // error... iff has more than 2 operands
                System.out.println("Error on evaluation: iff has more than 2 operands:   " + representedSubFormula);
                System.out.print("Operands: ");
                for(PropOperator s : childOperators)
                {
                    System.out.print(s.getOperatorTypeAsString() + "  ");
                }
                throw new Exception("iff has more than 2 operands");
            }
        }
        else if( operatorType == TRUE )
            super.setValue(true);
        else if( operatorType == FALSE )
            super.setValue(false);
        else if( operatorType == XOR )
        {
            if( childAtoms.size() == 2
                    && ((childAtoms.get(0).getValue() && !childAtoms.get(1).getValue() )
                    || (!childAtoms.get(0).getValue() && childAtoms.get(1).getValue())) )
                super.setValue(true);
            else if( childAtoms.size() == 2
                    && ((childAtoms.get(0).getValue() && childAtoms.get(1).getValue() )
                    || (!childAtoms.get(0).getValue() && !childAtoms.get(1).getValue())) )
                super.setValue(false);
            else if( childAtoms.size() == 1 && childOperators.size() == 1
                    && ((childAtoms.get(0).getValue() && !childOperators.get(0).getValue() )
                    || (!childAtoms.get(0).getValue() && childOperators.get(0).getValue())))
                super.setValue(true);
            else if( childAtoms.size() == 1 && childOperators.size() == 1
                    && ((childAtoms.get(0).getValue() && childOperators.get(0).getValue() )
                    || (!childAtoms.get(0).getValue() && !childOperators.get(0).getValue())))
                super.setValue(false);
            else if( childOperators.size() == 2
                    && ((childOperators.get(0).getValue() && !childOperators.get(1).getValue())
                    || (!childOperators.get(0).getValue() && childOperators.get(1).getValue())) )
                super.setValue(true);
            else if( childOperators.size() == 2
                    && ((childOperators.get(0).getValue() && childOperators.get(1).getValue())
                    || (!childOperators.get(0).getValue() && !childOperators.get(1).getValue())) )
                super.setValue(false);

            else
            {
                System.out.println("Error on evaluation: XOR has more than 2 operands");
                throw new Exception("XOR has more than 2 operands");
            }
            // generalized operator evaluation for more than 2 operands
            /*int trueOperands = 0;
            for(PropAtom a : childAtoms)
            {
                if( trueOperands >= 2 ) break;
                if( a.getValue() ) trueOperands++;
            }
            for(PropOperator op : childOperators)
            {
                if( trueOperands >= 2 ) break;
                if( op.getValue() ) trueOperands++;
            }
            if( trueOperands == 1 ) super.setValue(true);
            else super.setValue(false);*/
        }
        else if( operatorType == NOR )
        {

            //generalized operator evaluation for more than 2 operands
            int trueOperands = 0;
            for(PropAtom a : childAtoms)
            {
                if( trueOperands >= 1 ) break;
                if( a.getValue() ) trueOperands++;
            }
            for(PropOperator op : childOperators)
            {
                if( trueOperands >= 1 ) break;
                if( op.getValue() ) trueOperands++;
            }
            if( trueOperands == 0 ) super.setValue(true);
            else super.setValue(false);
        }
        else if( operatorType == XNOR )
        {

            //generalized operator evaluation for more than 2 operands
            int trueOperands = 0;
            for(PropAtom a : childAtoms)
            {
                if( a.getValue() ) trueOperands++;
            }
            for(PropOperator op : childOperators)
            {
                if( op.getValue() ) trueOperands++;
            }
            //System.out.println(trueOperands);
            if( trueOperands % 2 == 0 ) super.setValue(true);
            else super.setValue(false);
        }
        else if( operatorType == NAND )
        {

            //generalized operator evaluation for more than 2 operands
            int trueOperands = 0;
            for(PropAtom a : childAtoms)
            {
                if( a.getValue() ) trueOperands++;
            }
            for(PropOperator op : childOperators)
            {
                if( op.getValue() ) trueOperands++;
            }
            if( trueOperands == (childAtoms.size()+childOperators.size()) ) super.setValue(false);
            else super.setValue(true);
        }
        else
        {
            // error, invalid type or type is TRUE/FALSE
            //System.err.println("Error while evaluating operator.");
        }
    }

    public Vector<PropOperator> getChildOperators() { return childOperators; }

    public Vector<PropAtom> getChildAtoms() { return childAtoms; }

    public short getOperatorType() { return operatorType; }

    public String getOperatorTypeAsString()
    {
        if( operatorType == AND ) return "AND";
        else if( operatorType == OR ) return "OR";
        else if( operatorType == NEG ) return "NEG";
        else if( operatorType == IMP ) return "IMP";
        else if( operatorType == IFF ) return "IFF";
        else if( operatorType == TRUE ) return "TRUE";
        else if( operatorType == FALSE ) return "FALSE";
        else if( operatorType == NOR ) return "NOR";
        else if( operatorType == NAND ) return "NAND";
        else if( operatorType == XOR ) return "XOR";
        else if( operatorType == XNOR ) return "XNOR";
        else return "INVALID";
    }

    public void addChildAtom(PropAtom atom) { childAtoms.add(atom); }

    public void addChildAtom(String name)
    {
        PropAtom newAtom = new PropAtom(name);
        childAtoms.add(newAtom);
    }

    public void addChildOperator(PropOperator operator) { childOperators.add(operator); }

    /**
     *
     * @param type type of the operator
     * @param subFormula the part of the formula represented by this operator
     */
    public void addChildOperator(String type, String subFormula) { childOperators.add(new PropOperator(type, subFormula)); }

    public PropOperator(String type, String subFormula)
    {
        //System.out.println("new Op: \"" + type + "\"");
        representedSubFormula = subFormula;

        type = type.toLowerCase();
        if( type.equals("and") ) operatorType = AND;
        else if( type.equals("or") ) operatorType = OR;
        else if( type.equals("neg") ) operatorType = NEG;
        else if( type.equals("imp") ) operatorType = IMP;
        else if( type.equals("iff") ) operatorType = IFF;
        else if( type.equals("c(v)") )
        {
            //System.out.println("Type set to True!");
            operatorType = TRUE;
            //super.setValue(true);
        }
        else if( type.equals("c(f)") )
        {
            operatorType = FALSE;
            //super.setValue(false);
        }
        else if( type.equals("nor") ) operatorType = NOR;
        else if( type.equals("nand") ) operatorType = NAND;
        else if( type.equals("xor") ) operatorType = XOR;
        else if( type.equals("xnor") ) operatorType = XNOR;
        else operatorType = INVALID_TYPE;

        childAtoms = new Vector<PropAtom>();
        childOperators = new Vector<PropOperator>();
    }
}
