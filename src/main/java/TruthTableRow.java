import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class TruthTableRow
{
    //private LinkedList<String> atomOrder;

    //private boolean[] bAssignment;
    private LinkedList<String> assignment;
    //private boolean bResult;
    private String result;
    private int rowNumber;
    /**
     * Contains the numbers of covered minterms in case the TruthTableRow represents an implicant
     */
    private Vector<Integer> covers;
    private boolean isPrimeTerm;

    public LinkedList<String> getAssignment() { return assignment; }

    //public boolean[] getAssignment() { return bAssignment; }

    //public String getResult() { return result; }

    public boolean getResult()
    {
        if( result.equals("true") ) return true;
        else return false;
        //return bResult;
    }

    public boolean getIsPrimeTerm() { return isPrimeTerm; }

    public void setIsPrimeTerm(boolean isPrime) { isPrimeTerm = isPrime; }

    public Vector<Integer> getCovers() { return covers; }

    public void setCovers(Vector<Integer> covers) { this.covers = covers; }

    public int getRowNumber() { return rowNumber; }

    public void setRowNumber(int newRowNumber) { rowNumber = newRowNumber; }

    public boolean isCovered(int covered)
    {
        if( covers.contains(covered)) return true;
        else return false;
    }

    public TruthTableRow(LinkedList<String> assignment, String result)
    {
        this.assignment = assignment;
        this.result = result;
    }

    public TruthTableRow(LinkedList<String> assignment, String result, int rowNumber) {
        this.assignment = assignment;
        /*
        bAssignment = new boolean[assignment.size()];
        int i = 0;
        for(String s : assignment)
        {
            if(s.equals("true")) bAssignment[i] = true;
            else if(s.equals("false")) bAssignment[i] = false;
            i++;
        }
        */

        this.result = result;
        /*
        if(result.equals("true")) bResult = true;
        else if(result.equals("false") bResult = false;
         */
        this.rowNumber = rowNumber;
        isPrimeTerm = true;
        covers = new Vector<Integer>();
    }

    public TruthTableRow(int[] assignment)
    {
        this.assignment = new LinkedList<String>();
        for(int i = 0; i < assignment.length; i++)
        {
            if( assignment[i] == 0 ) this.assignment.add("false");
            else if( assignment[i] == 1 ) this.assignment.add("true");
            else if( assignment[i] == -1 ) this.assignment.add("d.c.");
            else { /*todo error handling*/ }
        }
    }

    public TruthTableRow(List<String> assignment, String result, int rowNumber) {
        this.assignment = new LinkedList<String>();
        /*
        bAssignment = new boolean[assignment.size()];
        int i = 0;
        for(String s : assignment)
        {
            if(s.equals("true")) bAssignment[i] = true;
            else if(s.equals("false")) bAssignment[i] = false;
            i++;
        }
         */
        for(String s : assignment)
        {
            this.assignment.add(s);
        }

        /*
        if(result.equals("true")) bResult = true;
        else if(result.equals("false") bResult = false;
         */
        if( result.equals("true") || result.equals("false") ) this.result = result;
        this.rowNumber = rowNumber;
        isPrimeTerm = true;

        covers = new Vector<Integer>();
    }

    /**
     * Use for implicants
     * @param assignment
     * @param result
     * @param rowNumber
     * @param coverage
     */
    public TruthTableRow(List<String> assignment, String result, int rowNumber, Vector<Integer> coverage)
    {
        /*
        bAssignment = new boolean[assignment.size()];
        int i = 0;
        for(String s : assignment)
        {
            if(s.equals("true")) bAssignment[i] = true;
            else if(s.equals("false")) bAssignment[i] = false;
            i++;
        }
         */
        this.assignment = new LinkedList<String>();
        for(String s : assignment)
        {
            this.assignment.add(s);
        }

        /*
        if(result.equals("true")) bResult = true;
        else if(result.equals("false") bResult = false;
         */
        if( result.equals("true") || result.equals("false") ) this.result = result;
        this.rowNumber = rowNumber;

        covers = new Vector<Integer>(coverage);
        isPrimeTerm = true;
    }
}
