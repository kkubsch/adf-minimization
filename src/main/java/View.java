import javax.print.DocFlavor;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Iterator;
import java.util.Vector;

public class View implements ActionListener
{

    //todo: import several ADFs at once from a folder
    private JFrame frame;

    private JMenuBar menuBar;

    private JMenu fileMenu;
    private JMenuItem importFromFile;

    private JMenu minimizeMenu;
    private JMenuItem kvMinimize;
    private JMenuItem qmMinimize;
    private JMenuItem scherzo;

    //private JMenu measureMenu;
    //private JMenuItem kvMeasure;
    //private JMenuItem qmMeasure;
    //private JMenuItem scherzoMeasure;


    private JMenu resultsMenu;
    private JMenuItem exportMinimizedADF;

    private JScrollPane scrollPane;
    private JTextArea textField;
    private JButton button;
    private JLabel label;
    private JLabel errorLabel;

    //todo: make fileChooser only show diamond formats
    private JFileChooser fileChooser;

    private Vector<FormulaTree> formulae;
    private Vector<String> minimizedFormulae;
    private String completeADF;
    private Vector<String> statements;
    private Vector<String> substitutes;

    public View()
    {
        frame = new JFrame();
        frame.setLayout(null);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //menu construction
        menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        //file items
        fileMenu = new JMenu("File");
        importFromFile = new JMenuItem("Import ADF from file...");
        importFromFile.addActionListener(this);

        fileMenu.add(importFromFile);

        //minimize items
        minimizeMenu = new JMenu("Minimize");
        kvMinimize = new JMenuItem("Karnaugh-Veitch");
        kvMinimize.addActionListener(this);
        qmMinimize = new JMenuItem("Quine-McCluskey");
        qmMinimize.addActionListener(this);
        scherzo = new JMenuItem("SCHERZO");
        scherzo.addActionListener(this);
        //espresso = new JMenuItem("Espresso");
        //espresso.addActionListener(this);

        minimizeMenu.add(kvMinimize);
        minimizeMenu.add(qmMinimize);
        minimizeMenu.add(scherzo);
        //minimizeMenu.add(espresso);

        //measure items
        //measureMenu = new JMenu("Measuring");
        //kvMeasure = new JMenuItem("Karnaugh-Veitch");
        //kvMeasure.addActionListener(this);
        //qmMeasure = new JMenuItem("Quine-McCluskey");
        //qmMeasure.addActionListener(this);
        //scherzoMeasure = new JMenuItem("SCHERZO");
        //scherzoMeasure.addActionListener(this);

        //measureMenu.add(kvMeasure);
        //measureMenu.add(qmMeasure);
        //measureMenu.add(scherzoMeasure);

        //result items
        resultsMenu = new JMenu("Results");

        exportMinimizedADF = new JMenuItem("Export minimized ADF...");
        exportMinimizedADF.addActionListener(this);

        resultsMenu.add(exportMinimizedADF);

        menuBar.add(fileMenu);
        menuBar.add(minimizeMenu);
        //menuBar.add(measureMenu);
        menuBar.add(resultsMenu);

        scrollPane = new JScrollPane();
        scrollPane.setVerticalScrollBar(new JScrollBar());
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        frame.add(scrollPane);
        scrollPane.setLocation(2,2);
        scrollPane.setSize(250,200);
        scrollPane.setVisible(true);
        textField = new JTextArea("Enter an ADF here.");
        scrollPane.add(textField);
        textField.setSize(232,200);
        textField.setLocation(0,0);

        button = new JButton("Import!");
        button.setSize(80,20);
        button.addActionListener(this);
        frame.add(button);
        button.setLocation(254,101);

        label = new JLabel();
        errorLabel = new JLabel();
        frame.add(label);
        frame.add(errorLabel);
        label.setSize(500, 300);
        label.setLocation(2, 204);
        errorLabel.setSize(160, 20);
        errorLabel.setLocation(340, 101);

        label.setText("Hallo!");

        fileChooser = new JFileChooser();

        minimizedFormulae = new Vector<String>();

        frame.setTitle("ADF Minimizing");
        frame.setSize(500,500);  //todo
        //frame.pack();
        frame.setVisible(true);

        textField.setText(textField.getText() + "\n");
    }

    public void actionPerformed(ActionEvent event)
    {
        if( event.getSource().equals(importFromFile) )
        {
            int returnVal = fileChooser.showOpenDialog(frame);

            if( returnVal == JFileChooser.APPROVE_OPTION )
            {
                formulae = null;
                File file = fileChooser.getSelectedFile();

                try
                {
                    BufferedReader reader = new BufferedReader(new FileReader(file));
                    completeADF = new String();
                    while(reader.readLine() != null)
                        completeADF += reader.readLine();
                    reader.close();
                } catch (IOException e)
                {
                    errorLabel.setText("Error while reading file.");
                    e.printStackTrace();
                }

                //statement processing
                //todo: possible bug here, the FormulaReader should actually work on the ADF with substituted atoms
                processStatements();

                formulae = FormulaReader.readADFFromFile(file);
            }
            else
            {
                errorLabel.setText("No ADF was imported.");
            }
        }
        else if( event.getSource().equals(exportMinimizedADF) )
        {
            if( completeADF == null || minimizedFormulae.size() == 0 )
                errorLabel.setText("Error while exporting: No ADF has been imported or minimized yet.");
            else
            {
                int returnVal = fileChooser.showOpenDialog(frame);
                if( returnVal == JFileChooser.APPROVE_OPTION )
                {
                    File file = fileChooser.getSelectedFile();
                    String minimizedADF = constructExportFormula();

                    if( minimizedADF != null )
                        try
                        {
                            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                            writer.write(minimizedADF);
                            writer.close();
                        }
                        catch (Exception e)
                        {
                            errorLabel.setText("Exception while writing results file!");
                            e.printStackTrace();
                        }
                    else errorLabel.setText("Error while constructing output ADF.");
                    label.setText(minimizedADF);
                }
            }
        }
        else if( event.getSource().equals(kvMinimize) )
        {
            if( completeADF != null )
            {
                minimizedFormulae = new Vector<String>();
                for( FormulaTree formula : formulae )
                    minimizedFormulae.add(FormulaMinimizer.karnaughVeitchMinimization(formula));
                label.setText(constructExportFormula());
            }
            else if( formulae.size() == 0 )
                errorLabel.setText("Error: Formulae were not constructed.");
            else
                errorLabel.setText("Error: No ADF has been imported yet!");
        }
        else if( event.getSource().equals(qmMinimize) )
        {
            if( completeADF != null )
            {
                minimizedFormulae = new Vector<String>();
                for( FormulaTree formula : formulae )
                    minimizedFormulae.add(FormulaMinimizer.quineMccluskeyMinimization(formula));
                label.setText(constructExportFormula());
            }
            else if( formulae.size() == 0 )
                errorLabel.setText("Error: Formulae were not constructed.");
            else
                errorLabel.setText("Error: No ADF has been imported yet!");
        }
        /*
        else if( event.getSource().equals(scherzo) )
        {
            if( completeADF != null )
            {
                minimizedFormulae = new Vector<String>();
                for( FormulaTree formula : formulae )
                    minimizedFormulae.add(FormulaMinimizer.scherzoMinimization(formula));
                label.setText(constructExportFormula());
            }
            else if( formulae.size() == 0 )
                errorLabel.setText("Error: Formulae were not constructed.");
            else
                errorLabel.setText("Error: No ADF has been imported yet!");
        }
        */
        //else if( event.getSource().equals(espresso) )
        //{
        //    if( completeADF != null )
        //    {
        //        minimizedFormulae = new Vector<String>();
        //        for( FormulaTree formula : formulae )
        //            minimizedFormulae.add(FormulaMinimizer.espressoMinimization(formula));
        //        label.setText(constructExportFormula());
        //    }
        //    else if( formulae.size() == 0 )
        //        errorLabel.setText("Error: Formulae were not constructed.");
        //    else
        //        errorLabel.setText("Error: No ADF has been imported yet!");
        //}
        else if( event.getSource().equals(button) )
        {
            String adf = textField.getText();
            if( !adf.equals("") && !adf.equals("Enter an ADF here.") )
            {
                completeADF = adf;

                processStatements();

                minimizedFormulae = new Vector<String>();
                formulae = new Vector<FormulaTree>();
                for(String s : completeADF.split("\\n"))
                {
                    System.out.println(s);
                    if( s.startsWith("ac(") )
                        formulae.add(new FormulaTree(s));
                }
            }
            else
                errorLabel.setText("Enter an ADF first.");
        }
        /* //TEMPLATE
        else if( event.getSource().equals() )
        {
            //Todo: add action
        }
        */
    }

    /**
     * Substitutes each statement by a computed Integer number, which are later handled as names of atoms
     */
    private void processStatements()
    {
        if( completeADF == null ) return;

        String[] adfLines = completeADF.split("\\.");
        statements = new Vector<String>();

        //System.out.println(adfLines.length);
        for(int i=0; i < adfLines.length; i++)
        {
            adfLines[i] = adfLines[i].replace("\n", "");
            adfLines[i] += ".";
            if( adfLines[i].startsWith("s(")/* adfLines[i].matches("s(.+)\\.")*/ )
            {
                //System.out.println("Match");
                statements.add(adfLines[i].substring(2, adfLines[i].length()-2));
            }
            //System.out.println("tested: " + adfLines[i]);
        }

        substitutes = new Vector<String>();
        Integer i = 1;
        for( String s : statements )
        {
            //System.out.println("Running: " + s + " -> " + i);
            substitutes.add("<" + i.toString() + ">");
            s = s.trim();

            CharSequence replacement = substitutes.lastElement();
            CharSequence target = s;
            completeADF = completeADF.replace(target, replacement);
            //completeADF = completeADF.replaceAll(s, i.toString());
            i++;
        }

        //System.out.println(completeADF);
    }

    private String constructExportFormula()
    {
        if( completeADF != null && minimizedFormulae.size() != 0 )
        {
            String minimizedADF = "";
            String adfNoWhites = "";
            for(char c : completeADF.toCharArray())
            {
                if(c != ' ') adfNoWhites += c;
            }

            /*int i = 0;
            for(String s : minimizedFormulae)
            {
                adfNoWhites.indexOf("ac(");
                int index = adfNoWhites.indexOf("ac(");
                String nodeName = adfNoWhites.substring(index+3, adfNoWhites.indexOf(",", index+3));
                adfNoWhites = adfNoWhites.replaceFirst("ac\\(.+\\)" , "ac(" + nodeName + "," + s + ").\n");
                //adfNoWhites = adfNoWhites.replaceFirst("ac.+\\." , "ac(" + nodeName + "," + s + ").\n");

                //minimizedADF += adfNoWhites.substring(0, adfNoWhites.indexOf('.')+1); //or +2 to include \n?
                minimizedADF += adfNoWhites.substring(0, index + s.length()+4 + nodeName.length() + 1);
                //line above could also work with minimizedADF = adfNoWhites.substring(0, adfNoWhites.indexOf(".")+1);

                adfNoWhites = adfNoWhites.substring(index + s.length()+2);
                //adfNoWhites = adfNoWhites.substring(0, adfNoWhites.indexOf('.')+1);
                i++;
            }*/

            int i = 0;
            for(String s : adfNoWhites.split("\\n"))
            {

                if( s.startsWith("s(") )
                {
                    minimizedADF += s + "\n";
                    //adfNoWhites = adfNoWhites.substring(0, adfNoWhites.indexOf('.'));
                }
                else if( s.startsWith("ac(") )
                {
                    s = "ac(" + s.substring(3, s.indexOf(',')+1) + minimizedFormulae.get(i) + ").";
                    minimizedADF += s + "\n";
                    i++;
                }
            }

            //resubstitutes original statements back into the formula
            Integer j = 0;
            //System.out.println("CompleteADF before subs.: \n" + minimizedADF);
            for(String s : substitutes)
            {
                //CharSequence target = "<" + j.toString() + ">";
                CharSequence target = s;
                CharSequence replacement = statements.get(j).trim();
                //System.out.println("Substitution: " + target + " -> " + replacement);

                minimizedADF = minimizedADF.replace(target, replacement);

                //minimizedADF = minimizedADF.replaceAll(s, statements.get(i));
                j++;
            }

            //label.setText(completeADF);
            return minimizedADF;
        }
        else return null;
    }

    public void printError(String errMessage)
    {
        errorLabel.setText(errMessage);
    }
}
