import jdd.zdd.ZDD;
import jdd.zdd.ZDDGraph;

import java.util.Vector;

public class SCHMinimizer extends MinimizerTemplate implements Runnable
{
    @Override
    public void run()
    {
        finished = false;
        startTime = System.currentTimeMillis();
        //scherzoMinimization();
        ScherzoAlgorithms minimizer = new ScherzoAlgorithms(0,0,formula);
        this.minimizedFormula = minimizer.minimize();
        finished = true;
        finTime = System.currentTimeMillis();
        minimizer.cleanup();
    }

    /*public String scherzoMinimization() {
        //handling for special cases

        if (formula.getTruthTable().isEmpty()) {
            finTime = System.currentTimeMillis();
            finished = true;
            return formula.getTruthTableAsList().get(0).getResult() ? "true" : "false";
        }

        if (!formula.getTruthTable().isFound()) formula.findTruthTable();

        if (formula.getErrorOccured()) {
            finished = true;
            finTime = System.currentTimeMillis();
            return formula.getRepresentedFormula();
        }

        //tautology and contradiction handling
        if (formula.getTruthTable().isTautology() == 0) {
            finished = true;
            finTime = System.currentTimeMillis();
            return "c(v)";
        } else if (formula.getTruthTable().isTautology() == -1) {
            finished = true;
            finTime = System.currentTimeMillis();
            return "c(f)";
        }

        if (formula.getTruthTable().getTruthTable().size() == 2) {
            finished = true;
            finTime = System.currentTimeMillis();
            return formula.getFormulaAtoms().first().getName();
        }

        //compute zdd of prime(f)
        ZDDGraph zdd1 = new ZDDGraph(30000, 5000);  //todo: adjust correctly
        //ZDDGraph q = new ZDDGraph(30000, 5000);//

        Vector<Integer> variables = new Vector<Integer>();

        for(String s : formula.getTruthTable().getAtomOrder())
                variables.add(zdd1.createVar());

        int F = zdd1.cubes_union(formula.getTruthTable().toString());

        //compute cyclic core
        int i = 1;
        while( i == 1) //todo
        {
            // 1) Q = maxSubSetTauP(Q)

            // 2) Q = Q-E, P=P-E with E = Q intersect P

            // 3) P = maxSubSetTauQ(P)

        }
        //solve covering matrix

        //build minimized formula

        String minFormula = "";

        return minFormula;
    }*/



    public SCHMinimizer(FormulaTree formula)
    {
        this.formula = formula;
        minimizedFormula = "";
    }
}
