
public class PropNode
{
    private boolean value;

    public boolean getValue() { return value; }
    public void setValue(boolean newVal) { value = newVal; }
    public PropNode() { value = false; }
    public PropNode(boolean val) { value = val; }
}
