import java.util.Vector;

/**
 * A class which is able to generate single formulae or sets of them as String objects.
 */
public class FormulaGenerator
{
    private static int recursionCalls = 0;

    //todo: can generate "ac(s,neg(neg(b),neg(c),neg(a),b))"

    /**
     * Generates a number of formulae.
     * @param variables Variables to be used in the formulae.
     * @param maxOperatorDepth maximum depth of operator encapsulating.
     * @param numberOfFormulae targeted size of the returned Vector
     * @return a Vector of formulae
     */
    public static Vector<String> generateFormulaVector(Vector<String> variables, int maxOperatorDepth, int numberOfFormulae)
    {
        Vector<String> formulae = new Vector<String>();

        for(int i = 0; i < numberOfFormulae; i++)
        {
            formulae.add(generateFormula(variables, maxOperatorDepth));
        }

        return formulae;
    }

    public static String generateFormula(Vector<String> variables, int maxOperatorDepth/*, int maxFormulaLength*/)
    {
        recursionCalls = 0;
        String formula = "ac(s,";

        formula += generateNewOperator(variables, 0, maxOperatorDepth, formula);
        //recursionCalls++;
        /*double newOperator = Math.random();
        int i = 0;
        do
        {
            if( i != 0 ) formula += ",";
            formula += generateNewOperator(variables, 0, maxOperatorDepth, formula);
            newOperator = Math.random();
            i++;
        } while( newOperator < 0.5 && i < maxFormulaLength );*/
        /*if( newOperator < 0.5 )
        {
            formula += generateNewOperator(variables, 0, maxOperatorDepth);
        }
        else
        {
            //how many variables shall be added?
            double choice = variables.size() * Math.random();
            Vector<String> varOrder = new Vector<String>();

            for(int i = 0; i < Math.ceil(choice); i++)
            {
                double choice2 = variables.size() * Math.random();
                varOrder.add(variables.get((int) Math.floor(choice2)));
            }

            for(int i = 0; i < Math.ceil(choice); i++)
            {
                if( i > 0 && i < Math.ceil(choice)-1 ) formula += ",";
                double posNeg = Math.random();
                if( posNeg < 0.5 ) formula += varOrder.get(i);
            }
        }*/

        formula += ")";
        return formula;
    }

    private static String generateNewOperator(Vector<String> variables, int currOperatorDepth, int maxOperatorDepth, String currFormula)
    {
        String formula = "";

        //choose operator to add
        double choice = 5 * Math.random();
        if( choice < 1 ) formula += "and(";
        else if( choice < 2 && choice >= 1 ) formula += "or(";
        else if( choice < 3 && choice >= 2 ) formula += "neg(";
        else if( choice < 4 && choice >= 3 ) formula += "imp(";
        else if( choice < 5 && choice >= 4 ) formula += "iff(";

        //choose variables to add
        choice = Math.random();
        if( (choice < 0.5 && currOperatorDepth < maxOperatorDepth)
                || (formula.endsWith("neg(") && currOperatorDepth <= maxOperatorDepth)) //new Operator
        {
            if( recursionCalls > 0  && !currFormula.endsWith("(") ) formula += ","; //todo?? might be working
            recursionCalls++;
            if( formula.endsWith("imp(") || formula.endsWith("iff(") )
                formula += generateNewOperator(variables, currOperatorDepth+1, maxOperatorDepth, formula);
            else
                formula += generateNewOperator(variables, currOperatorDepth+1, maxOperatorDepth, formula);
        }
        else if( choice >= 0.5 || (choice < 0.5 && currOperatorDepth >= maxOperatorDepth) )     // literal
        {
            //how many variables shall be added?
            if( formula.endsWith("imp(") || formula.endsWith("iff(") ) choice = 2;
            else choice = variables.size() * Math.random();
            if( Math.ceil(choice) <= 1 ) choice += 1;

            Vector<String> varOrder = new Vector<String>();

            for(int i = 0; i < Math.ceil(choice); i++)
            {
                double choice2 = variables.size() * Math.random();
                varOrder.add(variables.get((int) Math.floor(choice2)));
            }

            for(int i = 0; i < Math.ceil(choice); i++)
            {
                if( i > 0 && i < Math.ceil(choice) ) formula += ",";
                double posNeg = Math.random();
                if( posNeg < 0.5 ) formula += varOrder.get(i);
                else formula += "neg(" + varOrder.get(i) + ")";
            }
        }

        formula += ")";
        return formula;
    }
}
