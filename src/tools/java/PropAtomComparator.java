import java.util.Comparator;

public class PropAtomComparator implements Comparator<PropAtom>
{
    @Override
    public int compare(PropAtom atom1, PropAtom atom2)
    {
        return atom1.getName().compareTo(atom2.getName());
    }
}
