import jdd.bdd.NodeTable;
import jdd.util.Allocator;
import jdd.util.JDDConsole;
import jdd.util.NodeName;
import jdd.zdd.ZDDPrinter;

import java.io.PrintStream;

/**
 * This class contains some modified code from the ZDDPrinter class created by Arash Vahidi.
 * The difference is that this class doesnt print to a console but rather returns the String to print.
 */
public class ZDDPrinterTool //extends ZDDPrinter
{
    private static NodeTable nt;
    private static PrintStream ps;
    private static final int NODE_MASK = 2147483647;
    private static final int DOT_MARK = -2147483648;
    private static boolean had_0;
    private static boolean had_1;
    private static NodeName nn;
    private static char[] set_chars = null;
    private static int max;
    private static int count;

    private static String printString = "";

    static String printSet(int var0, NodeTable var1, NodeName var2, int numVars) {
        printString = "";
        if(var0 < 2) {
            if(var2 != null) {
                //JDDConsole.out.println(var0 == 0?var2.zero():var2.one());
                printString += var0 == 0?var2.zero():var2.one() + "\n";
                return printString;
            } else {
                //JDDConsole.out.println(var0 == 0?"empty":"base");
                printString += var0 == 0?"empty\n":"base\n";
                return printString;
            }
        } else {
            //int var3 = 2 + numVars;
            int var3 = 2 + var1.getVar(var0);
            if(set_chars == null || set_chars.length < var3) {
                set_chars = Allocator.allocateCharArray(var3);
            }

            count = 0;
            nn = var2;
            nt = var1;
            //JDDConsole.out.print("{ ");
            printString += "{ ";
            printSet_rec(var0, 0, var1.getVar(var0));
            //JDDConsole.out.println(" }");
            printString += " }\n";
            helpGC();

            return printString;
        }
    }

    private static void printSet_rec(int var0, int var1, int var2) {
        if(var0 != 0) {
            if(var0 == 1 && var2 < 0) {
                if(count != 0) {
                    //JDDConsole.out.print(", ");
                    printString += ", ";
                }

                ++count;
                int var3;
                if(nn != null) {
                    var3 = 0;

                    for(int var4 = 0; var4 < var1; ++var4) {
                        if(set_chars[var4] == 49) {
                            //JDDConsole.out.print(nn.variable(var1 - var4 - 1));
                            printString += nn.variable(var1 - var4 - 1);
                            ++var3;
                        }
                    }

                    if(var3 == 0) {
                        //JDDConsole.out.print(nn.one());
                        printString += nn.one();

                    }
                } else {
                    for(var3 = 0; var3 < var1; ++var3) {
                        //JDDConsole.out.print(set_chars[var3]);
                        printString += set_chars[var3];
                    }
                }

            } else {
                --var2;
                if(nt.getVar(var0) <= var2) {
                    set_chars[var1] = 48;
                    printSet_rec(var0, var1 + 1, var2);
                } else {
                    set_chars[var1] = 48;
                    printSet_rec(nt.getLow(var0), var1 + 1, var2);
                    set_chars[var1] = 49;
                    printSet_rec(nt.getHigh(var0), var1 + 1, var2);
                }
            }
        }
    }

    private static void helpGC() {
        nt = null;
        ps = null;
        nn = null;
    }
}
