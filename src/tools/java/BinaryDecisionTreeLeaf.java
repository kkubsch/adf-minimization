/**
 * Created by Kevin on 15.04.2015.
 */
public class BinaryDecisionTreeLeaf
{
    private boolean value;

    public boolean getValue() { return value; }

    public BinaryDecisionTreeLeaf(boolean value)
    {
        this.value = value;
    }
}
