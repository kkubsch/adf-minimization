public class BinaryDecisionTree
{
    private BinaryDecisionTreeNode treeRoot;

    public BinaryDecisionTree(FormulaTree formula)
    {
        if( !formula.getTruthTable().isFound() ) formula.findTruthTable();

        //construct the BDD. For each row, the already build part of the tree is traversed and, if necessary, new nodes
        //are inserted until the leafs are reached.
        treeRoot = new BinaryDecisionTreeNode(null, formula.getTruthTable().getAtomOrder().get(0));

        for(TruthTableRow row : formula.getTruthTable().getTruthTable())
        {
            int i = 0;
            BinaryDecisionTreeNode currNode = treeRoot;
            for(String s : row.getAssignment())
            {
                if( s.equals("false") && currNode.getLeftChild() != null )
                {
                    currNode = currNode.getLeftChild();
                    continue;
                }
                else if( s.equals("true") && currNode.getRightChild() != null )
                {
                    currNode = currNode.getRightChild();
                    continue;
                }

                if( s.equals("false") && currNode.getLeftChild() == null )
                {
                    if( i < row.getAssignment().size()/*-1*/ )
                        currNode.setLeftChild(new BinaryDecisionTreeNode(currNode, formula.getTruthTable().getAtomOrder().get(i)));
                    else
                    {
                        Boolean b = row.getResult();
                        currNode.setLeftChild(new BinaryDecisionTreeLeaf(b));
                    }
                }
                else if( s.equals("true") && currNode.getRightChild() == null )
                {
                    if( i < row.getAssignment().size()/*-1*/ )
                        currNode.setRightChild(new BinaryDecisionTreeNode(currNode, formula.getTruthTable().getAtomOrder().get(i)));
                    else
                    {
                        Boolean b = row.getResult();
                        currNode.setRightChild(new BinaryDecisionTreeLeaf(b));
                    }
                }

                i++;
            }
        }
        System.out.println(this.toString());
    }

    /*@Override
    public String toString()
    {
        return toStringRecursion(treeRoot);
    }

    private String toStringRecursion(BinaryDecisionTreeNode currNode)
    {
        String s = "";

        s += currNode.getVariableName() + "\n";
        if( currNode.getLeftChild() != null ) s += toStringRecursion(currNode.getLeftChild()) + "\n";
        if( currNode.getRightChild() != null ) s += toStringRecursion(currNode.getRightChild()) + "\n";

        return s;
    }*/

    //public void print()
    //{
    //    printRecursion(treeRoot, "");
    //}

    public void printRecursion(BinaryDecisionTreeNode currNode, String offset)
    {
        if( currNode == null ) return;

        offset = offset + "\t";

        printRecursion(currNode.getLeftChild(), offset);
        System.out.println(currNode.getVariableName() + " ");
        printRecursion(currNode.getRightChild(), offset);
    }

    public void print() { treeRoot.print("", true); }

}
