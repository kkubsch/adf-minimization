
public class BinaryDecisionTreeNode
{
    private BinaryDecisionTreeNode parent;
    //private BinaryDecisionTreeNode children[];
    private BinaryDecisionTreeNode leftChild;
    private BinaryDecisionTreeNode rightChild;
    private BinaryDecisionTreeLeaf leftLeaf;
    private BinaryDecisionTreeLeaf rightLeaf;
    private String variableName;

    private boolean isRoot;

    public BinaryDecisionTreeNode(BinaryDecisionTreeNode parentNode, String variableName)
    {
        this.variableName =  variableName;

        if( parentNode == null ) isRoot = true;
        else this.parent = parentNode;
    }

    public String getVariableName() { return variableName; }

    public BinaryDecisionTreeNode getLeftChild()
    {
        //if( children == null ) return null;
        //else return children[0];
        return leftChild;
    }

    public BinaryDecisionTreeNode getRightChild()
    {
        //if( children == null ) return null;
        //else return children[1];
        return rightChild;
    }

    public void setLeftChild(BinaryDecisionTreeNode leftChild)
    {
        //if( children == null ) children = new BinaryDecisionTreeNode[2];
        //children[0] = leftChild;
        this.leftChild = leftChild;
    }

    public void setRightChild(BinaryDecisionTreeNode rightChild)
    {
        //if( children == null ) children = new BinaryDecisionTreeNode[2];
        //children[1] = rightChild;
        this.rightChild = rightChild;
    }

    public void setLeftChild(BinaryDecisionTreeLeaf leftChild)
    {
        //if( leafs == null ) leafs = new BinaryDecisionTreeLeaf[2];
        //leafs[0] = leftChild;
        this.leftLeaf = leftChild;
        this.leftChild = null;
    }

    public void setRightChild(BinaryDecisionTreeLeaf rightChild)
    {
        //if( leafs == null ) leafs = new BinaryDecisionTreeLeaf[2];
        //leafs[1] = rightChild;
        this.rightLeaf = rightChild;
        this.rightChild = null;
    }

    public void print(String prefix, boolean isTail)
    {
        System.out.println(prefix + (isTail ? "└── " : "├── ") + variableName);
        if( leftChild != null ) leftChild.print(prefix + (isTail ? "    " : " |   "), false);
        if( rightChild != null ) rightChild.print(prefix + (isTail ? "    " : " |   "), false);
    }

    /*private void print(String prefix, boolean isTail) {
        System.out.println(prefix + (isTail ? "└── " : "├── ") + name);
        for (int i = 0; i < children.size() - 1; i++) {
            children.get(i).print(prefix + (isTail ? "    " : "│   "), false);
        }
        if (children.size() > 0) {
            children.get(children.size() - 1).print(prefix + (isTail ?"    " : "│   "), true);
        }
    }*/
}
