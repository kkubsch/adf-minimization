import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

/**
 * Holds the hyper cube reprensation of a TruthTable object. Used for KV-minimization.
 */
public class kvHyperCube
{
    private Vector<kvHyperCubeNode> hyperCube;
    private boolean[][] adjacencyMatrix;

    private int numberOfMinTerms;

    private boolean[][] getAdjacencyMatrix() { return adjacencyMatrix; }

    public Vector<kvHyperCubeNode> getHyperCube() { return hyperCube; }

    public int getNumberOfMinTerms() { return numberOfMinTerms; }

    /**
     * Generates the hyper cube from a TruthTable object
     * @param truthTable The TruthTable to be transformed into its hyper cube representation
     */
    public kvHyperCube(TruthTable truthTable)
    {
        hyperCube = new Vector<kvHyperCubeNode>();

        for(TruthTableRow row : truthTable.getTruthTable())
            if( row.getResult() )
            {
                Vector<String> assignment = new Vector<String>();
                for (String s : row.getAssignment())
                    assignment.add(s);

                if( row.getResult() ) numberOfMinTerms++;

                kvHyperCubeNode node = new kvHyperCubeNode(assignment, row.getResult());
                if( !hyperCube.contains(node) ) hyperCube.add(node);
            }

        //todo: optimize this part - possibly better ways than n²
        for(kvHyperCubeNode node1 : hyperCube)
            for(kvHyperCubeNode node2 : hyperCube)
                if( !node1.equals(node2) && node1.isNeighbourOf(node2) )
                {
                    node1.addNeighbour(node2);
                    node2.addNeighbour(node1);
                    //System.out.println("Neighbours added!");
                }

        buildAdjacencyMatrix();
    }

    private void buildAdjacencyMatrix()
    {
        adjacencyMatrix = new boolean[hyperCube.size()][hyperCube.size()];

        for(int i = 0; i < hyperCube.size(); i++)
            for(int j = 0; j < hyperCube.size(); j++)
                if( hyperCube.get(i).equals(hyperCube.get(j)) || hyperCube.get(i).isNeighbourOf(hyperCube.get(j)))
                    adjacencyMatrix[i][j] = true;
                else adjacencyMatrix[i][j] = false;
    }

    /**
     * Determines whether node2 is reachable from node1. Uses Warshalls algorithm.
     * @param node1
     * @param node2
     * @return
     */
    public boolean canReach(kvHyperCubeNode node1, kvHyperCubeNode node2)
    {
        boolean[][] adMatCopy = adjacencyMatrix;

        for(int i = 0; i < hyperCube.size(); i++)
            for(int j = 0; j < hyperCube.size(); j++)
                if( adMatCopy[j][i] )
                    for( int k = 0; k < hyperCube.size(); k++)
                        if( adMatCopy[i][k] ) adMatCopy[j][k] = true;

        if( adMatCopy[hyperCube.indexOf(node1)][hyperCube.indexOf(node2)] ) return true;
        else return false;
    }

    /**
     * Checks whether the block is valid. A block is valid iff
     * a) it is connected
     * b) its size is element of the set 2^i (for i=1..n where n is the number of variables used)
     * c) the number of changing variables is less or equal to i when the blocks size is equal to 2^i
     * @param block the set of variables to be checked
     * @return true, if the block is valid. Else returns false
     */
    public boolean validateBlock(List<kvHyperCubeNode> block)
    {
        boolean isValid = true;
        int variableCount = block.get(0).getAssignment().size();

        //check whether size is valid
        //todo: this part is possibly unneeded
        /*Set<Integer> validSizes = new TreeSet<Integer>();
        for (int i = 0; i < variableCount; i++)
        {
            int pow = 1;

            for(int j = 0; j < i; j++) pow *= 2;

            validSizes.add(pow);
        }

        if( !validSizes.contains(block.size()) )
        {
            isValid = false;
            return false;
        }*/

        //check whether number of changing variables is valid

        //todo: possibly faster solution here? only one loop, keep assignment
        //Vector<Boolean> varChanged = new Vector<Boolean>();
        boolean varChanged[] = new boolean[variableCount];
        //for(Boolean b : varChanged) b = false;
        for(int i = 0; i < variableCount; i++) varChanged[i] = false;

        for(kvHyperCubeNode node1 : block)
            for(kvHyperCubeNode node2 : block)
            {
                //int i = 0;
                //for(String s : node1.getAssignment())
                for(int i = 0; i < Math.min(node1.getAssignment().size(), node1.getAssignment().size()); i++)
                {
                    if( !node1.getAssignment().get(i).equals(node2.getAssignment().get(i)))
                    {
                        //i++;
                        varChanged[i] = true;
                    }
                    //i++;
                }
                //int changes = node1.getDifferenceCounter(node2);
                //if( changes > changingVariables ) changingVariables = changes;
            }

        int changingVariables = 0;
        for(int i = 0; i < variableCount; i++)
            if( varChanged[i] ) changingVariables++;

        if( !(changingVariables <= Math.log10(block.size())/Math.log10(2)) )
        {
            isValid = false;
            return false;
        }
        else /*System.out.println("Correct number of changing variables!")*/;

        //check whether block is connected

        boolean connected = true;
        for(kvHyperCubeNode node1 : block)
            for(kvHyperCubeNode node2 : block)
                if( !node1.equals(node2) && !canReach(node1, node2))
                {
                    //System.out.println("Invalid because non-connectivity");
                    return false;
                }

         return true;
    }
}
