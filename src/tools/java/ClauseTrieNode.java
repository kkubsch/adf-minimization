import java.util.Vector;

public class ClauseTrieNode
{
    private int id;
    private Vector<ClauseTrieNode> childNodes;
    private ClauseTrieLeaf childLeaf;
    private ClauseTrieNode parentNode;

    private boolean isRoot;

    public int getId() { return id; }

    public Vector<ClauseTrieNode> getChildNodes() { return childNodes; }

    public ClauseTrieNode getParentNode() { return parentNode; }

    public void addChildNode(ClauseTrieNode childNode) { childNodes.add(childNode); }

    public void setRoot() { isRoot = true; }

    public boolean getIsRoot() { return isRoot; }

    public ClauseTrieNode(int id, ClauseTrieNode parent)
    {
        this.id = id;

        if( parent != null ) parentNode = parent;
        else isRoot = true;

        childNodes = new Vector<ClauseTrieNode>();
        isRoot = false;
    }
}
