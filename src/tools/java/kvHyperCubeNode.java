import java.util.HashSet;
import java.util.LinkedList;
import java.util.Vector;

/**
 * Represents a minterm within the hyper cube form of a KV-diagram
 */
public class kvHyperCubeNode
{
    /**
     * The boolean encoding of the minterms number
     */
    private Vector<String> assignment;
    private boolean value;

    /**
     * Those kvHyperCubeNodes, which are different to this node in exactly one variable.
     */
    private LinkedList<kvHyperCubeNode> neighbours;

    public kvHyperCubeNode(Vector<String> assignment, boolean value)
    {
        this.assignment = assignment;
        this.value = value;
        neighbours = new LinkedList<kvHyperCubeNode>();
    }

    public kvHyperCubeNode(kvHyperCubeNode node)
    {
        this.assignment = node.assignment;
        this.value = node.value;
        neighbours = node.getNeighbours();
    }

    public Vector<String> getAssignment() { return assignment; }

    public LinkedList<kvHyperCubeNode> getNeighbours() { return neighbours; }

    public void setNeighbours(LinkedList<kvHyperCubeNode> neighbours) { this.neighbours = neighbours; }

    public void addNeighbour(kvHyperCubeNode neighbour) { neighbours.add(neighbour); }

    public boolean getValue() { return value; }

    /**
     * Checks whether the this node is a neighbour of "node"
     * @return True if the nodes assignments differentiate in exactly one position
     */
    public boolean isNeighbourOf(kvHyperCubeNode node)
    {
        int difference = 0;
        int i = 0;
        for(String s : node.getAssignment())
        {
            if( !s.equals(assignment.get(i)) )
                difference++;
            i++;
        }

        if( difference == 1) return true;
        else return false;
    }

    /**
     * Counts the number of different variables between this node and the node handled to this function
     * @return count of differences
     */
    public int getDifferenceCounter(kvHyperCubeNode node)
    {
        int difference = 0;
        int i = 0;
        for(String s : node.getAssignment())
        {
            if( !s.equals(assignment.get(i)) )
                difference++;
            i++;
        }

        return difference;
    }
}
