
public class ClauseTrieLeaf
{
    private int[] literalIds;
    private ClauseTrieNode parentNode;

    private ClauseTrieNode getParentNode() { return parentNode; }

    private int[] getLiteralIds() { return literalIds; }

    public ClauseTrieLeaf(ClauseTrieNode parent)
    {
        parentNode = parent;

        ClauseTrieNode currNode = parentNode;
        int i = 0;
        while( !currNode.getIsRoot() )
        {
            currNode = currNode.getParentNode();
            i++;
        }
        literalIds = new int[i];

        currNode = parentNode;
        while( !currNode.getIsRoot() )
        {
            literalIds[i-1] = currNode.getId();
            currNode = currNode.getParentNode();
            i--;
        }
    }
}
