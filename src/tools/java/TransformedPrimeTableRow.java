import java.util.LinkedList;
import java.util.TreeSet;


public class TransformedPrimeTableRow
{
    /**
     * number of this minterm
     */
    private int primeTermNumber;
    /**
     * the list of prime terms covering this term
     */
    private TreeSet<Integer> coveredBy;

    public int getPrimeTermNumber() { return primeTermNumber; }

    public TreeSet<Integer> getCoveredBy() { return coveredBy; }

    public TransformedPrimeTableRow(int termNumber, TreeSet<Integer> coveredByList)
    {
        primeTermNumber = termNumber;
        coveredBy = coveredByList;
    }
}
