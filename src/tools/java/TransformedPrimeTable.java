import java.util.LinkedList;
import java.util.TreeSet;

/**
 * A tool class utilized in FormulaMinimizer
 */
public class TransformedPrimeTable
{
    private LinkedList<TransformedPrimeTableRow> minTerms;

    public LinkedList<TransformedPrimeTableRow> getMinTerms() { return minTerms; }

    public void print()
    {
        for(TransformedPrimeTableRow row : minTerms)
        {
            System.out.print(row.getPrimeTermNumber() + " | ");
            for(Integer i : row.getCoveredBy())
            {
                System.out.print(i.intValue() + " ");
            }
            System.out.println("");
        }
    }

    public TransformedPrimeTable(TruthTable table)
    {
        minTerms = new LinkedList<TransformedPrimeTableRow>();

        TreeSet<Integer> minTermNumbers = new TreeSet<Integer>();

        for(TruthTableRow row : table.getTruthTable())
        {
            if( !minTermNumbers.containsAll(row.getCovers()) )
                minTermNumbers.addAll(row.getCovers());
        }

        for(Integer i : minTermNumbers)
        {
            TreeSet<Integer> coveredBy = new TreeSet<Integer>();

            for(TruthTableRow row : table.getTruthTable())
            {
                if( row.getCovers().contains(i) )
                    coveredBy.add(row.getRowNumber());
            }

            minTerms.add(new TransformedPrimeTableRow(i, coveredBy));
        }
    }
}
